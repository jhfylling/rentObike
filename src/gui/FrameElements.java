package gui;

import java.awt.GridBagConstraints;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The FrameElements interface is used to manage GridBagConstraints.
 */

public interface FrameElements {
    /**
     *
     * @return the GridBagConstraints used for this object
     */
    public GridBagConstraints getGbc();

    /**
     *
     * @param gbc set GridBagConstraints to be used by object
     */
    public void setGbc(GridBagConstraints gbc);
}
