package gui;

import design.GuiColors;

import javax.swing.*;
import java.awt.*;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The MenuButton class is used to manage menu buttons.
 */

public class MenuButton extends JButton {
    private GridBagConstraints gbc;

    /**
     * Creates a button with text.
     * @param text the text of the button
     */
    public MenuButton(String text) {
        super(text);

        setBorder(BorderFactory.createEmptyBorder());
        setOpaque(true);
        setBackground(GuiColors.menuColor);
        setFocusPainted(false);
        //setSize(70, 90);
        setPreferredSize(new Dimension(150, 50));
        setFont(new Font("Arial", Font.PLAIN, 18));
    }

    /**
     * Set a color for the button
     * @param g a graphics object
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if(getModel().isPressed()){
            g.setColor(new Color(82, 177, 249));
        }
    }
}
