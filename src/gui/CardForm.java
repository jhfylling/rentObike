package gui;

import design.GuiColors;
import dto.BikeType;
import forms.FormLabel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The CardForm class is inherited by several form classes
 * The CardForms are displayed on top of other content like a floating card
 * Depends on a JFrame with a JLayeredPane
 */

public abstract class CardForm extends JPanel {
    private JFrame parentFrame;
    protected TablePanel parentPanel;
    public int x, y, width, height;
    private JLayeredPane layeredPane;
    protected JLabel headLine;
    protected JPanel buttonWrapper;
    protected JButton submit, cancel;
    protected JLabel errorLabel;

    protected ArrayList<JTextField> textFields;
    protected ArrayList<JLabel> labels;

    /**
     * Creates a CardForm on top of other content in order to collect data from user
     * @param parentFrame the JFrame container the card should be added to
     * @param parentPanel the panel that is retrieving data from user
     * @param x cards horizontal position anchored left side
     * @param y cards vertical position anchored in bottom
     * @param width card width
     * @param height card height
     */
    public CardForm(JFrame parentFrame, TablePanel parentPanel, int x, int y, int width, int height) {
        BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
        setLayout(layout);
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.setBounds(x, y, width, height);
        this.parentFrame = parentFrame;
        this.parentPanel = parentPanel;

        layeredPane = parentFrame.getLayeredPane();
        setStyle();
        layeredPane.add(this, JLayeredPane.POPUP_LAYER);
        parentFrame.repaint();
        setVisible(false);
        setVisible(true);

        parentFrame.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                newBounds();
            }
        });

        textFields = new ArrayList<>();
        labels = new ArrayList<>();

        //headline
        headLine = new JLabel();
        headLine.setFont(new Font("Arial", Font.PLAIN, 23));
        headLine.setPreferredSize(new Dimension(this.getWidth(), 25));
        headLine.setMaximumSize(new Dimension(this.getWidth(), 25));
        headLine.setMinimumSize(new Dimension(this.getWidth(), 25));
        headLine.setOpaque(true);

        headLine.setBackground(GuiColors.DARK);
        headLine.setForeground(GuiColors.WHITE);

        errorLabel = new FormLabel(" ");
        errorLabel.setForeground(GuiColors.RED);

        add(headLine);


    }

    /**
     * Adds labels and related textfield to ArrayLists
     * Cal createForm() to add them to the form
     * @param label JLabel above textfield
     */
    protected void addInput(String label){
        labels.add(new JLabel(label));
        textFields.add(new JTextField());
    }
    protected void addDropDownInput(JComboBox<BikeType> types){

    }

    /**
     * Adds labels and textfields to the form with dark theme style
     */
    protected void createForm(){
        if(labels.size() != 0){
            for(int i=0;i<labels.size();i++){
                //add label first with style
                JLabel tempLabel = labels.get(i);
                tempLabel.setForeground(GuiColors.GRAY_TEXT);
                add(tempLabel);

                JTextField tempField = textFields.get(i);
                tempField.setPreferredSize(new Dimension(width, 25));
                tempField.setBackground(GuiColors.ODD_DARK);
                tempField.setForeground(GuiColors.WHITE);
                tempField.setCaretColor(GuiColors.WHITE);
                tempField.setBorder(BorderFactory.createLineBorder(GuiColors.GRAY_TEXT, 1));
                add(tempField);
            }
        }


        //add submit button and errorlabel to the bottom
        add(errorLabel);
        addSubmitAndCancel();

        //mouse listeners to make Card clickable
        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {

            }
        });

        if(textFields.size() != 0) textFields.get(0).requestFocus();
    }

    /**
     * Adds submit and cancel button to the CardForm
     */
    private void addSubmitAndCancel(){
        buttonWrapper = new JPanel(new BorderLayout());
        buttonWrapper.setBackground(GuiColors.DARK);
        addSubmitButton();
        addCancelButton();
        add(buttonWrapper);
    }

    /**
     * Adds a styled submit button to the form
     */
    protected void addSubmitButton(){
        //submit button
        submit = new JButton("Submit");
        submit.addActionListener(e -> save());
        submit.setBorder(BorderFactory.createEmptyBorder());
        submit.setOpaque(true);
        submit.setBackground(Color.BLACK);
        submit.setForeground(GuiColors.GRAY_TEXT);
        submit.setFocusPainted(false);
        submit.setPreferredSize(new Dimension(50, 30));
        submit.setMaximumSize(new Dimension(50, 30));
        submit.setMinimumSize(new Dimension(50, 30));
        submit.setFont(new Font("Arial", Font.PLAIN, 14));

        buttonWrapper.add(submit, BorderLayout.WEST);
    }

    /**
     * Adds styled cancel button to the form
     */
    private void addCancelButton(){
        cancel = new JButton("Cancel");
        cancel.addActionListener(e -> cancelAndClear());
        cancel.setBorder(BorderFactory.createEmptyBorder());
        cancel.setOpaque(true);
        cancel.setBackground(Color.BLACK);
        cancel.setForeground(GuiColors.GRAY_TEXT);
        cancel.setFocusPainted(false);
        cancel.setPreferredSize(new Dimension(50, 30));
        cancel.setMaximumSize(new Dimension(50, 30));
        cancel.setMinimumSize(new Dimension(50, 30));
        cancel.setFont(new Font("Arial", Font.PLAIN, 14));

        buttonWrapper.add(cancel, BorderLayout.EAST);
    }

    /**
     * Called upon clicking submit
     * Uses parentPanels save method to save all inputs
     * after validation
     */
    protected void save(){
        if(validateInput()) {
            ArrayList<String> input = new ArrayList<>();
            for (JTextField temp : textFields) {
                input.add(temp.getText());
            }
            if (!parentSave(input)) {
                errorLabel.setText("Could not save");
                validate();
            }else{
                for (JTextField temp : textFields) {
                    temp.setText(null);
                }
                this.hideCard();
                errorLabel.setText(" ");
            }
        }
    }

    /**
     * Resets all textfields, errorlabel and hides card
     */
    protected void cancelAndClear(){
        for(JTextField temp : textFields){
            temp.setText(null);
        }
        this.hideCard();
        errorLabel.setText(" ");
    }

    /**
     * @return true if all input fields contain valid input
     */
    protected abstract boolean validateInput();

    /**
     * Used when GUI is resized in order to manually recalculate and set
     * card position and size
     */
    public void newBounds(){
        if(isVisible()) {
            int height = this.height;
            int width = this.width;
            int x = this.x;
            int y = this.getRootPane().getHeight() - height - 60;
            this.setBounds(x, y, width, height);
        }
    }

    /**
     * Repaints parentFrame
     */
    private void parentRepaint(){
        parentFrame.repaint();
    }

    /**
     * @param input an arraylist of string inputs to save
     * @return true if parentpanel's save method succeeds
     */
    protected boolean parentSave(ArrayList<String> input){
        return parentPanel.save(input);
    }

    /**
     * Styling options set in this method
     */
    private void setStyle(){
        setBackground(GuiColors.DARK);
        setBorder(new EmptyBorder(5,5,5,5));
    }

    /**
     * Adds card to layeredpane and set's card visible
     */
    protected void showCard(){
        this.setBounds(x, y, width, height);
        setVisible(true);
        layeredPane.add(this, JLayeredPane.POPUP_LAYER);
        parentRepaint();
        if(textFields.size() != 0) textFields.get(0).requestFocus();
    }

    /**
     * Temporarily remove card from frame
     */
    public void hideCard(){
        setVisible(false);
        layeredPane.remove(this);
        parentRepaint();
    }

    /**
     * Show card if card is hidden
     * Hide card if card is displayed
     */
    public void toggle(){
        if(isVisible()){
            hideCard();
        }
        else{
            showCard();
        }
    }
}
