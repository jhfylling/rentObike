package gui;

import design.GuiColors;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.util.Date;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The Table class is used to create a table.
 */

public class Table extends JTable {

    private JTableHeader header;
    private DefaultTableModel jtModel = (DefaultTableModel) getModel();

    /**
     * A constructor used to create a new table
     * @param tableData all table data of type Object[][]
     * @param column header data of type String[]
     */
    public Table(Object[][] tableData, String [] column ){
        super(new DefaultTableModel(tableData, column));
        setStyle();
    }

    /**
     * A constructor used to create new table with custom table models
     * @param model custom table model created in panel class
     */
    public Table(TableModel model){
        super(model);
        setStyle();
    }

    /**
     * Set the style of the table
     */
    private void setStyle(){
        header = getTableHeader();
        header.setBackground(Color.white);
        header.setFont(new Font("Courier New Greek", Font.BOLD, 15));
        header.setPreferredSize(new Dimension(100,50));

        setRowHeight(25);
        setFont(new Font("Courier New Greek", Font.PLAIN, 15));
        setShowGrid(false);
        setAutoCreateRowSorter(true);
        setAlternativRowColor();
    }

    /**
     * Set alternative colors for the rows in the table
     */
    private void setAlternativRowColor(){
        setDefaultRenderer(Object.class, new TableCellRenderer() {

            private DefaultTableCellRenderer DEFAULT_RENDERER =  new DefaultTableCellRenderer();



            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component c = DEFAULT_RENDERER.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                DEFAULT_RENDERER.setHorizontalAlignment(JLabel.CENTER);

                return setRowColor(isSelected, c, row);
            }

        });

        setDefaultRenderer(Integer.class, new TableCellRenderer() {
            private DefaultTableCellRenderer DEFAULT_RENDERER =  new DefaultTableCellRenderer();
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component c = DEFAULT_RENDERER.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                DEFAULT_RENDERER.setHorizontalAlignment( JLabel.CENTER );
                return setRowColor(isSelected, c, row);
            }

        });

        setDefaultRenderer(Date.class, new TableCellRenderer() {
            private DefaultTableCellRenderer DEFAULT_RENDERER =  new DefaultTableCellRenderer();
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component c = DEFAULT_RENDERER.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                DEFAULT_RENDERER.setHorizontalAlignment( JLabel.CENTER );
                return setRowColor(isSelected, c, row);
            }

        });

        setDefaultRenderer(Double.class, new TableCellRenderer() {
            private DefaultTableCellRenderer DEFAULT_RENDERER =  new DefaultTableCellRenderer();
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component c = DEFAULT_RENDERER.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                DEFAULT_RENDERER.setHorizontalAlignment( JLabel.CENTER );
                return setRowColor(isSelected, c, row);
            }
        });
    }

    /**
     * Help method to setAlternativRowColor() sets component to dark theme if selected
     * And alternates colors in list
     * @param isSelected boolean from getTableCellRendererComponent true if row is selected
     * @param c component to decorate
     * @param row row to decorate
     * @return decorated component
     */
    private Component setRowColor(boolean isSelected, Component c, int row){
        if(isSelected){
            c.setBackground(GuiColors.ODD_DARK);
            c.setForeground(GuiColors.GRAY_TEXT);
        }
        else{
            c.setForeground(GuiColors.BLACK);
            if (row%2 == 0){
                c.setBackground(Color.WHITE);
            }
            else {
                c.setBackground(GuiColors.listColor);
            }
        }
        return c;
    }

    /**
     * Can be used on an interval in order to keep the panel updated
     * @param tabledata a table with data
     */
    public void updateTable(Object [][] tabledata) {
        RentoBikeTableModel jt = (RentoBikeTableModel) jtModel;

        int currentRows = jtModel.getRowCount();
        int dataLength = tabledata.length;

        int newLength;
        if(currentRows < dataLength){
            newLength = currentRows;
        }
        else{
            newLength = dataLength;
        }

        for (int i = 0; i < newLength; i++) {
            for (int x = 0; x < tabledata[0].length; x++) {
                jt.updateValueAt(tabledata[i][x], i, x);
            }
        }

        if(currentRows < dataLength){
            for(int i=currentRows;i<dataLength;i++){
                jt.addRow(tabledata[i]);
            }
        }

        if(currentRows > dataLength){
            System.out.println("current: "+currentRows+", data: "+dataLength);
            for(int i=currentRows-1;i>=dataLength;i--){
                System.out.println("Deleting "+i);
                jt.removeRow(i);
            }
        }
    }
}
