package gui;


import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The PictureLabel class is used to create a picture label.
 */

public class PictureLabel extends JLabel implements FrameElements {

    private GridBagConstraints gbc;

    /**
     * A constructor to create a picture label.
     * @param x x coordinate
     * @param y y coordinate
     * @param weightX size of X in percentage
     * @param weightY size of Y in percentage
     * @param spanX gridWidth X direction
     * @param spanY gridHeight Y direction
     * @param anchor placement inside a cell
     * @param image the image you want to use
     */
    public PictureLabel(int x, int y, int weightX, int weightY, int spanX, int spanY, int anchor, BufferedImage image){
        super(new ImageIcon(image));
        gbc = new GridBagConstraints();
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.weightx = weightX;
        gbc.weighty = weightY;
        gbc.gridwidth = spanX;
        gbc.gridheight = spanY;
        gbc.anchor = anchor;
    }

    /**
     * Get an GridBagConstraint object
     * @return an GridBagConstraint object
     */
    @Override
    public GridBagConstraints getGbc() {
        return gbc;
    }

    /**
     * Set a new GridBagConstraint object
     * @param gbc set GridBagConstraints to be used by object
     */
    @Override
    public void setGbc(GridBagConstraints gbc) {

    }
}
