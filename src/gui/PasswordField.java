package gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The PasswordField class is used to create a password field.
 */

public class PasswordField extends JPasswordField implements FrameElements {

    private GridBagConstraints gbc;

    /**
     * A constructor to create a password field.
     * @param x x coordinate
     * @param y y coordinate
     * @param weightX size of X in percentage
     * @param weightY size of Y in percentage
     * @param spanX gridWidth X direction
     * @param spanY gridHeight Y direction
     * @param anchor placement inside a cell
     * @param columns number of columns in JTextField
     */
    public PasswordField(int x, int y, int weightX, int weightY, int spanX, int spanY, int anchor, int columns){
        super(columns);
        gbc = new GridBagConstraints();
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.weightx = weightX;
        gbc.weighty = weightY;
        gbc.gridwidth = spanX;
        gbc.gridheight = spanY;
        gbc.anchor = anchor;
    }

    /**
     * @return a GridBagConstraint object
     */
    @Override
    public GridBagConstraints getGbc() {
        return gbc;
    }

    /**
     * @param gbc set GridBagConstraints to be used by object
     */
    @Override
    public void setGbc(GridBagConstraints gbc) {

    }
}
