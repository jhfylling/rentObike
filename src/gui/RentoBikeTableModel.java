package gui;

import javax.swing.table.DefaultTableModel;
import java.sql.Date;
import java.util.ArrayList;

public class RentoBikeTableModel extends DefaultTableModel {
    TablePanel parentPanel;
    ArrayList<Class> editor;

    /**
     * Constructs a <code>DefaultTableModel</code> and initializes the table
     * by passing <code>data</code> and <code>columnNames</code>
     * to the <code>setDataVector</code>
     * method. The first index in the <code>Object[][]</code> array is
     * the row index and the second is the column index.
     *
     * @param data        the data of the table
     * @param columnNames the names of the columns
     * @see #getDataVector
     * @see #setDataVector
     */
    public RentoBikeTableModel(Object[][] data, Object[] columnNames, TablePanel parentPanel, ArrayList<Class> editor) {
        super(data, columnNames);
        this.parentPanel = parentPanel;
        this.editor = editor;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return editor.get(columnIndex);
    }

    /**
     * Sets the object value for the cell at <code>column</code> and
     * <code>row</code>.  <code>aValue</code> is the new value.  This method
     * will generate a <code>tableChanged</code> notification.
     *
     * @param aValue the new value; this can be null
     * @param row    the row whose value is to be changed
     * @param column the column whose value is to be changed
     * @throws ArrayIndexOutOfBoundsException if an invalid row or
     *                                        column was given
     */
    @Override
    public void setValueAt(Object aValue, int row, int column) {
        super.setValueAt(aValue, row, column);
        parentPanel.clickCellEdit();
    }

    public void updateValueAt(Object aValue, int row, int column) {
        super.setValueAt(aValue, row, column);
    }

    /**
     * Returns true regardless of parameter values.
     *
     * @param row    the row whose value is to be queried
     * @param column the column whose value is to be queried
     * @return true
     * @see #setValueAt
     */
    @Override
    public boolean isCellEditable(int row, int column) {
        return (column != 0);
    }

}
