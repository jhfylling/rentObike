package gui;


import design.GuiColors;
import panels.StatisticsPanel;
import panels.*;


import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import sql.DBconnection;
import sun.java2d.SunGraphicsEnvironment;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 *
 * The Frame class is the main JFrame of the application and initiated by the Main method
 */

public class Frame extends JFrame{

    private String adminEmail;

    private JPanel wrapper, left, top, right, bottom;
    private LoginPanel login;

    private MenuPanel menu;
    private TablePanel content;
    private ArrayList<TablePanel> contentList = new ArrayList<>();
    private DBconnection connector = new DBconnection();
    private Connection con = null;

    private String title;
    private int width;
    private int height;


    public static boolean shouldUpdate = true;

    private Color normalC = GuiColors.focusColor;

    /**
     * Creates a new, initially invisible <code>Frame</code> with the
     * specified title.
     * <p>
     * This constructor sets the component's locale property to the value
     * returned by <code>JComponent.getDefaultLocale</code>.
     *
     * @throws HeadlessException if GraphicsEnvironment.isHeadless()
     *                           returns true.
     * @see GraphicsEnvironment#isHeadless
     * @see Component#setSize
     * @see Component#setVisible
     * @see JComponent#getDefaultLocale
     */
    public Frame(String title, int width, int height) throws HeadlessException {
        this.title = title;
        this.width = width;
        this.height = height;

        setTitle(title);                            //set window title
        setSize(700, 466);              //set window size to login image size
        setUndecorated(true);                       //remove native decoration such as title bar and borders
        setLayout(new BorderLayout());              //set frame layout to borderlayout
        setLocationRelativeTo(null);                //place window in the middle of the screen
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);    //default close operation to dispose on close

        //connect to database
        if(connector.connect()){
            con = connector.getCon();
            System.out.println("Frame connected to DB");
        }else{
            System.out.println("Could not connect to database");
        }

        //set frame icon (taskbar icon)
        this.setIconImage(getImage("/design/pictures/rentobikeIcon.png"));


        add(login = new LoginPanel(this, con), BorderLayout.CENTER);    //add loginpanel

        setVisible(true);   //set everything visible
        //start(adminEmail);
    }

    /**
     * Called by LoginPanel as soon as login is validated
     * Starts main application panels
     */
    public void start(String adminEmail){
        this.adminEmail = adminEmail;
        //remove login panel
        setVisible(false);
        remove(login);

        //This limits the extended state max to leave the windows taskbar
        Rectangle r = SunGraphicsEnvironment.getUsableBounds(getGraphicsConfiguration().getDevice());
        setMaximizedBounds(r);

        //set extendedstate to normal size
        setExtendedState(JFrame.NORMAL);

        //disable default closing so we can override
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        //override closing operation to close connection before exiting

        //initiate wrapper
        wrapper = new JPanel(new BorderLayout());

        //initiate borders
        left = new JPanel(new BorderLayout());
        top = new JPanel(new BorderLayout());
        right = new JPanel(new BorderLayout());
        bottom = new JPanel(new BorderLayout());

        //left.setOpaque(false);
        left.setBackground(normalC);
        top.setBackground(normalC);
        right.setBackground(normalC);
        bottom.setBackground(normalC);

        left.setBorder(new EmptyBorder(5, 5, 5, 5));
        right.setBorder(new EmptyBorder(5, 5, 5, 5));
        bottom.setBorder(new EmptyBorder(5, 5, 5, 5));

        top.setBackground(normalC);

        BorderResizeListener borderResizeListener = new BorderResizeListener(this, true);
        left.addMouseListener(borderResizeListener);
        left.addMouseMotionListener(borderResizeListener);

        borderResizeListener = new BorderResizeListener(this, false);
        right.addMouseListener(borderResizeListener);
        right.addMouseMotionListener(borderResizeListener);
        bottom.addMouseListener(borderResizeListener);
        bottom.addMouseMotionListener(borderResizeListener);


        //add wrapper to frame
        add(wrapper, BorderLayout.CENTER);

        //add draggable / resize borders to frame
        add(bottom, BorderLayout.SOUTH);

        onLoad();

        setSize(width, height);
        //place window in the middle of the screen
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * Overriden dispose to close connections and exit cleanly when window is closed
     */
    @Override
    public void dispose() {
        shouldUpdate = false;
        //TODO: wait for threads to stop before continuing
        //TODO: right now we sometimes close connections right before an update is made
        for(TablePanel temp : contentList){
            temp.closeCon();
        }
        connector.disconnect();
        System.exit(0);
        super.dispose();
    }

    /**
     * Gets the background color of this window.
     * <p>
     * Note that the alpha component of the returned color indicates whether
     * the window is in the non-opaque (per-pixel translucent) mode.
     *
     * @return this component's background color
     * @see Window#setBackground(Color)
     * @see Window#isOpaque
     * @see GraphicsDevice.WindowTranslucency
     */
    @Override
    public Color getBackground() {
        return new Color(69, 143, 209);
    }


    /**
     * Run this upon loading the frame in order to add all panels to the panel list
     * and to set the initial content
     */
    public void onLoad(){

        //load panels into contentlist
        contentList.add(new StatisticsPanel(con));
        contentList.add(new MapsPanel(con));
        contentList.add(new AdminPanel(con));
        contentList.add(new DockingPanel(con));
        contentList.add(new BikePanel(con));
        contentList.add(new UserPanel(con));

        //set menupanel
        menu = new MenuPanel(this, con, adminEmail);
        menu.setPreferredSize(new Dimension(500, 50));
        wrapper.add(menu, BorderLayout.NORTH);

        //set first content to map
        changeContent(0);
    }

    /**
     * Changes the content panel of the main frame into a different panel
     * with a different view
     * @param panelId The index of the desired panel
     */
    public void changeContent(int panelId){
        //remove current content
        if(content != null){
            wrapper.remove(content);
            content.swapOut();
        }

        //set new content according to id
        content = contentList.get(panelId);
        wrapper.add(content, BorderLayout.CENTER);
        repaint();
        setVisible(true);
    }

    /**
     * @param path to image
     * @return image from path
     */
    private Image getImage(String path){
        InputStream in = getClass().getResourceAsStream(path);
        try {
            BufferedImage img = ImageIO.read(in);
            return img;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
