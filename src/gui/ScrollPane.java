package gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The ScrollPane class is used to create a scroll pane.
 */

public class ScrollPane extends JScrollPane {

    /**
     * A constructor used to create a table for the scroll pane
     * @param table a new JTable object
     */
    public ScrollPane (JTable table){
        super(table);
        setStyle();
    }

    /**
     * Set the style for the table and scroll pane
     */
    private void setStyle(){
        setBorder(BorderFactory.createEmptyBorder());
        getViewport().setBackground(Color.white);
    }
}
