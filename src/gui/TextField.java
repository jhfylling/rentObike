package gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The TextField class is used to create a text field used in different methods and panels.
 */

public class TextField extends JTextField implements FrameElements {
    private GridBagConstraints gbc;

    /**
     * Constructs a new empty <code>TextField</code> with the specified
     * number of columns.
     * A default model is created and the initial string is set to
     * <code>null</code>.
     * @param x the x coordinate
     * @param y the y coordinate
     * @param weightX space to use in x direction
     * @param weightY space to use in y direction
     * @param spanX number of columns to span
     * @param spanY number of rows to span
     * @param anchor gridbag enums to place object in cell
     * @param columns the number of columns to use to calculate
     *                the preferred width; if columns is set to zero, the
     *                preferred width will be whatever naturally results from
     *                the component implementation
     */
    public TextField(int x, int y, int weightX, int weightY, int spanX, int spanY, int anchor, int columns) {
        super(columns);
        gbc = new GridBagConstraints();
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.weightx = weightX;
        gbc.weighty = weightY;
        gbc.gridwidth = spanX;
        gbc.gridheight = spanY;
        gbc.anchor = anchor;
    }

    /**
     * @return the GridBagConstraints used for this object
     */
    @Override
    public GridBagConstraints getGbc() {
        return gbc;
    }

    /**
     * @param gbc set GridBagConstraints to be used by object
     */
    @Override
    public void setGbc(GridBagConstraints gbc) {

    }
}
