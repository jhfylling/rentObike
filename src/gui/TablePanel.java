package gui;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The Panel class is an abstract class used to manage panels used in the system.
 */

abstract public class TablePanel extends JPanel{

    /**
     * Create a new buffered JPanel with specified layout manager
     * @param layout a new LayoutManager object
     */
    public TablePanel(LayoutManager layout) {
        super(layout);
        setBackground(new Color(255, 255, 255));
    }

    /**
     * an empty abstract method to close connection to the database
     */
    abstract public void closeCon();

    /**
     * An empty abstract method swapOut
     */
    abstract public void swapOut();

    /**
     * An empty abstract method save
     * @param input an string array with input
     * @return abstract
     */
    abstract public boolean save(ArrayList<String> input);

    /**
     * An empty abstract method save
     * @return abstract
     */
    abstract public Object[][] tableData();

    /**
     * An empty abstract method save
     * @return abstract
     */
    abstract public boolean clickCellEdit();
}
