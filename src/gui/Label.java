package gui;

import javax.swing.JLabel;
import java.awt.*;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The Label class is used to manage labels used in the gui.
 */

public class Label extends JLabel implements FrameElements {
    private GridBagConstraints gbc;

    /**
     * Creates a <code>JLabel</code> instance with the specified text.
     * The label is aligned against the leading edge of its display area,
     * and centered vertically.
     * @param x the x coordinate
     * @param y the y coordinate
     * @param weightX space to use in x direction
     * @param weightY space to use in y direction
     * @param spanX number of columns to span
     * @param spanY number of rows to span
     * @param anchor gridbag to place object in cell
     * @param text The text to be displayed by the label.
     */
    public Label(int x, int y, int weightX, int weightY, int spanX, int spanY, int anchor, String text) {
        super(text);
        gbc = new GridBagConstraints();
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.weightx = weightX;
        gbc.weighty = weightY;
        gbc.gridwidth = spanX;
        gbc.gridheight = spanY;
        gbc.anchor = anchor;
       // setText(text);
       // setVisible(true);
    }
    /**
     * @return the GridBagConstraints used for this object
     */
    @Override
    public GridBagConstraints getGbc() {
        return gbc;
    }

    /**
     * @param gbc set GridBagConstraints to be used by object
     */
    @Override
    public void setGbc(GridBagConstraints gbc) {

    }

}
