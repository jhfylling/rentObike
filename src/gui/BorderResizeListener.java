package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The BorderResizeListener class is used to manage movement and resizing of the frame.
 */

public class BorderResizeListener extends MouseAdapter{
    private final JFrame frame;
    private Point mouseDownCompCoords = null;
    private Point start = null;
    private int width;
    private int height;
    private boolean left;

    /**
     * A constructor to initialize the class
     * @param frame the frame you want to resize
     * @param left to check if its the left or right side you are dragging the frame
     */
    public BorderResizeListener(JFrame frame, boolean left) {
        this.frame = frame;
        this.left = left;
    }

    /**
     * To tell that the mouse is released
     * @param e an mouse event object
     */
    public void mouseReleased(MouseEvent e) {
        mouseDownCompCoords = null;
    }

    /**
     * To tell that the mouse is pressed
     * @param e an mouse event object
     */
    public void mousePressed(MouseEvent e) {
        mouseDownCompCoords = e.getPoint();
        start = e.getLocationOnScreen();
        width = frame.getWidth();
        height = frame.getHeight();

        System.out.println("width: "+width+", height: "+height);
    }

    /**
     * to adjust the frame from which direction it is dragged
     * @param e an mouse event object
     */
    public void mouseDragged(MouseEvent e) {
        Point currCoords = e.getLocationOnScreen();

        if(left) {
            //frame.setLocation(currCoords.x - mouseDownCompCoords.x, currCoords.y - mouseDownCompCoords.y);
            frame.setLocation(currCoords.x - mouseDownCompCoords.x, start.y - mouseDownCompCoords.y);
            frame.setSize(width - (currCoords.x - start.x), height + (currCoords.y - start.y));
        }
        else{
            frame.setSize(width + (currCoords.x - start.x), height + (currCoords.y - start.y));
        }
    }
}
