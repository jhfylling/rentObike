package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The FrameDragListener class is used to manage adjustments to the frame.
 */

public class FrameDragListener extends MouseAdapter{
    private final JFrame frame;
    private Point mouseDownCompCoords = null;

    /**
     * A constructor used to create a frame
     * @param frame a new frame
     */
    public FrameDragListener(JFrame frame) {
        this.frame = frame;
    }

    /**
     * To tell that the mouse is released
     * @param e an mouse event object
     */
    public void mouseReleased(MouseEvent e) {
        mouseDownCompCoords = null;
    }

    /**
     * To tell that the mouse is pressed
     * @param e an mouse event object
     */
    public void mousePressed(MouseEvent e) {
        mouseDownCompCoords = e.getPoint();
    }

    /**
     * adjusts the frame from the output of where the mouse is dragged
     * @param e an mouse event object
     */
    public void mouseDragged(MouseEvent e) {
        if(!(frame.getExtendedState() == JFrame.MAXIMIZED_BOTH)) {
            Point currCoords = e.getLocationOnScreen();
            frame.setLocation(currCoords.x - mouseDownCompCoords.x, currCoords.y - mouseDownCompCoords.y);
        }
    }
}
