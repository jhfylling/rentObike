package sqlquery;

import java.io.*;
import java.util.Random;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The BikeGenerator class is used to generated new bikes.
 */

public class BikeGenerator {
    private int[] dock_id = new int[1000];

    /**
     * Method that creates insert query
     * @return String
     */
    public String bikeGenerator(){
        String insertSQL2 = "(DEFAULT,'";
        String insertSQL3 = ",110,85,('2018-03-21'),";
        String[] bikemake = {"DBS","Scott","GT","KTM","Hardrocx","Bits","Merida","White","BMC","Gekko","Fuji"};
        String[] price = {"599.00","799.50","999.99","399.99","1499.00","2459.99","4900.99","4799.99","2999.99","1200.00","5999.99"};

        Random randomInt = new Random();
        Random rnd = new Random();

        String res = "";
        for(int i = 0; i < 1000; i++){
            int dockID = randomInt.nextInt(30)+1;
            dock_id[i] = dockID;
            int typeID = rnd.nextInt(5)+1;
            res +=  insertSQL2 + getRandom(bikemake) + "'," + getRandom(price) + insertSQL3 + typeID+ "," + dockID + ")," + "\n" ;
        }
        return res;
    }

    /**
     * private method that returns one random index from the array
     * @param array
     * @return array index
     */
    private String getRandom(String[] array) {
        String random = String.valueOf(new Random().nextInt(array.length));
        return array[Integer.parseInt(random)];
    }

    public String getPos(){
        String res = "INSERT INTO position VALUES" + "\n";
        String insert1 = "(DEFAULT, (SELECT latitude FROM dock WHERE dock_id = ";// generer dockID
        String insert2 = "), (SELECT longitude FROM dock WHERE dock_id = ";
        String insert3 = "), CURRENT_TIMESTAMP, DEFAULT, ";
        for(int i = 0; i < 1000; i++){
            int id = i+1;
            res += insert1 + dock_id[i] + insert2 + dock_id[i] + insert3 + id +")," + "\n";
        }
        return res;
    }

    /**
     * writes to file
     * @param filename filename
     */
    public void writeTofile(String filename){

        try{
            FileWriter write = new FileWriter(filename);
            PrintWriter print = new PrintWriter(new BufferedWriter(write));
            print.print(bikeGenerator());
            print.print(getPos());
            print.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException ioe){
            ioe.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Main method to wright insert statements to file
     * @param args
     */
    public static void main(String[] args) {
        BikeGenerator bg = new BikeGenerator();
        bg.writeTofile("InsertSQL");
        System.out.println("finish");
    }
}
