USE rentobiketest;

SELECT SUM(power_usage) as totalUsage FROM dock;

DROP TABLE IF EXISTS admin;
DROP TABLE IF EXISTS position;
DROP TABLE IF EXISTS repairs;
DROP TABLE IF EXISTS transaction;
DROP TABLE IF EXISTS trip;
DROP TABLE IF EXISTS bike;
DROP TABLE IF EXISTS dock;
DROP TABLE IF EXISTS type;
DROP TABLE IF EXISTS users;

CREATE TABLE admin(
  admin_id INTEGER NOT NULL AUTO_INCREMENT,
  email VARCHAR(128),
  salt VARCHAR(256),
  hashed_pw VARCHAR(256),
  admin_name VARCHAR(128),
  -- security_level INTEGER,
  isEnabled BOOLEAN DEFAULT TRUE,
  CONSTRAINT admin_id_pk PRIMARY KEY (admin_id));

CREATE TABLE bike(
  bike_id INTEGER NOT NULL AUTO_INCREMENT,
  make VARCHAR(256),
  price DOUBLE,
  mileage INTEGER,
  charge_level INTEGER,
  date_bought DATE,
  type_id INTEGER,
  dock_id INTEGER,
  isEnabled BOOLEAN DEFAULT TRUE,
  CONSTRAINT bike_id_pk PRIMARY KEY (bike_id));

CREATE TABLE dock(
  dock_id INTEGER NOT NULL AUTO_INCREMENT,
  dock_name VARCHAR(256),
  power_usage INTEGER,
  max_bikes INTEGER,
  latitude DOUBLE NOT NULL,
  longitude DOUBLE NOT NULL,
  isEnabled BOOLEAN DEFAULT TRUE,
  CONSTRAINT dock_id PRIMARY KEY (dock_id));

CREATE TABLE position (
  position_id INTEGER NOT NULL AUTO_INCREMENT,
  latitude DOUBLE NOT NULL,
  longitude DOUBLE NOT NULL,
  time_position TIMESTAMP,
  trip_id INTEGER,
  bike_id INTEGER,
  CONSTRAINT position_id_pk PRIMARY KEY (position_id));

CREATE TABLE repairs(
  repair_id INTEGER NOT NULL AUTO_INCREMENT,
  date_sent DATE,
  date_recieved DATE,
  price DOUBLE,
  request_description VARCHAR(2000),
  repair_description VARCHAR(2000),
  bike_id INTEGER,
  CONSTRAINT repair_id_pk PRIMARY KEY (repair_id));

CREATE TABLE transaction(
  transaction_id INTEGER NOT NULL AUTO_INCREMENT,
  amount DOUBLE,
  transaction_time TIMESTAMP,
  type VARCHAR(256),
  dock_id INTEGER,
  trip_id INTEGER,
  CONSTRAINT transaction_id_pk PRIMARY KEY (transaction_id));

CREATE TABLE trip(
  trip_id INTEGER NOT NULL AUTO_INCREMENT,
  distance INTEGER,
  from_dock INTEGER,
  to_dock INTEGER,
  start_time DATETIME,
  end_time DATETIME,
  user_id INTEGER,
  bike_id INTEGER,
  CONSTRAINT trip_id_pk PRIMARY KEY (trip_id));

CREATE TABLE type(
  type_id INTEGER NOT NULL AUTO_INCREMENT,
  description VARCHAR(1000),
  isEnabled BOOLEAN DEFAULT TRUE,
  CONSTRAINT type_id_pk PRIMARY KEY (type_id));

CREATE TABLE users(
  user_id INTEGER NOT NULL AUTO_INCREMENT,
  card_info VARCHAR(50),
  ban_until DATE,
  comments VARCHAR(1000),
  isEnabled BOOLEAN DEFAULT TRUE,
  CONSTRAINT user_id_pk PRIMARY KEY (user_id));


-- ---------------------------------------------------------------------------------------------------------------------

ALTER TABLE bike
  ADD CONSTRAINT type_id_fk1 FOREIGN KEY (type_id) REFERENCES type(type_id) ON DELETE CASCADE;
ALTER TABLE bike
  ADD CONSTRAINT dock_id_fk2 FOREIGN KEY (dock_id) REFERENCES dock(dock_id) ON DELETE CASCADE;

ALTER TABLE position
  ADD CONSTRAINT trip_id_fk1 FOREIGN KEY (trip_id) REFERENCES trip(trip_id) ON DELETE CASCADE;
ALTER TABLE position
  ADD CONSTRAINT bike_id_fk2 FOREIGN KEY (bike_id) REFERENCES bike(bike_id) ON DELETE CASCADE;

ALTER TABLE repairs
  ADD CONSTRAINT bike_id_fk3 FOREIGN KEY (bike_id) REFERENCES bike(bike_id) ON DELETE CASCADE;

ALTER TABLE transaction
  ADD CONSTRAINT dock_id_fk1 FOREIGN KEY (dock_id) REFERENCES dock(dock_id) ON DELETE CASCADE;
ALTER TABLE transaction
  ADD CONSTRAINT trip_id_fk2 FOREIGN KEY (trip_id) REFERENCES trip(trip_id) ON DELETE CASCADE;

ALTER TABLE trip
  ADD CONSTRAINT user_id_fk1 FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE;
ALTER TABLE trip
  ADD CONSTRAINT bike_id_fk1 FOREIGN KEY (bike_id) REFERENCES bike(bike_id) ON DELETE CASCADE;



INSERT INTO admin (admin_id, email, salt, hashed_pw, admin_name) VALUES
  (DEFAULT,"kristian_kampenhoy@mail.com","123456","1q2w3e4r5t6y","kristian"),
  (DEFAULT,"johan_fylling@mail.com","101010","10a10e10g","johan"),
  (DEFAULT,"ben_oscar@mail.com","asddfg","Ben","asd1dfg2g5h"),
  (DEFAULT,"test","b6b25597c8b2fa330eb6d67fbda5e221","c68ee95de77ac658f0226feb264b62af80708cbdf1822c2e022493b43b48c2c9535afdcc085c63ba029a17e6a809334fca39c2369ec4a1c25d5d72aed00f316","test");

-- (DEFAULT,"dahl_wahl@mail.com","qwe","qwe5ffas6casd","magnus",3),
-- (DEFAULT,"sigurd_hynne@mail.com","qwervqw","56asd8ca3","sigurd",4),
-- (DEFAULT,"test","b6b25597c8b2fa330eb6d67fbda5e221","c68ee95de77ac658f0226feb264b62af80708cbdf1822c2e022493b43b48c2c9535afdcc085c63ba029a17e6a809334fca39c2369ec4a1c25d5d72aed00f316","test",3);

INSERT INTO dock(dock_id, dock_name, power_usage, max_bikes, latitude, longitude) VALUES
  (DEFAULT, 'Kalvskinnet', 100,20, 63.429254, 10.387912),
  (DEFAULT, 'Leuthenhaven', 100,20, 63.430054, 10.391358),
  (DEFAULT, 'Gloshaugen', 100,20, 63.420796, 10.404359),
  (DEFAULT, 'St Olav', 100,20, 63.422130, 10.393562),
  (DEFAULT, 'Solsiden', 100,20, 63.433931, 10.412754);

INSERT INTO type(type_id, description) VALUES
  (DEFAULT ,"Gentlemen"),
  (DEFAULT ,"Ladies"),
  (DEFAULT ,"Unisex"),
  (DEFAULT ,"Small"),
  (DEFAULT ,"Large");

INSERT INTO bike (bike_id, make, price, mileage, charge_level, date_bought, type_id, dock_id) VALUES
  (DEFAULT,"DBS",500.0,110,85,('2018-03-21'),1,1),
  (DEFAULT,"DBS",500.0,140,95,('2018-03-21'),2,2),
  (DEFAULT,"DBS",750.40,70,80,('2018-03-21'),3,3),
  (DEFAULT,"DBS",800.50,50,100,('2018-03-21'),4,4),
  (DEFAULT,"DBS",999.99,150,70,('2018-03-21'),5,5);

INSERT INTO users(user_id, card_info, ban_until, comments) VALUES
  (DEFAULT ,'4512 7896 7896 1478','2018-01-01',''),
  (DEFAULT ,'4287 8265 9878 1320','2018-01-01',''),
  (DEFAULT ,'4578 6598 3265 1245',('2018-05-21'),"vandalism on docking station"),
  (DEFAULT ,'9685 6352 8574 5241','2018-01-01',''),
  (DEFAULT ,'1425 3696 8525 5847',('2018-03-05'),"tried to steal a bicylce");


INSERT INTO trip(trip_id, distance, from_dock, to_dock, start_time, end_time, user_id, bike_id) VALUES
  (DEFAULT ,100,1,4,('2018-03-10 18:45:15'),('2018-03-10 19:30:00'),1,1),
  (DEFAULT ,100,2,1,('2018-03-30 10:00:01'),('2018-03-30 12:00:45'),2,2),
  (DEFAULT ,100,4,4,('2018-03-25 20:30:16'),('2018-03-25 22:45:36'),3,3),
  (DEFAULT ,100,5,1,('2018-03-20 14:55:00'),('2018-03-20 16:15:00'),4,4),
  (DEFAULT ,100,3,2,('2018-03-05 12:00:00'),('2018-03-05 15:00:00'),5,5);

INSERT INTO position (position_id, latitude, longitude, trip_id, bike_id) VALUES
  (DEFAULT, 63.420, 10.400,1,1),
  (DEFAULT, 63.420, 10.410,2,2),
  (DEFAULT, 63.420, 10.420,3,3),
  (DEFAULT, 63.420, 10.430,4,4),
  (DEFAULT, 63.420, 10.440,5,5);

INSERT INTO repairs(repair_id, date_sent, date_recieved, price, request_description, repair_description, bike_id) VALUES
  (DEFAULT ,('2018-03-15'),('2018-03-25'),2500,"new battery","new battery",1),
  (DEFAULT ,('2018-03-15'),('2018-03-25'),2500,"rear wheel punctured","new rear wheel",2),
  (DEFAULT ,('2018-03-15'),('2018-03-25'),2500,"worn pedals"," new handels",3),
  (DEFAULT ,('2018-03-15'),('2018-03-25'),2500,"worn handels","new pedals",4),
  (DEFAULT ,('2018-02-15'),('2018-03-25'),2500,"malfunction","new bike",5),
  (DEFAULT ,("2018-04-14"),("2018-04-24"),1500,'new wheel','new wheel inn place',1);

INSERT INTO transaction(transaction_id, amount, type, dock_id, trip_id) VALUES
  (DEFAULT ,100.0,'VISA',1,1),
  (DEFAULT ,100.0,'MASTERCARD',2,2),
  (DEFAULT ,100.0,'MASTERCARD',3,3),
  (DEFAULT ,100.0,'VISA',4,4),
  (DEFAULT ,100.0,'CASH',5,5);
