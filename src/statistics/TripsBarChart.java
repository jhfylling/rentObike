package statistics;

import design.GuiColors;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import dto.StatsContainer;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The TripsBarChart class is used to create bar charts and data.
 */
public class TripsBarChart extends JPanel{
    /**
     * a constructor to create a new border layout for the bar chart.
     */
    public TripsBarChart() {
        super(new BorderLayout());

    }

    /**
     * @param statsContainer
     * @return a dataset
     */

    private CategoryDataset createDataset(StatsContainer statsContainer) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
        ArrayList<Integer> amoutOfTripsPerUser = statsContainer.getAmountOfTripsPerUser();
        dataset.addValue(amoutOfTripsPerUser.get(0),"Trips", "1");
        dataset.addValue(amoutOfTripsPerUser.get(1),"Trips", "2-5");
        dataset.addValue(amoutOfTripsPerUser.get(2), "Trips", "6-10");
        dataset.addValue(amoutOfTripsPerUser.get(3), "Trips", "10+");

        return dataset;
    }

    /**
     * @param statsContainer contains data
     * @return JFreeChart containing a bar chart
     */
    private JFreeChart createBarChart (StatsContainer statsContainer) {

        String charTitle = "Amount of Trips Per User";
        JFreeChart barChart = ChartFactory.createBarChart(charTitle, "Trips", "Users", createDataset(statsContainer));
        barChart.getPlot().setBackgroundPaint(GuiColors.chartColorFour);

        final CategoryPlot plot = barChart.getCategoryPlot();
        ((BarRenderer) plot.getRenderer()).setBarPainter(new StandardBarPainter());

        BarRenderer barRenderer = (BarRenderer) barChart.getCategoryPlot().getRenderer();
        barRenderer.setSeriesPaint(0, GuiColors.chartColorFive);
        return barChart;
    }

    /**
     * @param statsContainer contains data
     * @return JPanel containing a line bar chart
     */
    public JPanel createPanel(StatsContainer statsContainer) {
        JFreeChart chart = createBarChart(statsContainer);
        return new ChartPanel(chart);
    }

}
