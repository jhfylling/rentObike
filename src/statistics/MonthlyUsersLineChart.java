package statistics;

import design.GuiColors;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import dto.StatsContainer;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The MonthlyUsersLineChart class is used to create line charts in the gui.
 */
public class MonthlyUsersLineChart extends JPanel {
    /**
     * a constructor to create a new border layout for the line chart
     */
    public MonthlyUsersLineChart() {
        super(new BorderLayout());

    }
    /**
     * @param statsContainer
     * @return dataset
     */
    private DefaultCategoryDataset createDataset(StatsContainer statsContainer) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
        ArrayList<Integer> montlyUniqueUsers = statsContainer.getMonthlyNewUser();
        int month = 1;
        for(int i = 0; i < montlyUniqueUsers.size(); i ++){
            dataset.addValue(montlyUniqueUsers.get(i),"Trips", String.valueOf(month));
            month++;
        }

        return dataset;
    }

    /**
     * @param statsContainer
     * @return a JFreeChart containing a line chart
     */
    private JFreeChart createLineChart (StatsContainer statsContainer) {

        String chartTitle = "Monthly Unique Users";
        JFreeChart lineChart = ChartFactory.createLineChart( chartTitle,
                "Months", "Unique Users",
                createDataset(statsContainer),
                PlotOrientation.VERTICAL,
                true, true, false);

        lineChart.getPlot().setBackgroundPaint(GuiColors.chartColorFour);
        CategoryPlot plot = (CategoryPlot) lineChart.getPlot();
        plot.getRenderer().setSeriesPaint(0,GuiColors.WHITE);
        plot.getRenderer().setSeriesStroke(1, new BasicStroke(10.0f));
        return lineChart;
    }

    /**
     * @param statsContainer
     * @return a JPanel containing a lineChart
     */
    public JPanel createPanel(StatsContainer statsContainer) {
        JFreeChart chart = createLineChart(statsContainer);
        return new ChartPanel(chart);
    }

}
