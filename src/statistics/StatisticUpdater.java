package statistics;


import gui.Frame;
import panels.StatisticsPanel;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The StatisticUpdater class creates a thread that updates the statistics panel.
 */

public class StatisticUpdater implements Runnable {

    private Thread thread;
    private final StatisticsPanel statsPanel;

    /**
     * Creates a local reference to the statistics panel
     * @param statisticsPanel receives StatistisPanel
     */
    public StatisticUpdater(StatisticsPanel statisticsPanel){
        statsPanel = statisticsPanel;
    }

    /**
     * Creates a thread and starts it.
     */
    public void start(){
        if(thread == null){
            thread = new Thread(this,"SimThread");
            thread.start();
        }
    }

    /**
     * Contains the run loop that updates and repaints the statistics panel
     */
    @Override
    public void run() {
        while(Frame.shouldUpdate){
            statsPanel.updateStats();
            statsPanel.validate();
            statsPanel.repaint();
            try{
                thread.sleep(10000);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
