package statistics;

import design.GuiColors;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import dto.StatsContainer;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The EconomyBarChart class is used to create bar charts and data.
 */

public class EconomyBarChart extends JPanel{

    public static final double kWHPrice = 0.349;
    /**
     * a constructor to create a new border layout for the bar chart.
     */
    public EconomyBarChart() {
        super(new BorderLayout());

    }

    /**
     * @param statsContainer
     * @return a dataset
     */
    private CategoryDataset createDataset(StatsContainer statsContainer) {

        final DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
        ArrayList<Double> compareStats = statsContainer.getCompareSet();
        dataset.addValue(compareStats.get(0),"Amount", "Income");
        dataset.addValue(compareStats.get(1),"Amount", "Repair Costs");
        dataset.addValue(compareStats.get(2) * kWHPrice,"Amount", "Energy Costs");
        return dataset;
    }

    /**
     * @param statsContainer
     * @return JFreeChart containing a bar chart
     */
    private JFreeChart createBarChart (StatsContainer statsContainer) {

        String charTitle = "Economy";
        JFreeChart barChart = ChartFactory.createBarChart(charTitle, "", "Nok", createDataset(statsContainer));
        barChart.getPlot().setBackgroundPaint(GuiColors.chartColorFour);


        final CategoryPlot plot = barChart.getCategoryPlot();
        ((BarRenderer) plot.getRenderer()).setBarPainter(new StandardBarPainter());

        BarRenderer barRenderer = (BarRenderer) barChart.getCategoryPlot().getRenderer();
        barRenderer.setSeriesPaint(0, GuiColors.chartColorFive);
        return barChart;
    }

    /**
     * @param statsContainer
     * @return a JPanel containing a bar chart
     */
    public JPanel createPanel(StatsContainer statsContainer) {
        JFreeChart chart = createBarChart(statsContainer);
        return new ChartPanel(chart);
    }
}
