package statistics;

import design.GuiColors;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import dto.StatsContainer;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The MonthlyRepairsBarChart class is used to create bar charts and data.
 */

public class MonthlyRepairsBarChart extends JPanel{

    /**
     * a constructor to create a new border layout for the bar chart.
     */
    public MonthlyRepairsBarChart() {
        super(new BorderLayout());

    }

    /**
     * @param statsContainer contains data
     * @return a dataset
     */
    private CategoryDataset createDataset(StatsContainer statsContainer) {

        final String rep = "Repairs";
        DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
        ArrayList<Integer> repairdBikesPerMonth = statsContainer.getRepairedBikesPerMonth();
        int month = 1;
        for(Integer amountOfRepairsForAMonth : repairdBikesPerMonth){
            dataset.addValue(amountOfRepairsForAMonth,rep,String.valueOf(month));
            month++;
        }
        return dataset;
    }

    /**
     * @param statsContainer containts data
     * @return JFreeChart containing a barChart
     */
    private JFreeChart createBarChart (StatsContainer statsContainer) {

        String charTitle = "Monthly Repairs";
        JFreeChart barChart = ChartFactory.createBarChart(charTitle, "Month", "Number of Bikes", createDataset(statsContainer));
        barChart.getPlot().setBackgroundPaint(GuiColors.chartColorFour);

        final CategoryPlot plot = barChart.getCategoryPlot();
        ((BarRenderer) plot.getRenderer()).setBarPainter(new StandardBarPainter());

        BarRenderer barRenderer = (BarRenderer) barChart.getCategoryPlot().getRenderer();
        barRenderer.setSeriesPaint(0, GuiColors.chartColorFive);
        return barChart;
    }

    /**
     * @param statsContainer contains data
     * @return JPanel containing a barChart
     */
    public JPanel createPanel(StatsContainer statsContainer) {
        JFreeChart chart = createBarChart(statsContainer);
        return new ChartPanel(chart);
    }

}

