package statistics;

import design.GuiColors;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import dto.StatsContainer;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The HourlyTripsLineChart class is used to create line charts in the gui.
 */
public class HourlyTripsLineChart extends JPanel{
    /**
     * a constructor to create a new border layout for the line chart
     */
    public HourlyTripsLineChart() {
        super(new BorderLayout());

    }
    /**
     * @param statsContainer
     * @return dataset
     */
    private DefaultCategoryDataset createDataset(StatsContainer statsContainer) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
        ArrayList<Integer> hourlyTrips = statsContainer.getHourlyTrips();
        int hour = 2;
        for(int i = 0; i < hourlyTrips.size(); i++){
            dataset.addValue(hourlyTrips.get(i),"Trips",String.valueOf(hour));
            hour += 2;
        }

        return dataset;
    }
    /**
     * @param statsContainer
     * @return a JFreeChart containing a line chart
     */
    private JFreeChart createLineChart (StatsContainer statsContainer) {

        String chartTitle = "Hourly Trips";
        JFreeChart lineChart = ChartFactory.createLineChart( chartTitle,
                "Hours", "Trips",
                createDataset(statsContainer),
                PlotOrientation.VERTICAL,
                true, true, false);

        lineChart.getPlot().setBackgroundPaint(GuiColors.chartColorFour);
        CategoryPlot plot = (CategoryPlot) lineChart.getPlot();
        plot.getRenderer().setSeriesPaint(0,GuiColors.WHITE);
        plot.getRenderer().setSeriesStroke(1, new BasicStroke(10.0f));
        return lineChart;
    }

    /**
     * @param statsContainer
     * @return a JPanel with a line chart
     */
    public JPanel createPanel(StatsContainer statsContainer) {
        JFreeChart chart = createLineChart(statsContainer);
        return new ChartPanel(chart);
    }

}


