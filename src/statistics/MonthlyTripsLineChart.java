package statistics;

import java.awt.*;
import java.util.ArrayList;

import design.GuiColors;
import org.jfree.chart.*;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import dto.StatsContainer;

import javax.swing.*;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The MonthlyTripsLineChart class is used to create line charts in the gui.
 */

public class MonthlyTripsLineChart extends JPanel {

    /**
     * a constructor to create a new border layout for the line chart
     */
    public MonthlyTripsLineChart() {
        super(new BorderLayout());
    }
    /**
     * @param statsContainer
     * @return dataset
     */
    private DefaultCategoryDataset createDataset(StatsContainer statsContainer) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
        ArrayList<Integer> monthlyBikeTrip = statsContainer.getMonthlyBikeTrips();
        int month = 1;
        for(Integer bikeTripsForMonth : monthlyBikeTrip){
            dataset.addValue(bikeTripsForMonth,"Users",String.valueOf(month));
            month++;
        }
        return dataset;
    }

    /**
     * @param statsContainer
     * @return a JFreeChart containing a line chart
     */
   private JFreeChart createLineChart (StatsContainer statsContainer) {

       String chartTitle = "Monthly Bike Trips";
       JFreeChart lineChart = ChartFactory.createLineChart( chartTitle,
               "Month", "Trips",
               createDataset(statsContainer),
               PlotOrientation.VERTICAL,
               true, true, false);

       lineChart.getPlot().setBackgroundPaint(GuiColors.chartColorFour);
       CategoryPlot plot = (CategoryPlot) lineChart.getPlot();
       plot.getRenderer().setSeriesPaint(0,GuiColors.WHITE);
       plot.getRenderer().setSeriesStroke(1, new BasicStroke(8.0f));
       return lineChart;
   }

    /**
     * @param statsContainer
     * @return a JPanel with a line chart
     */
   public JPanel createPanel(StatsContainer statsContainer) {
        JFreeChart chart = createLineChart(statsContainer);
        return new ChartPanel(chart);
    }
}
