package design;

import java.awt.*;
/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The GuiColors class is used to manage different colors used in the Gui.
 */

public class GuiColors {


    /**
     * Forest green color
     */
    public final static Color menuColor = new Color (61, 150, 82);
    /**
     * Light grey color
     */
    public final static Color listColor = new Color(248,248,248);

    /**
     * White color
     */
    public final static Color focusColor = new Color(255,255,255);
    /**
     * Black color
     */
    public final static Color focusTextColor = new Color(0,0,0);
    /**
     * Soft blue color
     */
    public static final Color chartColorFour = new Color(89, 118, 142);
    /**
     * Smooth red color
     */
    public static final Color chartColorFive = new Color(224, 100, 78);
    /**
     * Dark color
     */
    public static final Color DARK = new Color(65, 70, 79);
    /**
     * Odd dark color
     */
    public static final Color ODD_DARK = new Color(74, 78, 89);
    /**
     * Black color
     */
    public static final Color BLACK = new Color(45, 47, 51);
    /**
     * Grey text color
     */
    public static final Color GRAY_TEXT = new Color(184, 185, 188);
    /**
     * White color
     */
    public static final Color WHITE = new Color(255, 255, 255);
    /**
     * Red color
     */
    public static final Color RED = new Color(255,0,0);

}
