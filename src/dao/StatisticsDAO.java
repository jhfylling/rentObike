package dao;

import dto.Bike;
import sql.Cleaner;
import dto.StatsContainer;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The Statistics class is used to get statistical information.
 */

public class StatisticsDAO {
    Connection con;
    PreparedStatement statement;

    /**
     * This is a constructor to get a connection to the class from the database
     */
    public StatisticsDAO(Connection con) {
        this.con = con;
    }
    /**
     * Creates a StatsContainer and inserts information into it
     */

    public StatsContainer getStatscontainer(){
        StatsContainer statsContainer = new StatsContainer();
        statsContainer.setAmountOfTripsPerUser(getAmountOfTripsPerUser());
        statsContainer.setRepairedBikesPerMonth(getRepairedBikesPerMonth());
        statsContainer.setCompareSet(getCompareSet());
        statsContainer.setMonthlyBikeTrips(getMonthlyBikeTrips());
        statsContainer.setMonthlyNewUser(getMonthlyNewUser());
        statsContainer.setHourlyTrips(getHourlyTrips());
        statsContainer.setEarnings(getTotaltEarned());
        statsContainer.setNumberOfUsers(getNumberOfUsers());
        statsContainer.setNumberOfTrips(getNumberOfTrips());
        statsContainer.setOnlyBeenOnOneTrip(onlyBeenOnOneTrip());
        statsContainer.setTotalRepairCost(getTotalRepairCosts());
        statsContainer.setPowerUsage(getPowerUsage());
        return statsContainer;
    }

    /**
     * Creates an ArrayList with Amount of trips per User info.
     * @return ArrayList of integers with trip data
     */
    public ArrayList<Integer> getAmountOfTripsPerUser(){
        ArrayList<Integer> amountOfTripsPerUser = new ArrayList<>();
        amountOfTripsPerUser.add(onlyBeenOnOneTrip());
        amountOfTripsPerUser.add(betweenTwoAndFiveTrips());
        amountOfTripsPerUser.add(betweenSixAndTenTrips());
        amountOfTripsPerUser.add(moreThenTenTrips());
        return amountOfTripsPerUser;
    }

    /**
     * Creates a ArrayList containing monthly bike repairs
     * @return ArrayList with monthly bikes repairs
     */
    private ArrayList<Integer> getRepairedBikesPerMonth() {
        ArrayList<Integer> repairedBikesPerMonth = new ArrayList<>();
        ArrayList<Date> months = getMonths();
        for(int i = 0; i < months.size()-1; i += 2){
            repairedBikesPerMonth.add(getRepairedBikes(months.get(i),months.get(i+1)));
        }
        return repairedBikesPerMonth;
    }

    /**
     * Creates a ArrayList with selected stats
     * @return ArrayList with selected stats
     */
    private ArrayList<Double> getCompareSet(){
        ArrayList<Double> compareSet = new ArrayList<>();
        compareSet.add(getTotaltEarned());
        compareSet.add(getTotalRepairCosts());
        compareSet.add(getPowerUsage());
        return compareSet;
    }

    /**
     * Creates a ArrayList with monthly biek trips stats
     * @return ArrayList with monthly bike trips stats
     */
    private ArrayList<Integer> getMonthlyBikeTrips(){
        ArrayList<Integer> monthlyBiketrips = new ArrayList<>();
        ArrayList<Date> months = getMonths();
        for(int i = 0; i < months.size()-1; i += 2){
            monthlyBiketrips.add(getTripsInPeriode(months.get(i),months.get(i+1)));
        }
        return monthlyBiketrips;
    }

    /**
     * Creates a ArrayList with monthly user stats
     * @return a ArrayList with monthly user stats
     */
    private ArrayList<Integer> getMonthlyNewUser(){
        ArrayList<Integer> monthlyNewUser = new ArrayList<>();
        ArrayList<Date> months = getMonths();
        for(int i = 0; i < months.size()-1; i+=2){
            monthlyNewUser.add(getUserForMonth(months.get(i), months.get(i+1)));
        }
        return monthlyNewUser;
    }

    /**
     * Creates a ArrayList with hourly trips taken
     * @return ArrayList with hourly trips taken
     */
    private ArrayList<Integer> getHourlyTrips(){
        ArrayList<Integer> hourlyTrips = new ArrayList<>();
        ArrayList<String> hours = createTimeStamps();
        for(int i = 0; i < hours.size()-1; i+=2){
            hourlyTrips.add(getTripsPerHour(hours.get(i),hours.get(i+2)));
        }
        return hourlyTrips;
    }




    /**
     * Get the bike used ratio in percentage
     * @return percentage of bikes currently on a trip
     */
    public int getBikeUseRatio() {
        //get this number from the database

        BikeDAO bikef = new BikeDAO(con);

        ArrayList<Bike> bikes = bikef.getBikes();
        int bikesInUse = 0;
        int res = 0;

        for (int i = 0; i < bikes.size(); i++) {

            if (bikef.getDock(bikes.get(i).getBike_id()) == 0) {
                bikesInUse ++;
            }
        }
        res = bikesInUse/bikes.size()*100;
        return res;
    }

    /**
     * Get the amount of repaired bikes between two dates of the users choosing
     * @param start the start date
     * @param end the end date
     * @return the amount of repairs between the two dates
     */
    public int getRepairedBikes(java.sql.Date start, java.sql.Date end) {

        ResultSet res = null;
        int amountOfRepairs = 0;

        try {
            statement = con.prepareStatement("SELECT COUNT(repair_id) FROM repairs WHERE date_Sent BETWEEN ? AND ?");
            statement.setDate(1, start);
            statement.setDate(2, end);
            res = statement.executeQuery();

            while(res.next()) {
                amountOfRepairs = res.getInt(1);
            }

        } catch (SQLException e) {
            Cleaner.errorMessage(e,"Staststics getRepairedBikes");
        }
        finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return amountOfRepairs;
    }

    /**
     * Get the amount of trips for bikes between two dates of the users choosing
     * @param start start date
     * @param end end date
     * @return the amount of trips between the two dates
     */
    public int getTripsInPeriode(java.sql.Date start, java.sql.Date end) {

        ResultSet res = null;
        int amountOfTrips = 0;

        try {
            statement = con.prepareStatement("SELECT COUNT(trip_id) FROM trip WHERE start_time BETWEEN ? AND ?");
            statement.setDate(1, start);
            statement.setDate(2, end);
            res = statement.executeQuery();

            while(res.next()) {
                amountOfTrips = res.getInt(1);
            }

        } catch (SQLException e) {
            Cleaner.errorMessage(e,"Staststics getTripsInPeriode");
        }
        finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return amountOfTrips;
    }

    /**
     * Finds amount of trips in a time period
     * @param start time
     * @param end time
     * @return amount of trips in a tie period
     */
    public int getTripsPerHour(String start, String end){
        ResultSet res = null;
        try{
            String sentence = "SELECT COUNT(trip_id) as numberOfTrips from trip WHERE cast(start_time as time) BETWEEN (?) and (?);";
            statement = con.prepareStatement(sentence);
            statement.setString(1,start);
            statement.setString(2,end);
            res = statement.executeQuery();
            if(res.next()) return res.getInt("numberOfTrips");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return 0;
    }

    /**
     * Finds active users for a month
     * @param start date
     * @param end date
     * @return amount of active user for a month
     */
    public int getUserForMonth(Date start, Date end){
        ResultSet res = null;
        try{
            String sentence = "SELECT count(DISTINCT user_id) as activeUsers FROM trip WHERE start_time BETWEEN ? AND ?";
            statement = con.prepareStatement(sentence);
            statement.setDate(1,start);
            statement.setDate(2,end);
            res = statement.executeQuery();
            if(res.next()) return res.getInt("activeUsers");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return 0;
    }

    /**
     *
     * @return income from payed trips.
     */
    public double getTotaltEarned(){
        ResultSet res = null;
        try{
            String sentence = "SELECT SUM(amount) as earnings FROM transaction";
            statement = con.prepareStatement(sentence);
            res = statement.executeQuery();
            if(res.next()){
                return res.getDouble("earnings");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    /**
     *
     * @return number of unique users
     */
    public int getNumberOfUsers(){
        ResultSet res = null;
        try{
            String sentence = "Select COUNT(user_id) as numberOfUsers FROM users";
            statement = con.prepareStatement(sentence);
            res = statement.executeQuery();
            if(res.next()){
                return res.getInt("numberOfUsers");
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return 0;
    }

    /**
     *
     * @return total number of trips started.
     */
    public int getNumberOfTrips(){
        ResultSet res = null;
        try{
            String sentence = "SELECT COUNT(trip_id) as numberOfTrips FROM trip";
            statement = con.prepareStatement(sentence);
            res = statement.executeQuery();
            if(res.next()){
                return res.getInt("numberOfTrips");
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return 0;
    }

    /**
     *
     * @return amount of users with more then 10 trips
     */
    public int moreThenTenTrips(){
        ResultSet res = null;
        int count = 0;
        try{
            String sentence = "SELECT COUNT(trip_id) as numberOfTripsPerUser FROM trip GROUP BY user_id";
            statement = con.prepareStatement(sentence);
            res = statement.executeQuery();
            while(res.next()){
                if(res.getInt("numberOfTripsPerUser") > 10) count ++;
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return count;
    }

    /**
     *
     * @return amount of users with between six and ten trips
     */
    public int betweenSixAndTenTrips(){
        ResultSet res = null;
        int count = 0;
        try{
            String sentence = "SELECT COUNT(trip_id) as numberOfTripsPerUser FROM trip GROUP BY user_id";
            statement = con.prepareStatement(sentence);
            res = statement.executeQuery();
            while(res.next()){
                if(res.getInt("numberOfTripsPerUser") > 5 && res.getInt("numberOfTripsPerUser") < 11) count ++;
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return count;
    }

    /**
     *
     * @return amount of users with between two and five trips
     */
    public int betweenTwoAndFiveTrips(){
        ResultSet res = null;
        int count = 0;
        try{
            String sentence = "SELECT COUNT(trip_id) as numberOfTripsPerUser FROM trip GROUP BY user_id";
            statement = con.prepareStatement(sentence);
            res = statement.executeQuery();
            while(res.next()){
                if(res.getInt("numberOfTripsPerUser") > 1 && res.getInt("numberOfTripsPerUser") < 6) count ++;
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return count;
    }

    /**
     *
     * @return number of users that have only been on one trip.
     */
    public int onlyBeenOnOneTrip(){
        ResultSet res = null;
        int count = 0;
        try{
            String sentence = "SELECT COUNT(trip_id) as numberOfTripsPerUser FROM trip GROUP BY user_id";
            statement = con.prepareStatement(sentence);
            res = statement.executeQuery();
            while(res.next()){
                if(res.getInt("numberOfTripsPerUser") == 1) count ++;
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return count;
    }

    /**
     *
     * @return total bike repair costs
     */
    public double getTotalRepairCosts(){
        ResultSet res = null;
        try{
            String sentence = "SELECT SUM(price) as repairCosts FROM repairs";
            statement = con.prepareStatement(sentence);
            res = statement.executeQuery();
            if(res.next()) return res.getDouble("repairCosts");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return 0;
    }

    /**
     *
     * @return totalt power usage for all docking stations
     */
    public double getPowerUsage(){
        ResultSet res = null;
        try{
            String sentence = "SELECT SUM(power_usage) as totalUsage FROM dock";
            statement = con.prepareStatement(sentence);
            res = statement.executeQuery();
            if(res.next()) {
                double pwrUsage = res.getDouble("totalUsage");
                System.out.println(pwrUsage);
                return pwrUsage;
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return 0;
    }

    /**
     *
     * @return Arraylist with start and end dates for very month in 2018
     */
    private ArrayList<Date> getMonths(){
        ArrayList<Date> months = new ArrayList<>();
        months.add(Date.valueOf("2018-01-01"));
        months.add(Date.valueOf("2018-01-31"));
        months.add(Date.valueOf("2018-02-01"));
        months.add(Date.valueOf("2018-02-28"));
        months.add(Date.valueOf("2018-03-01"));
        months.add(Date.valueOf("2018-03-31"));
        months.add(Date.valueOf("2018-04-01"));
        months.add(Date.valueOf("2018-04-30"));
        months.add(Date.valueOf("2018-05-01"));
        months.add(Date.valueOf("2018-05-31"));
        months.add(Date.valueOf("2018-06-01"));
        months.add(Date.valueOf("2018-06-30"));
        months.add(Date.valueOf("2018-07-01"));
        months.add(Date.valueOf("2018-07-31"));
        months.add(Date.valueOf("2018-08-01"));
        months.add(Date.valueOf("2018-08-31"));
        months.add(Date.valueOf("2018-09-01"));
        months.add(Date.valueOf("2018-09-30"));
        months.add(Date.valueOf("2018-10-01"));
        months.add(Date.valueOf("2018-10-31"));
        months.add(Date.valueOf("2018-11-01"));
        months.add(Date.valueOf("2018-11-30"));
        months.add(Date.valueOf("2018-12-01"));
        months.add(Date.valueOf("2018-12-31"));
        return months;
    }

    /**
     *
     * @return ArrayList with Strings for every hour
     */
    public ArrayList<String> createTimeStamps(){
        ArrayList<String> times = new ArrayList<>();
        String start = "00:00:00";
        String next;
        times.add(start);
        for(int i = 1; i < 25; i ++){
            if(i < 10){
                next = "0" + i + start.substring(2,start.length());
            }else {
                next = i + start.substring(2,start.length());
            }

            times.add(next);
        }
        return times;
    }

    /**
     * Close the connection to the database
     */
    public void close(){

    }
}
