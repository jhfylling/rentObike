package dao;

import dto.Bike;
import dto.BikeType;
import sql.Cleaner;
import dto.DockingStation;
import sql.DBconnection;

import java.sql.Date;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The BikeDAO class is used to control and manage bikes in the system.
 */

public class BikeDAO {
    //private ArrayList<Bike> bikes = new ArrayList<>();
    private Connection con;

    /**
     * This is a constructor made to get access to all bikes
     * @param con connection to the database
     */
    public BikeDAO(Connection con){
        this.con = con;
    }

    /**
     * Get connection to the database
     * @return connection to the database
     */
    public Connection getCon() {
        return con;
    }

    /**
     * Get bike types from the database
     * @return an array list with bike types
     */
    public ArrayList<BikeType> getTypes(){
        PreparedStatement statement = null;
        ResultSet res = null;
        ArrayList<BikeType> types = new ArrayList<>();

        try{
            String sentence = "SELECT * FROM type WHERE isEnabled = TRUE";
            statement = con.prepareStatement(sentence);
            res = statement.executeQuery();
            while(res.next()){
                types.add(new BikeType(res.getInt("type_id"), res.getString("description")));
            }
            return types;

        }catch (SQLException e){
            e.printStackTrace();
        }finally{
            Cleaner.closeRes(res);
            Cleaner.closeStatement(statement);
        }
        return types;
    }

    /**
     * Disable type
     * @param id
     * @return true or false
     */
    public boolean disableType(int id){
        String sqlQuery = "UPDATE type SET isEnabled = FALSE WHERE type_id = ?";
        try(PreparedStatement stmt = con.prepareStatement(sqlQuery)){
            stmt.setInt(1,id);
            if(stmt.executeUpdate() > 0){
                return true;
            }
        }catch (SQLException se){
            System.out.println(se + "Delete type");
        }
        return false;
    }

    /**
     * Add a bike to the system
     * @param make the initial make
     * @param price the initial price
     * @param mileage the bikes mileage
     * @param charge_level the bikes charge level
     * @param type_id type ID for the bikes type
     * @param dock_id dock ID for where the bike is placed
     * @return true if the registration was a success and false if something went wrong
     */
    public boolean addBike(String make, double price, int mileage, int charge_level, int type_id, int dock_id){
        PreparedStatement statement = null;
        ResultSet res = null;
        int bike_id;

        try{
            con.setAutoCommit(false);
            String sentence = "INSERT INTO bike(make, price, mileage, charge_level, date_bought, type_id, dock_id) VALUES(?,?,?,?,CURDATE(),?,?)";
            statement = con.prepareStatement(sentence);
            statement.setString(1, make);
            statement.setDouble(2, price);
            statement.setInt(3, mileage);
            statement.setInt(4, charge_level);
            statement.setInt(5, type_id);
            statement.setInt(6, dock_id);
            statement.execute();

            sentence = "SELECT MAX(bike_id) AS bike_id FROM bike";
            statement = con.prepareStatement(sentence);
            res = statement.executeQuery();
            if(res.next()){
                bike_id = res.getInt("bike_id");

                sentence = "INSERT INTO position VALUES(DEFAULT, (SELECT latitude FROM dock WHERE dock_id=?), (SELECT longitude FROM dock WHERE dock_id=?), CURRENT_TIMESTAMP, DEFAULT, ?)";
                statement = con.prepareStatement(sentence);
                statement.setInt(1, dock_id);
                statement.setInt(2, dock_id);
                statement.setInt(3, bike_id);
                statement.execute();
                con.commit();
            }
            return true;
        }catch (SQLException e){
            e.printStackTrace();
            if(con != null){
                try{
                    con.rollback();
                }catch (SQLException excep){
                    System.out.println(excep);
                }
            }
            return false;
        }finally{
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
            Cleaner.settAutoCommit(con);
        }
    }

    /**
     * A method to disable a bike, it will remove the bike from the list but it will not disappear from the database.
     * @param index bike index
     * @return true if the bike was disabled and false if something went wrong
     */
    public boolean disableBike(int index){
        String sqlQuery = "UPDATE bike SET isEnabled = false WHERE bike_id = ?";
        try(PreparedStatement stmt = con.prepareStatement(sqlQuery)){
            stmt.setInt(1,index);
            if(stmt.executeUpdate() > 0){
                return true;
            }
        }catch (SQLException se){
            System.out.println(se + "disableBike()");
        }
        return false;
    }

    /**
     * Edit information about bike
     * @param make
     * @param chargelvl
     * @param price
     * @param id
     * @return true
     */
    public boolean editBike(int id, String make, int chargelvl, double price, int type_id, int dock_id){
        String sqlQuery ="UPDATE bike SET  make = ?,charge_level = ?, price = ?, type_id = ?, dock_id = ?  WHERE bike_id = ?";
        try(PreparedStatement stmt = con.prepareStatement(sqlQuery)){
            stmt.setString(1,make);
            stmt.setInt(2,chargelvl);
            stmt.setDouble(3,price);
            stmt.setInt(4,type_id);
            stmt.setInt(5,dock_id);
            stmt.setInt(6,id);
            stmt.executeUpdate();
            return true;
        }catch (SQLException e){
            System.out.println(e +"Error: editBike()");
        }
        return false;
    }


    /**
     * Get every bike in the system
     * @return an array list with bikes
     */
    public ArrayList getBikes(){
        PreparedStatement statement = null;
        ResultSet res = null;
        Bike bike = null;
        ArrayList bikes = new ArrayList();
        try{
            //Statement selects all bikes and their position and their docking information
            //only selects bikes that are enabled.
            String sentence = "SELECT b.*, t.description, d.dock_name, d.power_usage, d.max_bikes, d.longitude AS dockLong, d.latitude AS dockLat, p.latitude, p.longitude FROM bike b\n" +
                    "  LEFT JOIN position p ON b.bike_id = p.bike_id\n" +
                    "  LEFT JOIN type t ON t.type_id = b.type_id\n" +
                    "  LEFT JOIN dock d ON d.dock_id = b.dock_id\n" +
                    "WHERE b.isEnabled = true AND (p.time_position = (SELECT max(p.time_position) FROM position p WHERE b.bike_id = p.bike_id) OR p.time_position IS NULL)" +
                    "ORDER BY b.bike_id";

            statement = con.prepareStatement(sentence);
            res = statement.executeQuery();
            DockingStation temp = null;
            while(res.next()) {
                if (res.getInt("dock_id") != 0){
                    temp = new DockingStation(res.getInt("dock_id"), res.getString("dock_name"),
                            res.getInt("power_usage"), res.getInt("max_bikes"), res.getDouble("dockLat"), res.getDouble("dockLong"), con);
                }
                else{
                    temp = new DockingStation(0, "Not in dock", 0, 5000,5,5, con);
                }
                bike = new Bike(res.getInt("bike_id"), new BikeType(res.getInt("type_id"),
                        res.getString("description")), res.getString("make"),
                        res.getInt("price"), res.getInt("mileage"),
                        res.getInt("charge_level"),res.getDate("date_bought"),
                        temp, con, res.getDouble("latitude"), res.getDouble("longitude"));
                bikes.add(bike);
            }
            return bikes;

        }catch (SQLException e){
            e.printStackTrace();
        }finally{
            Cleaner.closeRes(res);
            Cleaner.closeStatement(statement);
        }
        return null;
    }


    /**
     * Get a specific bike
     * @param bike_id bike ID for the bike you want
     * @return a bike object
     */
    public Bike getBike(int bike_id){
        PreparedStatement statement = null;
        ResultSet res = null;
        Bike bike = null;
        try{

            String sentence = "SELECT b.*, t.description, d.dock_name, d.power_usage, d.max_bikes, d.latitude AS dockLat, d.longitude AS dockLong, p.latitude, p.longitude FROM bike b JOIN type t ON t.type_id = b.type_id LEFT JOIN position p on b.bike_id = p.bike_id LEFT JOIN dock d ON d.dock_id = b.dock_id WHERE (p.time_position = (SELECT max(p.time_position) FROM position p WHERE b.bike_id = p.bike_id) AND b.bike_id = ?) OR (p.time_position IS NULL AND b.bike_id = ?)";

            statement = con.prepareStatement(sentence);
            statement.setInt(1, bike_id);
            statement.setInt(2, bike_id);

            res = statement.executeQuery();
            DockingStation temp = null;
            while(res.next()) {
                if (res.getInt("dock_id") != 0){
                    temp = new DockingStation(res.getInt("dock_id"), res.getString("dock_name"),
                            res.getInt("power_usage"), res.getInt("max_bikes"), res.getDouble("dockLat"), res.getDouble("dockLong"), con);
                }
                bike = new Bike(res.getInt("bike_id"), new BikeType(res.getInt("type_id"),
                        res.getString("description")), res.getString("make"),
                        res.getInt("price"), res.getInt("mileage"),
                        res.getInt("charge_level"),res.getDate("date_bought"),
                        temp, con, res.getDouble("latitude"), res.getDouble("longitude"));
            }
            return bike;

        }catch (SQLException e){
            e.printStackTrace();
        }finally{
            Cleaner.closeRes(res);
            Cleaner.closeStatement(statement);
        }
        return null;
    }

    /**
     * Get the repair history for a specific bike
     * @param bike_id bike ID of the initial bike
     * @return a string array with repair history of the bike
     */
    public ArrayList<String> getRepairHistory(int bike_id){
        PreparedStatement statement = null;
        ResultSet res = null;
        ArrayList<String> history = new ArrayList<>();
        try{
            String sentence = "SELECT * FROM repairs WHERE bike_id = ?";
            statement = con.prepareStatement(sentence);
            statement.setInt(1,bike_id);
            res = statement.executeQuery();
            while(res.next()){
                history.add("Date sent: " + res.getDate("date_sent") +
                        ";Date received: " + res.getDate("date_recieved") + ";Request: " + res.getString("request_description") +
                        ";Work done: " + res.getString("repair_description"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return history;
    }

    /**
     * Get the number of trips of a bike
     * @param bike_id bike ID of the initial bike
     * @return number of trips of a bike
     */
    public int numberOfTrips(int bike_id){
        PreparedStatement statement = null;
        ResultSet res = null;
        try{
            String sentence = "SELECT COUNT(trip_id) as trips FROM trip WHERE bike_id = ?";
            statement = con.prepareStatement(sentence);
            statement.setInt(1,bike_id);
            res = statement.executeQuery();
            if(res.next()) return res.getInt("trips");

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return 0;
    }

    /**
     * Get the distance traveled inn kms of a bike
     * @param bike_id bike ID of the initial bike
     * @return distance traveled in kms of a bike
     */
    public int numberOfKms(int bike_id){
        PreparedStatement statement = null;
        ResultSet res = null;
        try{
            String sentence = "SELECT SUM(distance) as length FROM trip WHERE bike_id = ?";
            statement = con.prepareStatement(sentence);
            statement.setInt(1,bike_id);
            res = statement.executeQuery();
            if(res.next()) return res.getInt("length");

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return 0;
    }
    /**
     * Get the docking station the bike is located at. If its not in a docking station it returns -1
     * @param bike_id the initial bike ID you want to look up
     * @return docking station ID
     */
    public int getDock(int bike_id){
        PreparedStatement statement = null;
        ResultSet res = null;

        try{
            String sentence = "SELECT d.dock_id FROM dock d JOIN bike b ON d.dock_id = b.dock_id WHERE b.bike_id = ?";
            statement = con.prepareStatement(sentence);
            statement.setInt(1, bike_id);
            res = statement.executeQuery();
            if(res.next()){
                int dockid = res.getInt("dock_id");
                return dockid;
            }

        }catch (SQLException e){
            e.printStackTrace();
        }finally{
            Cleaner.closeRes(res);
            Cleaner.closeStatement(statement);
        }
        return -1;
    }

    /**
     * Register a new repair for the initial bike
     * @param date date sent to repair
     * @param description description of the repair
     * @param bike_id bike id of the initial bike
     * @return true if the registration was a success and false if something went wrong
     */
    public boolean registerNewRepair(Date date, String description, int bike_id){
        PreparedStatement statement = null;
        ResultSet res = null;
        try{
            String sentence = "INSERT INTO repairs (repair_id, date_sent, request_description, bike_id) VALUES (DEFAULT,?,?,?);";
            statement = con.prepareStatement(sentence);
            statement.setDate(1,date);
            statement.setString(2,description);
            statement.setInt(3,bike_id);
            statement.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return true;
    }

    /**
     * Registers that a bike is repaired
     * @param date date finished
     * @param description description of what was done to the bike
     * @param bike_id the initial bike ID
     * @param price cost of the repair
     * @return true if the registration was a success and false if something went wrong
     */
    public boolean registerFinishedRepair(Date date, String description, int bike_id, double price){
        PreparedStatement statement = null;
        ResultSet res = null;
        try{
            String sentence = "UPDATE repairs SET date_recieved = ?, repair_description = ?, price = ? WHERE bike_id = ? and repair_description IS NULL";
            statement = con.prepareStatement(sentence);
            statement.setDate(1,date);
            statement.setString(2,description);
            statement.setDouble(3,price);
            statement.setInt(4,bike_id);
            statement.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return true;
    }

    /**
     * Save a new bike type to the sql database
     * @param description bike type description
     * @return returns the new type id
     */
    public int saveNewType(String description){
        PreparedStatement statement = null;
        ResultSet res = null;
        int isolation = Connection.TRANSACTION_REPEATABLE_READ;
        int type_id = -1;
        try{
            con.setAutoCommit(false);
            isolation = con.getTransactionIsolation();
            con.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            String sentence = "INSERT INTO type (description) VALUES (?)";
            statement = con.prepareStatement(sentence);
            statement.setString(1, description);

            if(statement.executeUpdate() >= 1){
                sentence = "SELECT MAX(type_id) FROM type";
                statement = con.prepareStatement(sentence);
                res = statement.executeQuery();
                if(res.next()){
                    type_id = res.getInt(1);
                }
                else{
                    con.rollback();
                    return type_id;
                }
            }
            else{
               con.rollback();
               return type_id;
            }
            con.commit();
            return type_id;
        }catch(SQLException e){
            e.printStackTrace();
            return type_id;
        }finally {
            try {
                con.setTransactionIsolation(isolation);
            }catch(SQLException e){
                System.out.println("Could not set transaction isolation level back to default");
                e.printStackTrace();
            }
            Cleaner.settAutoCommit(con);
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
    }

    /**
     * Find last users ID
     * @param bike_id
     * @return last users ID or -1 if bike has not been used.
     */
    public int lastUsedById(int bike_id){
        PreparedStatement statement = null;
        ResultSet res = null;
        try{
            String sentence = "SELECT user_id FROM trip WHERE start_time = (SELECT MAX(start_time) FROM trip WHERE bike_id = ?) and bike_id = ?";
            statement = con.prepareStatement(sentence);
            statement.setInt(1,bike_id);
            statement.setInt(2,bike_id);
            res = statement.executeQuery();
            if(res.next()) return res.getInt("user_id");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return -1;
    }

    /**
     *
     * @param bike_id
     * @return Arraylist containing user history for a bike
     */
    public ArrayList<String> getUserHistory(int bike_id){
        System.out.println(bike_id);
        PreparedStatement statement = null;
        ResultSet res = null;
        ArrayList<String> userHistory = new ArrayList<>();
        String aHistory;
        try{
            String sentence = "SELECT user_id, start_time, end_time FROM trip WHERE bike_id = ?";
            statement = con.prepareStatement(sentence);
            statement.setInt(1,bike_id);
            res = statement.executeQuery();
            while (res.next()){
                aHistory = "User ID " + res.getInt("user_id") + "<br> Trip started: " + res.getTimestamp("start_time") +
                        "<br>Trips Ended: " + res.getTimestamp("end_time");
                userHistory.add(aHistory);
                System.out.println(aHistory);

            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return userHistory;
    }
}