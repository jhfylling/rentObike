package dao;
import registration.HashGenerator;
import registration.MailSender;
import registration.RandomString;
import dto.Admin;
import sql.Cleaner;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The AdminDAO class is used to control and manage administrators in the system.
 */

public class AdminDAO {
    private Connection con;

    private MailSender mailSender = new MailSender();
    private RandomString randomStringGen = new RandomString();
    private HashGenerator hashGenerator = new HashGenerator();
    private String randomGenPassword;

    /**
     * This is a constructor made to get access to all administrators
     * @param con connection to the database
     */
    public AdminDAO(Connection con){
        this.con = con;
    }

    /**
     * Collects admin users from database
     * @return a arrayList of admins
     */
    public ArrayList<Admin> getAllAdmin(){
        ArrayList<Admin> adminlist = new ArrayList<>();
        ResultSet res = null;
        String sqlQuery = "SELECT * FROM admin WHERE isEnabled = true;";
        try(PreparedStatement stmt = con.prepareStatement(sqlQuery)){
            res = stmt.executeQuery();
            while(res.next()){
                int admin_id = res.getInt("admin_id");
                String email = res.getString("email");
                String admin_name = res.getString("admin_name");
                adminlist.add(new Admin(admin_id,email,admin_name,con));
            }
            return adminlist;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            Cleaner.closeRes(res);
        }
        return null;
    }
    
    /**
     * Disables admin from jTable
     * @param index
     * @return true
     */
    public boolean disableAdmin(int index){
        String sqlQuery = "UPDATE admin SET isEnabled = false WHERE admin_id = ?";
        try(PreparedStatement stmt = con.prepareStatement(sqlQuery)){
            stmt.setInt(1,index);
            stmt.executeUpdate();
            return true;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    /**
    * edit an administrator information
    * @param email new administrator e-mail
    * @param adminname new administrator name
    * @param adminId new administrator ID
    * @return true if the update was a success and false if something went wrong
    */
    public boolean editadmin(String email, String adminname, int adminId){
        String sqlQuery = "UPDATE admin SET email = ?,admin_name = ? WHERE admin_id = ?";
        try(PreparedStatement stmt = con.prepareStatement(sqlQuery)){
            stmt.setString(1,email);
            stmt.setString(2,adminname);
            stmt.setInt(3,adminId);
            stmt.executeUpdate();

            return true;
        }catch (SQLException e){
            System.out.println(e + "Error : edit Admin");
        }
        return false;
    }

    /**
     * Registere a new administrator to the system
     * @param input an string array with administrator information
     * @return return true if the registration is complete and false if something went wrong
     */
    public boolean registerNewAdmin(ArrayList<String> input){
        String emailInput = input.get(0);
        randomGenPassword = randomStringGen.nextString();
        String saltAndHash[] = hashGenerator.getSaltAndHash(randomGenPassword);
        String nameInput = input.get(1);

        if(emailInput.length() < 5) return false;

        //check if E-mail is in use
        ResultSet res = null;
        String sentence = "SELECT email FROM admin WHERE email = ? AND isEnabled IS TRUE";
        try(PreparedStatement stmt = con.prepareStatement(sentence)){
            stmt.setString(1, emailInput);
            res=stmt.executeQuery();
            if(res.next()){
                //E-mail all ready in use
                return false;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally {
            Cleaner.closeRes(res);
        }

        //register admin
        res = null;
        String sqlQuery = "INSERT INTO admin VALUES (DEFAULT, ?,?,?,?,?)";
        try(PreparedStatement stmt = con.prepareStatement(sqlQuery)){
            stmt.setString(1,emailInput);
            stmt.setString(2,saltAndHash[0]);
            stmt.setString(3,saltAndHash[1]);
            stmt.setString(4,nameInput);
            stmt.setBoolean(5,true);
            stmt.executeUpdate();
            mailSender.sendRegistrationEmail(input.get(0),randomGenPassword, nameInput);
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeRes(res);
        }
        return false;
    }

    /**
     * Retrieve password hash from database for given admin user E-mail address
     * @param email login E-mail
     * @return stored passwordhash
     */
    public String getPasswordFromDB(String email){
        PreparedStatement statement = null;
        ResultSet res = null;
        try{
            String sentence = "SELECT * FROM admin WHERE email = ? AND isEnabled IS TRUE";
            statement = con.prepareStatement(sentence);
            statement.setString(1,email);
            res = statement.executeQuery();
            if(res.next()){
                return HashGenerator.iterations + ":" + res.getString("salt") + ":" + res.getString("hashed_pw");
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return "";
    }

    /**
     * Change admin's password
     * @param email Login E-mail
     * @param salt new salt
     * @param hash new password hash
     * @return true if password has been changed
     */
    public boolean changePassword(String email, String salt, String hash){
        PreparedStatement statement= null;
        ResultSet res = null;
        try{
            String sentence ="UPDATE admin SET salt = ?, hashed_pw = ? WHERE email = ? AND isEnabled IS TRUE";
            statement = con.prepareStatement(sentence);
            statement.setString(1,salt);
            statement.setString(2,hash);
            statement.setString(3,email);
            statement.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return true;
    }

    /**
     * Checks if email is registered
     * Generates new password
     * Sends forgotten password mail
     * @param email
     * @return true or false
     */
    public boolean sendNewPassword(String email){
        String adminName = adminExists(email);
        String newPassword = getNewPassword(email);
        if( adminName != null && newPassword != null){
            mailSender.sendFogottenPasswordMail(adminName,email,newPassword);
            return true;
        }
        return false;
    }

    /**
     * Helping method for sendNewPassword
     * @param email
     * @return
     */
    private String adminExists(String email){
        PreparedStatement statement = null;
        ResultSet res = null;
        try{
            String sentence = "Select * FROM admin WHERE email = ?";
            statement = con.prepareStatement(sentence);
            statement.setString(1,email);
            res = statement.executeQuery();
            if(res.next()){
                return res.getString("admin_name");
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
         Cleaner.closeStatement(statement);
         Cleaner.closeRes(res);
        }
        return null;
    }

    /**
     * Helping mehtod for sendNewPassword
     * Creates new passwords, hashes it and registers hash and salt in DB.
     * @param email
     * @return
     */
    public String getNewPassword(String email){
        String newPassword = randomStringGen.nextString();
        String [] saltAndHash = hashGenerator.getSaltAndHash(newPassword);
        PreparedStatement statement = null;
        try{
            String sentence = "Update admin SET salt = ?, hashed_pw = ? WHERE email = ?";
            statement = con.prepareStatement(sentence);
            statement.setString(1,saltAndHash[0]);
            statement.setString(2,saltAndHash[1]);
            statement.setString(3,email);
            statement.executeUpdate();
            return newPassword;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
        }
        return null;
    }
}