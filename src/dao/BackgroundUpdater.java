package dao;

import gui.Frame;
import gui.TablePanel;
import gui.Table;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The BackgroundUpdater class is used to update and refresh the panels.
 */

public class BackgroundUpdater implements Runnable {
    private String[] columnHeader;
    private Table jt;
    private TablePanel parentPanel;

    public BackgroundUpdater(String[] columnHeader, Table jt, TablePanel parentPanel) {
        this.columnHeader = columnHeader;
        this.jt = jt;
        this.parentPanel = parentPanel;
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     * Updates the panel every 10 seconds.
     */
    @Override
    public void run() {
        while(Frame.shouldUpdate){
            sleep(10000);
            jt.updateTable(parentPanel.tableData());
        }
    }

    /**
     * @param ms number of milliseconds to wait for
     */
    private void sleep(int ms){
        try{
            Thread.sleep(ms);
        }catch(InterruptedException ie){
            System.out.println(ie);
        }
    }
}
