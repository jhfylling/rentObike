package dao;

import sql.Cleaner;
import dto.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The UserDAO class is used to manage users in the system.
 */

public class UserDAO {
    private User newUser;
    private Connection con;

    /**
     * a constructor used to get a connection to the UserDAO class from the database
     * @param con a connection to the database
     */
    public UserDAO(Connection con){
        this.con = con;
    }

    /**
     * Get every user in the system
     * @return an array list of every user in the system
     */
    public ArrayList<User> getAllUsers(){
        Date ban_until; // = new GregorianCalendar();
        ArrayList<User> userlist = new ArrayList<>();
        ResultSet res = null;
        String sqlQuery = "SELECT * FROM users;";
        try(PreparedStatement stmt = con.prepareStatement(sqlQuery)){
            res = stmt.executeQuery();
            while(res.next()){
                int user_id = res.getInt("user_id");
                String card_info = res.getString("card_info");
                if(res.getDate("ban_until") == null){
                    ban_until = null;
                }else{
                    ban_until = res.getDate("ban_until");
                }
                String comments = res.getString("comments");
                userlist.add(new User(user_id,card_info,ban_until,comments,con));
            }
            return userlist;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            Cleaner.closeRes(res);
        }
        return null;
    }

    /**
     * Ban a user
     * @param banDate banned until date of the user
     * @param banComment a comment for the ban
     * @param user_id the id of user to ban
     * @return true if the person is banned and false if something went wrong
     */
    public boolean ban(Date banDate,String banComment, int user_id) {

        String sqlQuery = "UPDATE users SET ban_until = ? , comments =  ?  WHERE user_id = ?; ";
        try (PreparedStatement stmt = con.prepareStatement(sqlQuery)) {
            stmt.setDate(1, banDate);
            stmt.setString(2,banComment);
            stmt.setInt(3, user_id);
            stmt.executeUpdate();
            return true;
        } catch (SQLException se) {
            se.printStackTrace();
        }
        return false;
    }

    /**
     * unban a user
     * @param user_id the id of user to unban
     * @return return true if the person is unbanned and false if something went wrong
     */
    public boolean unban(int user_id){

        String sqlQuery = "UPDATE users SET ban_until = NULL, comments = NULL WHERE user_id = ?";
        try(PreparedStatement stmt = con.prepareStatement(sqlQuery)){
            stmt.setInt(1, user_id);
            stmt.executeUpdate();
            return true;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }
}