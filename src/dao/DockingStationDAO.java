package dao;

import dto.Bike;
import dto.BikeType;
import sql.Cleaner;
import dto.DockingStation;
import sql.DBconnection;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The DockingStationDAO class is used manage docking station in the system.
 */

public class DockingStationDAO {

    private PreparedStatement statement;
    private Connection con;
    private ArrayList<DockingStation> dockingStations;

    /**
     * This is a constructor to initialize a connection to the database for the class.
     * @param con connection to the database
     */
    public DockingStationDAO(Connection con) {
        this.con = con;
        this.dockingStations = new ArrayList();
    }

    /**
     * Get how many docking stations there is in the system
     * @return the amount of docking stations
     */
    public int getSize() {
        return dockingStations.size();
    }

    /**
     * A method used in addDockingStation to give a new DockingStation the highest ID.
     * @return the highest docking station ID
     */

    public int getMaxId () {
        int highest = 0;

        for (int i = 0; i < dockingStations.size() ; i++) {
            if (highest <= dockingStations.get(i).getId()) {
                highest = dockingStations.get(i).getId();
            }
        }
        return highest;
    }

    /**
     * Get a docking station
     * @param index index of the docking station you want
     * @return a docking station object
     */
    public DockingStation getDockingStation(int index) { return dockingStations.get(index); }

    /**
     * Get an array list of every docking station object
     * @return an array list of every docking stations objects
     */
    public ArrayList<DockingStation> getDockingStationList() {

        ResultSet res = null;

        try {
            statement = con.prepareStatement("SELECT * FROM dock ORDER BY dock_id ASC");
            res = statement.executeQuery();
            while (res.next()) {
                int id = res.getInt(1);
                String name = res.getString(2);
                double power_usage = res.getDouble(3);
                int max_bikes = res.getInt(4);
                double lat = res.getInt(5);
                double lon = res.getInt(6);

                DockingStation d = new DockingStation(id, name, power_usage, max_bikes, lat, lon, con);
                d.setId(id);
                dockingStations.add(d);
            }
        }
        catch (SQLException e) {
            System.out.println(e + "SQL error in getDockingStationList");
        }
        finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return dockingStations;
    }

    /**
     * get a docking station object using a dock id as input
     * @param dock_id the initial dock id
     * @return a docking station object
     */
    public DockingStation getDock(int dock_id){
        PreparedStatement statement = null;
        ResultSet res = null;
        DockingStation dock = null;
        try{

            String sentence = "SELECT * FROM dock d WHERE d.dock_id = ?";

            statement = con.prepareStatement(sentence);
            statement.setInt(1, dock_id);

            res = statement.executeQuery();
            while(res.next()) {
                dock = new DockingStation(res.getInt("dock_id"), res.getString("dock_name"),
                        res.getInt("power_usage"), res.getInt("max_bikes"),
                        res.getDouble("latitude"), res.getDouble("longitude"), con);
            }
            return dock;

        }catch (SQLException e){
            e.printStackTrace();
        }finally{
            Cleaner.closeRes(res);
            Cleaner.closeStatement(statement);
        }
        return null;
    }


    /**
     * Add a new docking station to the database
     * @param name name of the new docking station
     * @param max_bikes maximum capacity of bikes in the new docking station
     * @param lat latitude coordinates of the new docking station
     * @param lon longitude coordinates of the new docking station
     * @return return true if the docking station was added to the database and false if something went wrong
     */
    public boolean addDockingStation(String name, int max_bikes, double lat, double lon) {

        int id = getMaxId()+1;

        DockingStation d = new DockingStation(id, name, 0, max_bikes, lat, lon, con);

        try {
            statement = con.prepareStatement("INSERT INTO dock VALUES (DEFAULT,?,?,?,?,?,?)");
            statement.setString(1, name);
            statement.setDouble(2, 0);
            statement.setInt(3, max_bikes);
            statement.setDouble(4, lat);
            statement.setDouble(5,lon);
            statement.setBoolean(6,true);
            statement.executeUpdate();

            //d.setId(getMaxId()+1);

            dockingStations.add(d);


        } catch (SQLException e) {
            System.out.println(e + "error addDockingStation");
            return false;
        } finally {
            Cleaner.closeStatement(statement);
        }

        return true;
    }

    /**
     * a method to update the list of docking station with information from the database
     * @return true if the update was a success and false if some thing went wrong
     */
   public boolean update() {
        ResultSet res = null;
        dockingStations = new ArrayList<>();

        try {
            statement = con.prepareStatement("SELECT * FROM dock WHERE isEnabled = true ORDER BY dock_id ASC");
            //statement = con.prepareStatement("SELECT * FROM dock WHERE isEnabled = true");
            res = statement.executeQuery();

            while (res.next()) {
                int id = res.getInt(1);
                String name = res.getString(2);
                double power_usage = res.getDouble(3);
                int max_bikes = res.getInt(4);
                double lat = res.getDouble(5);
                double lon = res.getDouble(6);

                DockingStation d = new DockingStation(id, name, power_usage, max_bikes, lat, lon, con);
                d.setId(id);
                dockingStations.add(d);
            }
            return true;
        }
        catch (SQLException e) {
            System.out.println(e + "SQL error in update function");
        }
        finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return false;
    }



    /**
     * Disables a dockingStation based on index in the DockingStationList
     * @param index
     * @return true
     */
    public boolean disableDockingStation(int index){
        String sqlQuery = "UPDATE dock SET isEnabled = false WHERE dock_id = ?";
        try(PreparedStatement stmt = con.prepareStatement(sqlQuery)){
            stmt.setInt(1,index);
            stmt.executeUpdate();
            return true;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Edit information about dockingStation
     * @param dock_name
     * @param power_usage
     * @param max_bikes
     * @param latitude
     * @param longitude
     * @param id
     * @return true
     */
    public boolean editDckingStation(String dock_name, double power_usage,int max_bikes, double latitude, double longitude, int id){
        String sqlQuery = "UPDATE dock SET dock_name = ? ,power_usage = ?, max_bikes = ?, latitude = ?, longitude = ? WHERE dock_id = ? ";
        try(PreparedStatement stmt = con.prepareStatement(sqlQuery)){
            stmt.setString(1,dock_name);
            stmt.setDouble(2,power_usage);
            stmt.setInt(3,max_bikes);
            stmt.setDouble(4,latitude);
            stmt.setDouble(5,longitude);
            stmt.setInt(6,id);
            stmt.executeUpdate();
            return true;
        }catch (SQLException e){
            System.out.println(e + "Error: edit dockingStation");
        }
        return false;
    }

    /**
    * Get the bikes at a specific docking station
    * @param dock_id docking station ID for the docking station you want to look at
    * @return returns an array list of bikes located at that docking station
    */
    public ArrayList<Bike> getBikesAtStation(int dock_id){
        Bike aBike;
        ArrayList<Bike> bikes = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet res = null;
        try{
            String sentence = "SELECT d.*, b.*, t.description FROM dock d\n" +
                    "  LEFT JOIN bike b ON d.dock_id = b.dock_id\n" +
                    "  LEFT JOIN position p ON b.bike_id = p.bike_id\n" +
                    "  LEFT JOIN type t ON t.type_id = b.type_id\n" +
                    "WHERE b.isEnabled = TRUE\n" +
                    "      AND (p.time_position = (SELECT max(p.time_position) FROM position p WHERE b.bike_id = p.bike_id) OR p.time_position IS NULL)\n" +
                    "      AND d.dock_id = ?\n" +
                    "ORDER BY b.dock_id ASC;";
            statement = con.prepareStatement(sentence);
            statement.setInt(1,dock_id);
            res = statement.executeQuery();
            while(res.next()){
                aBike = createBike(res);
                if(aBike != null) bikes.add(aBike);
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }
        return bikes;
    }

    /**
     * Get an array list of bike IDs in the current docking station
     * @return number of bikes at station
     */
    public int getNumberOfBikesAtStation(int id) { //returns list of bike_IDs at dockingStation


        ResultSet res = null;
        int count = 0;

        try {

            statement = con.prepareStatement("SELECT COUNT(bike_id) FROM bike WHERE dock_id = ?");
            statement.setInt(1, id);
            res = statement.executeQuery();
            if (res.next()) {
                count = res.getInt(1);
            }
        }
        catch (SQLException e) {
            System.out.println(e + " getNumberOfBikesAtStation error");
        }
        finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
        }

        return count;
    }

    /**
     * Helping method creating bike objects for getBikesAtStations
     * @param res
     * @return
     */
    private Bike createBike(ResultSet res){
        try{
            int bike_id = res.getInt("bike_id");
            BikeType bike_type = new BikeType(res.getInt("type_id"),res.getString("description"));
            String make = res.getString("make");
            int price = res.getInt("price");
            int mileage = res.getInt("mileage");
            int charge = res.getInt("charge_level");
            Date date = res.getDate("date_bought");
            double lat = res.getDouble("latitude");
            double lng = res.getDouble("longitude");
            DockingStation dock = new DockingStation(res.getInt("dock_id"),res.getString("dock_name"),
                    res.getDouble("power_usage"),res.getInt("max_bikes"),lat,lng,con);

            Bike theBike = new Bike(bike_id,bike_type,make,price,mileage,charge,date,dock,con,lat,lng);
            return theBike;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    /**
     * @return String of information about the docking station
     */
    public String toString () {
        String res = "";

        for (DockingStation d: dockingStations) {

            res += "\n id: " + d.getId() + " name: " + d.getName() + " power usage: " + d.getPower_usage() + " max bikes: " + d.getMax_bikes();
        }
        return res;
    }
}