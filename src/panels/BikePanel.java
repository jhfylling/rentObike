package panels;

import dao.BikeDAO;
import dao.BackgroundUpdater;
import dao.DockingStationDAO;
import design.GuiColors;
import dto.Bike;
import dto.BikeType;
import dto.DockingStation;
import forms.BikeForm;
import forms.FinishRepairForm;
import forms.NewRepairForm;
import forms.TypeForm;
import gui.*;
import gui.TablePanel;
import gui.ScrollPane;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import static javax.swing.JOptionPane.YES_NO_OPTION;
import static javax.swing.JOptionPane.YES_OPTION;
import static javax.swing.JOptionPane.showConfirmDialog;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The BikePanel class is used to create a panel to manage bikes.
 */

@SuppressWarnings("Duplicates")
public class BikePanel extends TablePanel {
    private Thread updateThread;
    private BackgroundUpdater backgroundUpdater;

    private String menuBarColumnName[]={"Id","Type","Make", "Docking Station", "Charge Level", "Price", "Purchased"};

    private BikeDAO bikeDAO;
    private DockingStationDAO dockDAO;
    private ArrayList<Bike> bikeList;
    private DefaultTableModel jtModel;
    private Connection con;

    private TypeForm typeCard;
    private BikeForm card;
    private NewRepairForm newRepairCard;
    private FinishRepairForm finishRepairCard;
    private JFrame parentFrame;
    private DefaultCellEditor boxEditor;

    private ScrollPane sp;
    private Table jt;
    private EmptyPanel buttonBar;

    private Bike selectedBike;
    private int bikeIndex = -1;

    private JPanel infoPanel;
    //Special fake docking station to indicate that bike is not docked
    private DockingStation notInDock = new DockingStation(0, "Not in dock", 0, 5000,5,5, con);

    /**
     * A constructor used to create a connection to the database and to load the content of the panel.
     * @param con connection to the database
     */
    public BikePanel(Connection con) {
        super(new BorderLayout());
        this.con = con;
        System.out.println("Opened Bike Panel");
        bikeDAO = new BikeDAO(con);
        bikeList = bikeDAO.getBikes();

        dockDAO = new DockingStationDAO(con);
        onLoad();

        // update();
    }

    /**
     * Should be run upon loading the panel
     * Adds all components to the panel
     */
    private void onLoad(){

        buttonBar = new EmptyPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        add(buttonBar, BorderLayout.SOUTH);
        addEverythingToButtonBar();

        ArrayList<Class> columnTypes = new ArrayList<>();
        columnTypes.add(Integer.class);
        columnTypes.add(Object.class);
        columnTypes.add(Object.class);
        columnTypes.add(Object.class);
        columnTypes.add(Integer.class);
        columnTypes.add(Integer.class);
        columnTypes.add(Date.class);

        jtModel = new RentoBikeTableModel(tableData(), menuBarColumnName, this, columnTypes);

        jt = new Table(jtModel);

        //set type column editor to combobox
        updateTypeBox();

        //set DcckingStation column editor to combobox
        dockDAO.update();
        ArrayList<DockingStation> dockList = dockDAO.getDockingStationList();
        DockingStation[] docks = new DockingStation[dockList.size()+1];
        int i=0;
        for(DockingStation temp : dockList){
            docks[i] = temp;
            i++;
        }
        docks[i] = notInDock; //Not in docking station placeholder dock

        JComboBox<DockingStation> docksBox = new JComboBox<>(docks);
        boxEditor = new DefaultCellEditor(docksBox);
        boxEditor.setClickCountToStart(2);
        jt.getColumnModel().getColumn(3).setCellEditor(boxEditor);

        sp = new ScrollPane(jt);
        add(sp, BorderLayout.CENTER);
        createListSelector();
        addMouseListnerToList();
        addJTKeyListner();


        //Start automatic updating
        backgroundUpdater = new BackgroundUpdater(menuBarColumnName, jt, this);
        updateThread = new Thread(backgroundUpdater);
        updateThread.start();
    }

    public void updateTypeBox(){
        ArrayList<BikeType> bikeTypesList = bikeDAO.getTypes();
        BikeType[] bikeTypes = new BikeType[bikeTypesList.size()];
        int i=0;
        for(BikeType temp : bikeTypesList){
            bikeTypes[i] = temp;
            i++;
        }
        JComboBox<BikeType> bikeTypesBox = new JComboBox<>(bikeTypes);
        boxEditor = new DefaultCellEditor(bikeTypesBox);
        boxEditor.setClickCountToStart(2);
        jt.getColumnModel().getColumn(1).setCellEditor(boxEditor);
    }

    /**
     * Opens the info panel when you select a bike from the panel and displays the selected bike.
     */
    private void createListSelector(){
        ListSelectionModel select = jt.getSelectionModel();
        select.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        select.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(!e.getValueIsAdjusting()) {
                    bikeIndex = jt.getSelectedRow();
                    if(bikeIndex != -1 && bikeIndex < jt.getRowCount()) {
                        int convertedIndex = jt.convertRowIndexToModel(bikeIndex);
                        selectedBike = bikeList.get(convertedIndex);
                        openInfoPanel();
                        validate();
                    }
                }
            }
        });
    }


    /**
     * Called by tablemodel's setValueAt to save edited data after edit
     * @return true if edited data has been saved in database
     */
    public boolean clickCellEdit(){
        //get selected row index
        int selectedRow = jt.getSelectedRow();
        int convertedIndex = jt.convertRowIndexToModel(selectedRow);

        DockingStation rowDock = (DockingStation) jt.getModel().getValueAt(convertedIndex, 3);
        if(rowDock.getId() == 0){
            return false;
        }
        String rowMake = (String) jt.getModel().getValueAt(convertedIndex, 2);
        int rowCharge = (int) jt.getModel().getValueAt(convertedIndex, 4);
        int rowPrice = (int) jt.getModel().getValueAt(convertedIndex, 5);
        BikeType rowType = (BikeType) jt.getModel().getValueAt(convertedIndex, 1);

        selectedBike.setMake(rowMake);
        selectedBike.setCharge_level(rowCharge);
        selectedBike.setPrice(rowPrice);
        selectedBike.setType(rowType);
        selectedBike.setDoc(rowDock);

        return selectedBike.save();
    }

    /**
     * Subclass to form right-click menu
     */
    class RowPopup extends JPopupMenu{
        /**
         * Called after right-click to open right-click menu
         * @param table to edit
         */
        public RowPopup (JTable table){
            JMenuItem[] menuItems = new JMenuItem[3];
            menuItems[0] = new JMenuItem("Add new bike");
            menuItems[1] = new JMenuItem("Edit bike");
            menuItems[2] = new JMenuItem("Disable bike");

            for(int i = 0; i < menuItems.length; i++){
                menuItems[i].setBackground(GuiColors.WHITE);
                menuItems[i].setForeground(GuiColors.BLACK);
            }

            setBackground(GuiColors.DARK);
            setFont(new Font("Arial",Font.PLAIN,12));
            setPreferredSize(new Dimension(150,100));
            setBorder(new EmptyBorder(1,1,1,1));

            menuItems[0].addActionListener(e -> newBike());
            menuItems[1].addActionListener(e -> {
                //editing starts in selected columns
                jt.editCellAt(jt.getSelectedRow(),jt.getSelectedColumn());
            });
            menuItems[2].addActionListener(e -> disableBike());
            for(int i = 0; i < menuItems.length;i++){
                add(menuItems[i]);
            }
        }
    }

    /**
     * Adds keylistener bound to enter key
     * Selects next row upon enter click
     */
    private void addJTKeyListner(){
        jt.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if(e.getKeyCode() == KeyEvent.VK_ENTER){
                    jt.setRowSelectionInterval(bikeIndex, bikeIndex);
                }

            }
        });
    }

    /**
     * Right click listener opens up right-click menu
     */
    private void addMouseListnerToList(){
        final RowPopup pop = new RowPopup(jt);
        jt.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me){
                if(SwingUtilities.isRightMouseButton(me)){
                    pop.show(me.getComponent(),me.getX(),me.getY());
                }
            }
        });

    }

    /**
     * Opens the information panel when you select a bike from the table in the panel
     */
    private void openInfoPanel(){
        infoPanel = new JPanel(new BorderLayout());
        infoPanel.setPreferredSize(new Dimension(300,300));
        infoPanel.setBackground(GuiColors.DARK);
        add(infoPanel, BorderLayout.LINE_START);


        addInfoToInfoPanel();
        addCloseButton();
    }

    /**
     * Adds a close button for when you want to close the information panel when you click on a bike
     */
    private void addCloseButton(){
        JPanel wrappingPanel = new JPanel(new BorderLayout());
        wrappingPanel.setBackground(GuiColors.DARK);

        JButton button = new JButton("Close");
        button.setForeground(GuiColors.GRAY_TEXT);
        button.setFont(new Font("Arial", Font.BOLD, 15));
        button.setMaximumSize(new Dimension(30,30));
        button.setFocusPainted(false);
        button.setBorderPainted(false);
        button.setBackground(Color.BLACK);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                remove(infoPanel);
                validate();
            }
        });
        wrappingPanel.add(button,BorderLayout.EAST);

        infoPanel.add(wrappingPanel,BorderLayout.AFTER_LAST_LINE);

    }

    /**
     * Adds information to the info panel when you select a bike
     */
    private void addInfoToInfoPanel(){
        int bike_id = selectedBike.getBike_id();
        String header = "Bike " + bike_id;
        String tripsText = "Trips taken: " + bikeDAO.numberOfTrips(bike_id);
        String distanceText = "Distance: " + bikeDAO.numberOfKms(bike_id) + "km";
        String lat = "" + selectedBike.getLat();
        String lng = "" + selectedBike.getLon();
        String positionText = lat.substring(0,6) + " : " + lng.substring(0,6);

        String panelText = "<html><h1>" +header + "</h1><br><br>" + tripsText + "<br>";
        panelText+= distanceText+"<br>Position: " + positionText +"<br><br>";

        JLabel panelTextLabel = new JLabel(panelText);
        panelTextLabel.setFont(new Font("Arial", Font.BOLD, 12));
        panelTextLabel.setForeground(GuiColors.GRAY_TEXT);
        panelTextLabel.setBackground(GuiColors.DARK);

        JButton showRepairs = new JButton("Show Repair History");
        showRepairs.setForeground(GuiColors.GRAY_TEXT);
        showRepairs.setFont(new Font("Arial", Font.BOLD, 15));
        showRepairs.setMaximumSize(new Dimension(300,50));
        showRepairs.setFocusPainted(false);
        showRepairs.setBorderPainted(false);;
        showRepairs.setBackground(Color.BLACK);
        showRepairs.addActionListener(actionEvent -> addRepairHistory());

        JLabel spacer = new JLabel(" ");
        JButton showUserIds = new JButton("Show User IDs");
        showUserIds.setForeground(GuiColors.GRAY_TEXT);
        showUserIds.setFont(new Font("Arial", Font.BOLD, 15));
        showUserIds.setMaximumSize(new Dimension(300,50));
        showUserIds.setFocusPainted(false);
        showUserIds.setBorderPainted(false);
        showUserIds.setBackground(Color.BLACK);
        showUserIds.addActionListener(actionEvent -> addUserHistory());

        JPanel headerWrapper = new JPanel();
        headerWrapper.setLayout(new BoxLayout(headerWrapper,BoxLayout.Y_AXIS));
        headerWrapper.setBackground(GuiColors.DARK);
        headerWrapper.setBorder(new EmptyBorder(0,10,0,10));
        headerWrapper.add(panelTextLabel);
        headerWrapper.add(showRepairs);
        headerWrapper.add(spacer);
        headerWrapper.add(showUserIds);
        infoPanel.add(headerWrapper, BorderLayout.NORTH);


    }

    /**
     * Add repair history for a bike to a side panel when you click a bike
     */
    private void addRepairHistory(){
        String panelText = "<html><h3>Repair history</h3>";

        ArrayList<String> repairHistory = bikeDAO.getRepairHistory(selectedBike.getBike_id());
        String [] formatedHistory;
        boolean moreThenOne = false;
        for(String aRepair : repairHistory){
            formatedHistory = aRepair.split(";");
            if(moreThenOne) {
                panelText += "<br><br>";
            }
            for(String aLine : formatedHistory){
                panelText += aLine + "<br>";
            }
            moreThenOne = true;
        }
        if(repairHistory.size() == 0){
            panelText += "No Repairs Made";
        }
        panelText+= "</html>";

        JLabel historyText = new JLabel(panelText);
        historyText.setOpaque(true);
        historyText.setForeground(GuiColors.GRAY_TEXT);
        historyText.setBackground(GuiColors.DARK);
        historyText.setBorder(new EmptyBorder(0,0,0,0));

        JScrollPane historyScroll = new JScrollPane(historyText, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        historyScroll.setBackground(GuiColors.DARK);
        historyScroll.setBorder(new EmptyBorder(0,0,0,0));

        JPanel historyWrapper = new JPanel(new BorderLayout());
        historyWrapper.setBackground(GuiColors.DARK);
        historyWrapper.setBorder(new EmptyBorder(0,10,0,0));
        historyWrapper.add(historyScroll,BorderLayout.NORTH);
        infoPanel.add(historyWrapper,BorderLayout.CENTER);
        validate();

    }

    /**
     * Add user history for a bike to a side panel when you click a bike
     */
    private void addUserHistory(){
        String panelText = "<html><h3>User History</h3>";
        ArrayList<String> userHistory = bikeDAO.getUserHistory(selectedBike.getBike_id());
        for(String trip : userHistory){
            panelText += trip + "<br><br>";
        }
        if(userHistory.size() == 0) panelText += "Never Been Used";
        panelText+= "</html>";

        JLabel userText = new JLabel(panelText);
        userText.setOpaque(true);
        userText.setForeground(GuiColors.GRAY_TEXT);
        userText.setBackground(GuiColors.DARK);
        userText.setBorder(new EmptyBorder(0,0,0,0));

        JScrollPane userScroll = new JScrollPane(userText, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        userScroll.setBackground(GuiColors.DARK);
        userScroll.setBorder(new EmptyBorder(0,0,0,0));


        JPanel userWrapper = new JPanel(new BorderLayout());
        userWrapper.setBackground(GuiColors.DARK);
        userWrapper.setBorder(new EmptyBorder(0,10,0,0));
        userWrapper.add(userScroll,BorderLayout.NORTH);
        infoPanel.add(userWrapper, BorderLayout.CENTER);
        validate();
    }

    /**
     * Adds every button to the button bar
     */
    private void addEverythingToButtonBar(){
        MenuButton newBikeB = new MenuButton("Register Bike");
        newBikeB.setPreferredSize(new Dimension(200,50));
        buttonBar.add(newBikeB);
        newBikeB.addActionListener(e -> newBike());

        MenuButton disable = new MenuButton("Disable Selected Bike");
        disable.setPreferredSize(new Dimension(200,50));
        buttonBar.add(disable);
        disable.addActionListener(e -> disableBike());

        MenuButton newRepairButton = new MenuButton("Register New Repair");
        newRepairButton.setPreferredSize(new Dimension(200,50));
        buttonBar.add(newRepairButton);
        newRepairButton.addActionListener(e->newRepair());

        MenuButton finsihRepairButton = new MenuButton("Register Finished Repair");
        finsihRepairButton.setPreferredSize(new Dimension(200,50));
        buttonBar.add(finsihRepairButton);
        finsihRepairButton.addActionListener(e-> finishRepair());

        MenuButton editTypeButton = new MenuButton("Disable type");
        editTypeButton.setPreferredSize(new Dimension(200,50));
        buttonBar.add(editTypeButton);
        editTypeButton.addActionListener(e-> editType());
    }



    /**
     * Method for putting information into columns when you want to register a new bike
     * @param input an string array with input
     * @return true if the bike was registered and false if something went wrong
     */
    public boolean save(ArrayList<String> input){
        Double price = Double.parseDouble(input.get(3));
        int typeId = Integer.parseInt(input.get(0));
        int dockId = Integer.parseInt(input.get(2));
        boolean result = bikeDAO.addBike(input.get(1), price, 0, 100, typeId, dockId);
        if(result){
            jt.updateTable(tableData());
            return true;
        }
        return false;
    }

    /**
     * Register a new repair on a bike
     * @param input user input about the registration written in the text fields in the panel
     * @return true if the registration was a success and false if something went wrong
     */
    public boolean registerNewRepair(ArrayList<String> input){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date date = null;
        try{
           date = sdf.parse(input.get(1));
        }catch (Exception e){

        }
        java.sql.Date sqlStartDate = new java.sql.Date(date.getTime());

        String description = input.get(2);
        int bike_id = Integer.parseInt(input.get(0));
        return bikeDAO.registerNewRepair(sqlStartDate,description,bike_id);
    }

    /**
     * Register when a repair is finished
     * @param input user input about the registration written in the text fields in the panel
     * @return true if the registration is complete and false if something went wrong
     */
    public boolean registerFinishRepair(ArrayList<String> input){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date date = null;
        try{
            date = sdf.parse(input.get(1));
        }catch (Exception e){

        }
        java.sql.Date sqlStartDate = new java.sql.Date(date.getTime());
        String description = input.get(2);
        int bike_id = Integer.parseInt(input.get(0));
        double price = Double.parseDouble(input.get(3));
        return bikeDAO.registerFinishedRepair(sqlStartDate,description,bike_id,price);

    }

    /**
     * Opens up a form with text fields for the registration of a new repair
     */
    private void newRepair(){
        parentFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
        int height = 250;
        int width = 300;
        int x=400;
        int y=this.getRootPane().getHeight()-height-60;
        if(newRepairCard == null){
            newRepairCard = new NewRepairForm(parentFrame,this,x,y,width,height);
            validate();
        }else{
            newRepairCard.toggle();
            newRepairCard.setBounds(x,y,width,height);
        }
        if(card != null){
            card.hideCard();
        }
        if(finishRepairCard != null){
            finishRepairCard.hideCard();
        }
        if(typeCard != null){
            typeCard.hideCard();
        }
    }

    /**
     * Opens up a form with text fields for the registration of a finished repair
     */
    private void finishRepair(){
        parentFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
        int height = 300;
        int width = 300;
        int x=600;
        int y=this.getRootPane().getHeight()-height-60;
        if(finishRepairCard == null){
             finishRepairCard = new FinishRepairForm(parentFrame,this,x,y,width,height);
             validate();
        }else{
            finishRepairCard.toggle();
            finishRepairCard.setBounds(x,y,width,height);
        }
        if(newRepairCard != null){
            newRepairCard.hideCard();
        }
        if(card != null){
            card.hideCard();
        }
        if(typeCard != null){
            typeCard.hideCard();
        }
    }

    /**
     * Opens up a form with text fields for the registration of a new bike
     */
    private void newBike(){
        parentFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
        int height = 300;
        int width = 300;
        int x=0;
        int y=this.getRootPane().getHeight()-height-60;

        if(card == null) {
            card = new BikeForm(parentFrame, this, x, y, width, height, con);
            repaint();
        }else{
            card.toggle();
            card.setBounds(x, y, width, height);
        }
        if(newRepairCard != null){
            newRepairCard.hideCard();
        }
        if(finishRepairCard != null){
            finishRepairCard.hideCard();
        }
        if(typeCard != null){
            typeCard.hideCard();
        }
    }

    /**
     * opens the edit type function as a form card
     */
    private void editType(){
        parentFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
        int height = 150;
        int width = 300;
        int x = 750;
        int y = this.getRootPane().getHeight()-height-60;

        if(typeCard == null) {
            typeCard = new TypeForm(parentFrame, this, x, y, width, height, con);
            repaint();
        }else{
            typeCard.toggle();
            typeCard.setBounds(x, y, width, height);
        }
        if(newRepairCard != null){
            newRepairCard.hideCard();
        }
        if(finishRepairCard != null){
            finishRepairCard.hideCard();
        }
        if(card != null){
            card.hideCard();
        }
    }

    /**
     * A method to remove a row in the panel
     */
    public void removeSelectedRows(){
        DefaultTableModel model = (DefaultTableModel) jt.getModel();
        int[] rows = jt.getSelectedRows();
        for(int i=0;i<rows.length;i++){
            model.removeRow(rows[i]-i);
        }
    }

    /**
     * Disable a bike from the panel
     */
    private void disableBike(){
        int bikeid = selectedBike.getBike_id();

        UIManager UI = new UIManager();
        UI.put("OptionPane.background", Color.white);
        UI.put("Panel.background", Color.white);

        int option = showConfirmDialog(null, "Are you sure?", "WARNING", YES_NO_OPTION);
        if (option == YES_OPTION) {
            bikeDAO.disableBike(bikeid);
            System.out.println("Disabling bike with id: " + bikeid);
            jt.updateTable(tableData());
        }

    }

    /**
     * Retrieves table data from bikeDAO and stores it in two-dim Object array
     * for JTable
     * @return two-dimensional Object array with bike-info
     */
    public Object[][] tableData(){
        bikeList = bikeDAO.getBikes();
        int length = bikeList.size();
        Object[][] data = new Object[length][7];
        BikeDAO bf = null;
        Bike temp;
        for(int i = 0;i<length; i++){
            temp = bikeList.get(i);
            data[i][0] = temp.getBike_id();
            data[i][1] = temp.getType();
            data[i][2] = temp.getMake();
            data[i][3] = temp.getDock();
            data[i][4] = temp.getCharge_level();
            data[i][5] = temp.getPrice();
            data[i][6] = temp.getDate_bought();
        }
        return data;
    }

    /**
     * Close the connection to the database
     */
    @Override
    public void closeCon() {
    }

    /**
     * Hides the popup form when you switch panels
     */
    @Override
    public void swapOut() {
        if(card != null) {
            card.hideCard();
        }

        if(newRepairCard != null) {
            newRepairCard.hideCard();
        }

        if(finishRepairCard != null) {
            finishRepairCard.hideCard();
        }
        if(typeCard != null){
            typeCard.hideCard();
        }
    }
}