package panels;

import gui.FrameElements;

import javax.swing.*;
import java.awt.*;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The ContainerPanel class is used to create a panel to use in other panels.
 */

public class ContainerPanel extends JPanel implements FrameElements {
    private GridBagConstraints gbc;

    /**
     * A constructor used to create a container panel
     * @param x the x coordinate
     * @param y the y coordinate
     * @param weightX space to use in x direction
     * @param weightY space to use in y direction
     * @param spanX number of columns to span
     * @param spanY number of rows to span
     * @param anchor gridbag to place object in cell
     */
    public ContainerPanel(int x, int y, double weightX, double weightY, int spanX, int spanY, int anchor) {
        super(new GridBagLayout());
        gbc = new GridBagConstraints();
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.weightx = weightX;
        gbc.weighty = weightY;
        gbc.gridwidth = spanX;
        gbc.gridheight = spanY;
        gbc.anchor = anchor;
    }

    /**
     * Get a gbc object
     * @return a new gbc object
     */
    @Override
    public GridBagConstraints getGbc() {
        return gbc;
    }

    /**
     * Set a new gbc object
     * @param gbc set GridBagConstraints to be used by object
     */
    @Override
    public void setGbc(GridBagConstraints gbc) {
        this.gbc = gbc;
    }
}
