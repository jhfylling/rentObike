package panels;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.Date;
import java.util.ArrayList;

import dao.AdminDAO;
import dao.BackgroundUpdater;
import design.GuiColors;
import dto.Admin;
import forms.AdminForm;
import gui.*;
import gui.TablePanel;
import gui.ScrollPane;

import static javax.swing.JOptionPane.YES_OPTION;
import static javax.swing.JOptionPane.showConfirmDialog;


/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The AdminPanel class is used to create a panel to manage administrators.
 */

@SuppressWarnings("Duplicates")
public class AdminPanel extends TablePanel {
    private Thread updateThread;
    private BackgroundUpdater backgroundUpdater;

    private final String[] menuBarNames = {"Id","Email","Name"};

    private MenuButton registerAdmin, disableAdmin;
    private AdminDAO adminDAO;
    private ArrayList<Admin> adminlist;
    private JScrollPane sp;
    private DefaultTableModel jtModel;
    private Connection con;

    private int adminId = 0;

    private AdminForm card;
    private JFrame parentFrame;

    private Table jt;
    private EmptyPanel buttonBar;

    private int adminIndex = -1;
    private Admin selectedAdmin;

    /**
     * A constructor used to create a connection to the database and to load the content of the panel.
     * @param con connection to the database
     */
    public AdminPanel (Connection con){
        super(new BorderLayout());
        this.con = con;
        onLoad();
        setVisible(true);
        System.out.println("opened AdminPanel");
    }

    /**
     * Load the administrator content into the panel
     */
    private void onLoad(){

        buttonBar = new EmptyPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        add(buttonBar, BorderLayout.SOUTH);
        addEverythingToButtonBar();

        adminDAO = new AdminDAO(con);
        adminlist = adminDAO.getAllAdmin();

        ArrayList<Class> columnTypes = new ArrayList<>();
        columnTypes.add(Integer.class);
        columnTypes.add(Object.class);
        columnTypes.add(Object.class);
        jtModel = new RentoBikeTableModel(tableData(), menuBarNames, this, columnTypes);

        jt = new Table(jtModel);
        addJTKeyListner();


        sp = new ScrollPane(jt);
        createListSelector();
        addMouseListnerToList();

        add(sp, BorderLayout.CENTER);

        //Start automatic updating
        backgroundUpdater = new BackgroundUpdater(menuBarNames, jt, this);
        updateThread = new Thread(backgroundUpdater);
        updateThread.start();

    }

    /**
     * A method to get a popup menu in the panel.
     */
    class RowPopup extends JPopupMenu{
        public RowPopup (JTable table){
            JMenuItem[] menuItems = new JMenuItem[3];
            menuItems[0] = new JMenuItem("Add new admin");
            menuItems[1] = new JMenuItem("Edit admin");
            menuItems[2] = new JMenuItem("Disable admin");

            for(int i = 0; i < menuItems.length; i++){
                menuItems[i].setBackground(GuiColors.WHITE);
                menuItems[i].setForeground(GuiColors.BLACK);
            }
            setBackground(GuiColors.DARK);
            setFont(new Font("Arial",Font.PLAIN,12));
            setPreferredSize(new Dimension(150,100));
            setBorder(new EmptyBorder(1,1,1,1));

            menuItems[0].addActionListener(e -> newAdmin());
            menuItems[1].addActionListener(e -> {

                //editing starts in selected columns
                jt.editCellAt(jt.getSelectedRow(),jt.getSelectedColumn());
            });
            menuItems[2].addActionListener(e -> disableAdmin());
            for(int i = 0; i < menuItems.length;i++){
                add(menuItems[i]);
            }
        }
    }

    /**
     * Adds a key listener to see if a key is pressed and updates the edited cell.
     */
    private void addJTKeyListner(){
        jt.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if(e.getKeyCode() == KeyEvent.VK_ENTER){
                    jt.setRowSelectionInterval(adminIndex, adminIndex);
                }

            }
        });
    }

    /**
     * Creates a popup menu if you right click a column.
     */
    private void addMouseListnerToList(){
        final RowPopup pop = new RowPopup(jt);
        jt.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me){
                if(SwingUtilities.isRightMouseButton(me)){
                    pop.show(me.getComponent(),me.getX(),me.getY());
                }
            }
        });

    }

    /**
     * Shows which admin is selected with information about the admin
     */
    private void createListSelector(){
        ListSelectionModel select = jt.getSelectionModel();
        select.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        select.addListSelectionListener(e -> {
            if(!e.getValueIsAdjusting()) {
                adminIndex = jt.getSelectedRow();
                if(adminIndex != -1 && adminIndex < jt.getRowCount()) {
                    int convertedIndex = jt.convertRowIndexToModel(adminIndex);
                    selectedAdmin = adminlist.get(convertedIndex);
                }
            }
        });
    }

    /**
     * Creates a button bar with different buttons.
     */
    private void addEverythingToButtonBar(){
        buttonBar.add(registerAdmin = new MenuButton("Register Administrator"));
        buttonBar.add(disableAdmin = new MenuButton("Disable Selected Admin"));
        disableAdmin.setPreferredSize(new Dimension(200,50));
        registerAdmin.setPreferredSize(new Dimension(200,50));

        registerAdmin.addActionListener(e -> newAdmin());
        disableAdmin.addActionListener(e -> {
            disableAdmin();
        });
    }

    /**
     * A method to register a new admin
     */
    private void newAdmin() {
        parentFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
        int height = 200;
        int width = 300;
        int x = 0;
        int y = this.getRootPane().getHeight() - height - 60;

        if (card == null) {
            System.out.println("Creating card");
            card = new AdminForm(parentFrame,this,x,y,width,height);
            repaint();
        } else {
            card.toggle();
            card.setBounds(x, y, width, height);
        }
    }

    /**
     * Edit a cell by clicking twice on the cell
     * @return true if the information was edited and false if something went wrong
     */
    public boolean clickCellEdit(){
        String rowEmail = (String) jt.getValueAt(jt.getSelectedRow(), 1);
        String rowName = (String) jt.getValueAt(jt.getSelectedRow(), 2);

        selectedAdmin.setEmail(rowEmail);
        selectedAdmin.setAdmin_name(rowName);

        selectedAdmin.save();

        return false;
    }

    /**
     * Method that register new admin to the system.
     * the new admin wil receive an email with correct login password
     * @return true if the registration was a success and false if something went wrong
     */

    public boolean save(ArrayList<String> input){
        if(adminDAO.registerNewAdmin(input)){
            System.out.println("Success! Email sent to "+input.get(0));
            jt.updateTable(tableData());
            return true;
        }else{
            System.out.println(input.get(0)+" all ready registered.");
            return false;
        }
    }

    /**
     * A method used to disable an administrator.
     */
    private void disableAdmin(){
        if(selectedAdmin != null){
            adminId = selectedAdmin.getAdmin_id();

            UIManager UI = new UIManager();
            UI.put("OptionPane.background", Color.white);
            UI.put("Panel.background", Color.white);

            int option = showConfirmDialog(null,"Are you sure?","Warning", JOptionPane.YES_NO_OPTION);
            if(option == YES_OPTION){
                adminDAO.disableAdmin(adminId);
                System.out.println("Disabling admin with admin id: " + adminId);
                jt.updateTable(tableData());
            }
        }
    }

    /**
     * A table of data used to create a table in the administrator panel
     * @return a string table of administrator data
     */
    public Object[][] tableData(){
        adminlist = adminDAO.getAllAdmin();
        int length = adminlist.size();
        Object[][] data = new Object[length][3];
        Admin temp;
        for(int i = 0; i < length; i++){
            temp = adminlist.get(i);
            data[i][0] = temp.getAdmin_id();
            data[i][1] = String.valueOf(temp.getAdmin_email());
            data[i][2] = String.valueOf(temp.getAdmin_name());
        }
        return data;
    }

    /**
     * Close connection to the database
     */
    @Override
    public void closeCon() {
    }

    /**
     * hides the card popup if you change panel.
     */
    @Override
    public void swapOut() {
        if(card != null) {
            card.hideCard();
        }
    }
}