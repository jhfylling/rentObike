package panels;

import chrriis.dj.nativeswing.NSComponentOptions;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserFunction;
import design.GuiColors;
import gui.BasicPanel;
import gui.TablePanel;


import javax.swing.*;
import java.awt.*;
import java.sql.Connection;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The MapsPanel class is used to create a panel to view a map with docking stations and bikes.
 */

@SuppressWarnings("Duplicates")
public class MapsPanel extends TablePanel {
    final JWebBrowser webBrowser = new JWebBrowser(NSComponentOptions.destroyOnFinalization());
    private MapSidePanel sidePanel;
    private Connection con;

    /**
     * A constructor to initialize the map panel and get a connection to the database
     * @param con connection to the database
     */
    public MapsPanel(Connection con) {
        super(new BorderLayout());
        this.con = con;
        onLoad();
        System.out.println("Opened MapsPanel");
    }

    /**
     * Should be run upon loading the panel
     * Adds all components to the panel
     */
    private void onLoad(){
        setBackground(GuiColors.focusColor);

        //Webbrowserpanel contains the webBrowser object and the contentPane contains
        //the webBrowserPanel.
        JPanel webBrowserPanel = new JPanel(new BorderLayout());
        webBrowserPanel.setBackground(GuiColors.focusColor);

        //select johandev.no which serves us the map
        webBrowser.navigate("https://johandev.no");
        //add the browser object to browserpanel
        webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
        //removes all bars such as url and menu etc
        webBrowser.setMenuBarVisible(false);
        webBrowser.setBarsVisible(false);
        //webBrowser.setJavascriptEnabled(false);
        //System.out.println("js enabled: "+webBrowser.isJavascriptEnabled());

        //webBrowser.executeJavascript(js);

        //add browserpanel to MapsPanel
        add(webBrowser, BorderLayout.CENTER);


        //Create and add sidePanel to MapsPanel
        sidePanel = new MapSidePanel(223, con, this);


        //Register a function that can be invoked by JS. This is used to click on a bike
        //and get the details of that bike. The bike ID parameter will be sent as an argument from JS to JAVA
        webBrowser.registerFunction(new WebBrowserFunction("setBike") {
            @Override
            public Object invoke(JWebBrowser jWebBrowser, Object... objects) {
                int id = Integer.parseInt(objects[0].toString());
                sidePanel.setBike(id);
                add(sidePanel, BorderLayout.WEST);
                validate();

                //System.out.println("id: "+id);
                return null;
            }
        });


        //Register a function that can be invoked by JS. This is used to click on a docking station
        //and get the details of that dock. The dock ID parameter will be sent as an argument from JS to JAVA
        webBrowser.registerFunction(new WebBrowserFunction("setDock") {
            @Override
            public Object invoke(JWebBrowser jWebBrowser, Object... objects) {

                int id = Integer.parseInt(objects[0].toString());
                sidePanel.setDock(id);
                add(sidePanel, BorderLayout.WEST);
                validate();

                return null;
            }
        });

    }

    /**
     * Close connection to the web browser
     */
    @Override
    public void closeCon() {
        webBrowser.disposeNativePeer(false);
    }

    /**
     * close the popup window when you switch panel
     */
    @Override
    public void swapOut() {

    }

    /**
     * It has to be here but it has no usage, so it just returns false
     * @param input an string array with input
     * @return false
     */
    @Override
    public boolean save(ArrayList<String> input) {
        return false;
    }

    /**
     * a overwritten class that just returns an empty object table.
     * @return
     */
    @Override
    public Object[][] tableData() {
        return new Object[0][];
    }

    /**
     * just a overwritten class that does nothing
     * @return
     */
    @Override
    public boolean clickCellEdit() {
        return false;
    }
}
