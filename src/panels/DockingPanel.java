package panels;

import dao.BackgroundUpdater;
import dao.DockingStationDAO;
import design.GuiColors;
import dto.Bike;
import dto.DockingStation;
import forms.DockingForm;
import gui.*;
import gui.TablePanel;
import gui.ScrollPane;
import sql.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.Date;
import java.util.ArrayList;

import static javax.swing.JOptionPane.*;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The DockingPanel class is used to create a panel to manage docking stations.
 */

@SuppressWarnings("Duplicates")
public class DockingPanel extends TablePanel {
    private Thread updateThread;
    private BackgroundUpdater backgroundUpdater;

    private String menuBarNames[]={"Id","Name","Power Usage", "Max Bikes", "Latitude", "Longitude"};

    private DockingStationDAO dockDAO;
    private DefaultTableModel jtModel;
    private ScrollPane sp;
    private MenuButton disable, newDockd;
    private DockingForm card;
    private Color hoverC = GuiColors.focusColor;
    private Color normalC = GuiColors.menuColor;
    private int buttonBarHeight = 50;
    private JFrame parentFrame;

    private EmptyPanel buttonBar;
    private Table jt;
    private JPanel infoPanel;

    private int dockIndex = -1;

    private DockingStation selectedDock;

    /**
     * A constructor used to create a connection to the database and to load the content of the panel.
     * @param con
     */
    public DockingPanel(Connection con) {
        super(new BorderLayout());
        System.out.println("Opened DockingStation Panel");

        dockDAO = new DockingStationDAO(con);
        onLoad();
    }

    /**
     * Should be run upon loading the panel
     * Adds all components to the panel
     */
    private void onLoad(){

        buttonBar = new EmptyPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        add(buttonBar, BorderLayout.SOUTH);
        addEverythingToMenuBar();

        //create table and fill with tabledata
        ArrayList<Class> columnTypes = new ArrayList<>();
        columnTypes.add(Integer.class);
        columnTypes.add(Object.class);
        columnTypes.add(Double.class);
        columnTypes.add(Integer.class);
        columnTypes.add(Double.class);
        columnTypes.add(Double.class);

        jtModel = new RentoBikeTableModel(tableData(), menuBarNames, this, columnTypes);
        jt = new Table(jtModel);

        addJTKeyListner();

        sp = new ScrollPane(jt);
        add(sp, BorderLayout.CENTER);

        createListSelector();
        addMouseListnerToList();

        //Start automatic updating
        backgroundUpdater = new BackgroundUpdater(menuBarNames, jt, this);
        updateThread = new Thread(backgroundUpdater);
        updateThread.start();

    }

    /**
     * Used to create a popup menu
     */
    class RowPopup extends JPopupMenu{
        public RowPopup (JTable table){
            JMenuItem[] menuItems = new JMenuItem[3];
            menuItems[0] = new JMenuItem("Add new dock");
            menuItems[1] = new JMenuItem("Edit dock");
            menuItems[2] = new JMenuItem("Disable dock");

            for(int i = 0; i < menuItems.length; i++){
                menuItems[i].setBackground(GuiColors.WHITE);
                menuItems[i].setForeground(GuiColors.BLACK);
            }
            setBackground(GuiColors.DARK);
            setFont(new Font("Arial",Font.PLAIN,12));
            setPreferredSize(new Dimension(150,100));
            setBorder(new EmptyBorder(1,1,1,1));

            menuItems[0].addActionListener(e -> newDock());
            menuItems[1].addActionListener(e -> {
                //editing starts in first editable column
                jt.editCellAt(jt.getSelectedRow(),jt.getSelectedColumn());
            });

            menuItems[2].addActionListener(e -> disableDockingStation());
            for(int i = 0; i < menuItems.length;i++){
                add(menuItems[i]);
            }
        }
    }

    /**
     * Adds a key listener to see if a key is pressed and updates the edited cell.
     */
    private void addJTKeyListner(){
        jt.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if(e.getKeyCode() == KeyEvent.VK_ENTER){
                    jt.setRowSelectionInterval(dockIndex, dockIndex);
                }
            }
        });
    }

    /**
     * Opens a panel with information about a selected docking station
     * @param selectedDock a docking station object for the selected docking station
     */
    private void openInfoPanel(DockingStation selectedDock){
        infoPanel = new JPanel(new BorderLayout());
        infoPanel.setPreferredSize(new Dimension(300,300));
        infoPanel.setBackground(GuiColors.DARK);
        add(infoPanel, BorderLayout.LINE_START);
        addInfoToInfoPanel(selectedDock);
        addCloseButton();
    }

    /**
     * Adds information about the selected docking station into the info panel
     * @param selectedDock a docking station object for the selected docking station
     */
    private void addInfoToInfoPanel(DockingStation selectedDock){
        String dockText = "<html> <h1>" + selectedDock.getName()+
                "<br> Docking Station</h1><br>";
        ArrayList<Bike> bikesAtStation = dockDAO.getBikesAtStation(selectedDock.getId());
        if(bikesAtStation.size() != 1){
            dockText += "<h3>Currently " +bikesAtStation.size()+ " bikes at station</h3>";
        }else {
            dockText += "<h3>Currently " +bikesAtStation.size()+ " bike at station</h3>";
        }


        for(Bike bike : bikesAtStation){
            dockText += "ID: " + bike.getBike_id() + "<br>Type: " + bike.getType() + "<br>Charge Level: " + bike.getCharge_level() + "%<br><br>";
        }
        dockText += "</html>";


        JLabel dockTextLabel = new JLabel(dockText);
        dockTextLabel.setFont(new Font("Arial", Font.BOLD, 12));
        dockTextLabel.setForeground(GuiColors.GRAY_TEXT);

        JPanel textWrapper = new JPanel(new BorderLayout());
        textWrapper.add(dockTextLabel, BorderLayout.NORTH);
        textWrapper.setBackground(GuiColors.DARK);
        //textWrapper.setPreferredSize(new Dimension(200,700));

        JScrollPane scroll = new JScrollPane(textWrapper, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setBorder(new EmptyBorder(0,20,0,0));
        scroll.setPreferredSize(new Dimension(200,700));
        scroll.setBackground(GuiColors.DARK);

        infoPanel.add(scroll, BorderLayout.NORTH);

    }

    /**
     * Adds a close button for when you want to close the information panel when you click on a bike
     */
    private void addCloseButton(){
        JPanel wrappingPanel = new JPanel(new BorderLayout());
        wrappingPanel.setBackground(GuiColors.DARK);

        JButton button = new JButton("Close");
        button.setForeground(GuiColors.GRAY_TEXT);
        button.setFont(new Font("Arial", Font.BOLD, 15));
        button.setMaximumSize(new Dimension(30,30));
        button.setFocusPainted(false);
        button.setBorderPainted(false);
        button.setBackground(Color.BLACK);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                remove(infoPanel);
                validate();
            }
        });
        wrappingPanel.add(button,BorderLayout.EAST);

        infoPanel.add(wrappingPanel,BorderLayout.PAGE_END);
    }

    /**
     * Creates a popup menu if you right click a column.
     */
    private void addMouseListnerToList(){
        final RowPopup pop = new RowPopup(jt);
        jt.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me){
                if(SwingUtilities.isRightMouseButton(me)){
                    pop.show(me.getComponent(),me.getX(),me.getY());
                }
            }
        });

    }

    /**
     * Opens the info panel when you select a docking station from the panel and displays the selected docking station.
     */
    private void createListSelector(){
        ListSelectionModel select = jt.getSelectionModel();
        select.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        select.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(!e.getValueIsAdjusting()) {

                    dockIndex = jt.getSelectedRow();
                    if(dockIndex != -1 && dockIndex <= jt.getRowCount()){
                        int convertedIndex = jt.convertRowIndexToModel(dockIndex);
                        selectedDock = dockDAO.getDockingStation(convertedIndex);
                        //TODO: convert this to openpanel only once
                        //TODO: and then just set dock in allready open panel. This is inefficient
                        openInfoPanel(selectedDock);
                        validate();
                    }
                }
            }
        });
    }

    /**
     * Adds every button to the button bar
     */
    private void addEverythingToMenuBar(){
        disable = new MenuButton("Disable Selected Dock");
        disable.setPreferredSize(new Dimension(200, 50));
        disable.addActionListener(e -> disableDockingStation());
        addListeners(disable);


        newDockd = new MenuButton("Register Docking Station");
        newDockd.setPreferredSize(new Dimension(200, 50));
        newDockd.addActionListener(e -> newDock());
        addListeners(newDockd);


        buttonBar.add(newDockd);
        buttonBar.add(disable);
    }

    /**
     * Adds listeners to the chosen button
     * @param temp a button you want to add listeners to
     */
    private void addListeners(MenuButton temp){
        temp.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if(temp.getModel().isArmed()){
                    //Shoot back
                    temp.setBackground(hoverC);
                }
                if(temp.getModel().isRollover()){
                    temp.setBackground(hoverC);
                }
                else{
                    temp.setBackground(normalC);
                }
            }
        });
    }

    /**
     * Creates a new dockingStation and inserts it into the database
     * @param input an string array with input
     * @return true if the registration was a success and false if something went wrong
     */
    public boolean save(ArrayList<String> input){
        boolean success = dockDAO.addDockingStation(input.get(0), Integer.parseInt(input.get(1)), Double.parseDouble(input.get(2)), Double.parseDouble(input.get(3)));
        if(success){
            jt.updateTable(tableData());
        }
        return success;
    }

    /**
     * double click on a cell to edit whats inside
     * @return true if the cell got edited and false if something went wrong
     */
    public boolean clickCellEdit(){
        String rowName = (String) jt.getValueAt(dockIndex, 1);
        double rowPower = (double) jt.getValueAt(dockIndex, 2);
        int rowMax = (int) jt.getValueAt(dockIndex, 3);
        double rowLat = (double) jt.getValueAt(dockIndex, 4);
        double rowLong = (double) jt.getValueAt(dockIndex, 5);

        selectedDock.setName(rowName);
        selectedDock.setPower_usage(rowPower);
        selectedDock.setMax_bikes(rowMax);
        selectedDock.setLat(rowLat);
        selectedDock.setLon(rowLong);

        return selectedDock.save();
    }

    /**
     * Creates a form with text fields to insert information about the new bike you want to register
     */
    private void newDock(){
        parentFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
        int height = 300;
        int width = 300;
        int x=0;
        int y=this.getRootPane().getHeight()-height-buttonBarHeight-10;

        if(card == null) {
            card = new DockingForm(parentFrame, this, x, y, width, height);
            repaint();
        }
        else{
            card.toggle();
            card.setBounds(x, y, width, height);
        }
    }

    /**
     * Disable a selected docking station
     */
    private void disableDockingStation(){
        int dockid = selectedDock.getId();

        UIManager UI = new UIManager();
        UI.put("OptionPane.background", Color.white);
        UI.put("Panel.background", Color.white);

        int option = showConfirmDialog(null, "Are you sure?", "WARNING", YES_NO_OPTION);
        if (option == YES_OPTION) {
            dockDAO.disableDockingStation(dockid);
            System.out.println("Disabled dockingStation with id: " + dockIndex);
            jt.updateTable(tableData());
        }
    }

    /**
     * Fills the JTable with data from the dockingStationList
     */
    public Object[][] tableData(){
        dockDAO.update();
        int length = dockDAO.getSize();
        Object[][] data = new Object[length][6];
        DockingStation temp;
        for(int i = 0; i < length; i++){
            temp = dockDAO.getDockingStation(i);
        try {
                data[i][0] = temp.getId();
                data[i][1] = String.valueOf(temp.getName());
                data[i][2] = temp.getPower_usage();
                data[i][3] = temp.getMax_bikes();
                data[i][4] = temp.getLat();
                data[i][5] = temp.getLon();

            } catch (NullPointerException e) {
            Cleaner.errorMessage(e, "tableData() nullpointer");
        }
        }
        return data;
    }

    /**
     * Close connection to the database
     */
    @Override
    public void closeCon() {

    }

    /**
     * close the popup form when you switch panels
     */
    @Override
    public void swapOut() {
        if(card != null) {
            card.hideCard();
        }
    }
}