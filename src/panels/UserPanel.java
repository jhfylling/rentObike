package panels;

import dao.BackgroundUpdater;
import dao.UserDAO;
import design.GuiColors;
import dto.User;
import forms.UserForm;
import gui.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.sql.Date;
import java.util.GregorianCalendar;

import gui.TablePanel;
import gui.ScrollPane;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The UserPanel class is used to create a panel to view user information and to manage this information.
 */

@SuppressWarnings("Duplicates")
public class UserPanel extends TablePanel {
    private Thread updateThread;
    private BackgroundUpdater backgroundUpdater;

    private String[] menuBarNames = {"ID","Card Info","Banned Until","Comments"};

    private ArrayList<User> userlist;
    private UserDAO userDAO;
    private int userIndex = -1;
    private Connection con;

    private ScrollPane sp;
    private Table jt;

    private JFrame parentFrame;
    private UserForm card;
    private EmptyPanel buttonBar;

    private User selectedUser;

    /**
     * A constructor to initialize the user panel and to load the initial information into the panel
     * @param con connection to the database
     */
    public UserPanel(Connection con) {
        super(new BorderLayout());
        this.con = con;
        onLoad();
        //update();

        setVisible(true);
        System.out.println("opened UserPanel");

    }

    /**
     * Loads all GUI elements to panel and initial data
     */
    private void onLoad(){
        //create bottom buttonbar and add buttons
        buttonBar = new EmptyPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        add(buttonBar, BorderLayout.SOUTH);
        addButtonsToButtonBar();

        //create UserDAO
        userDAO = new UserDAO(con);
        userlist = userDAO.getAllUsers();


        //create table and fill with tabledata
        DefaultTableModel jtModel;
        ArrayList<Class> columnTypes = new ArrayList<>();
        columnTypes.add(Integer.class);
        columnTypes.add(Object.class);
        columnTypes.add(Date.class);
        columnTypes.add(Object.class);
        jtModel = new RentoBikeTableModel(tableData(), menuBarNames, this, columnTypes);
        jt = new Table(jtModel);

        //add table to a scrollpane and add scrollpane to panel
        sp = new ScrollPane(jt);
        createListSelector();
        addMouseListnerToList();
        add(sp, BorderLayout.CENTER);

        //Start automatic updating
        backgroundUpdater = new BackgroundUpdater(menuBarNames, jt, this);
        updateThread = new Thread(backgroundUpdater);
        updateThread.start();
    }

    /**
     * A class to create a popup form
     */
    class RowPopup extends JPopupMenu{
        /**
         * Creates a table for the content of the popup
         * @param table the initial table for the popup form
         */
        public RowPopup (JTable table){
            JMenuItem[] menuItems = new JMenuItem[2];
            menuItems[0] = new JMenuItem("Ban");
            menuItems[1] = new JMenuItem("Unban");

            for(int i = 0; i < menuItems.length; i++){
                menuItems[i].setBackground(GuiColors.WHITE);
                menuItems[i].setForeground(GuiColors.BLACK);
            }

            setBackground(GuiColors.DARK);
            setFont(new Font("Arial",Font.PLAIN,12));
            setPreferredSize(new Dimension(150,66));
            setBorder(new EmptyBorder(1,1,1,1));

            menuItems[0].addActionListener(e -> banForm());
            menuItems[1].addActionListener(e -> unBanUser());


            for(int i = 0; i < menuItems.length;i++){
                add(menuItems[i]);
            }
        }
    }

    /**
     * Creates a popup menu if you right click a column.
     */
    private void addMouseListnerToList(){
        final RowPopup pop = new RowPopup(jt);
        jt.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me){
                if(SwingUtilities.isRightMouseButton(me)){
                    pop.show(me.getComponent(),me.getX(),me.getY());
                }
            }
        });
    }

    /**
     * Adds all buttons to the bottom button bar
     */
    private void addButtonsToButtonBar(){
        MenuButton banUser = new MenuButton("Ban user");
        banUser.setPreferredSize(new Dimension(200,50));
        buttonBar.add(banUser);
        banUser.addActionListener(e -> banForm());

        MenuButton unban = new MenuButton("Unban user");
        unban.setPreferredSize(new Dimension(200,50));
        buttonBar.add(unban);
        unban.addActionListener(e -> unBanUser());

    }

    /**
     * Opens up the CardForm to gather ban information
     */
    private void banForm(){
        parentFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
        int height = 200;
        int width = 300;
        int x=0;
        int y=this.getRootPane().getHeight()-height-60;

        if(card == null) {
            System.out.println("Creating card");
            card = new UserForm(parentFrame, this, x, y, width, height);
            repaint();
        }
        else{
            card.toggle();
            card.setBounds(x, y, width, height);
        }
    }

    /**
     * Called by CardForm to save formdata
     * @param input all form inputdata
     * @return
     */
    public boolean save(ArrayList<String> input){
        int days = 0;
        try {
            days = Integer.parseInt(input.get(0));
        }catch(NumberFormatException nfe){
            //Invalid input from form
            System.out.println("Ban days: "+nfe);
            return false;
        }
        String comment = input.get(1);
        banUser(days, comment);
        jt.updateTable(tableData());
        return true;
    }

    /**
     * ListSelectionListener is used to set selectedUser which is the user displayed in selected row
     */
    private void createListSelector(){
        ListSelectionModel select = jt.getSelectionModel();
        select.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        select.addListSelectionListener(e -> {
            if(!e.getValueIsAdjusting()) {
                userIndex = jt.getSelectedRow();
                if(userIndex != -1 && userIndex < jt.getRowCount()) {
                    int convertedIndex = jt.convertRowIndexToModel(userIndex);
                    selectedUser = userlist.get(convertedIndex);
                    System.out.println("id: "+selectedUser.getUserId());
                }
            }
        });
    }

    /**
     * @param days - number of days until automatic UnBan date
     * @return date of automatic UnBan
     */
    private static Date addDays(int days){
        Calendar date = new GregorianCalendar();
        date.getInstance();
        date.add(Calendar.DATE, days);
        java.sql.Date result = new java.sql.Date(date.getTimeInMillis());
        return result;
    }

    /**
     * Ban's selected user for specified number of days and adds comment for that ban
     * @param days number of days to be banned
     * @param comment a comment on why user was banned
     */
    private void banUser(int days, String comment){
        System.out.println("Banning user");
        Date ban;

        ban = addDays(days);
        selectedUser.ban(ban, comment);
    }

    /**
     * Remove ban from user
     * Makes call to User.unban();
     */
    public void unBanUser(){
        if(selectedUser != null && jt.getSelectedRow() != -1) {
            selectedUser.unban();
        }
        jt.updateTable(tableData());
    }


    /**
     * Updates the userlist and returns data to be presented in table
     * @return data to be presented in JTable
     */
    public Object[][] tableData(){
        userlist = userDAO.getAllUsers();
        int length = userlist.size();
        Object[][] data = new Object[length][4];
        User temp;
        for(int i = 0;i<length; i++){
            temp = userlist.get(i);
            data[i][0] = temp.getUserId();
            data[i][1] = String.valueOf(temp.getCard_info());
            if(temp.getBan_until()==null){
                data[i][2] = null;
            }else {
                data[i][2] = temp.getBan_until();
            }
            data[i][3] = temp.getComments();
        }
        return data;
    }

    /**
     * en empty overwritten method
     * @return false
     */
    @Override
    public boolean clickCellEdit() {
        return false;
    }

    /**
     * an empty overwritten method
     */
    @Override
    public void closeCon() {
    }

    /**
     * Prepares panel to be swapped out with a different one.
     * Method is called upon pressing a top menu button, swapping out the current panel
     * with selected panel
     */
    @Override
    public void swapOut() {
        if(card != null) {
            card.hideCard();
        }
    }
}
