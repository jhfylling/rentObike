package panels;

import design.GuiColors;
import forms.EditForm;
import gui.*;
import gui.Frame;
import gui.TablePanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The MenuPanel class is used to create a menu panel for the system.
 */

public class MenuPanel extends TablePanel {
    private Frame parentFrame;

    private EmptyPanel wrapper = new EmptyPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
    private ArrayList<MenuButton> buttons = new ArrayList<>();
    //private MenuButton exit = new MenuButton("EXIT");

    //controls panel and buttons
    private JPanel controls = new JPanel(new FlowLayout());
    private JLabel exit;
    private JLabel maxToggle;
    private JLabel tray;
    private JLabel profile;

    private Color hoverC = GuiColors.focusColor;
    private Color normalC = GuiColors.menuColor;

    private JFrame parentFrameTwo;
    private Connection con;
    private EditForm card;
    private final String adminEmail;

    private PictureLabel picLabel;

    /**
     * A constructor to initialize the menu panel and load the content of the panel
     * @param parent a parent panel to use as a reference when the child panel (MenuPanel) is going to execute a method
     */
    public MenuPanel(Frame parent, Connection con, String adminEmail) {
        //super(new FlowLayout(FlowLayout.LEFT, 0, 0));
        super(new BorderLayout());
        this.adminEmail = adminEmail;
        this.con = con;
        //layout = (FlowLayout) this.getLayout();
        setBackground(normalC);
        this.parentFrame = parent;
        onLoad();
        setVisible(true);
    }

    /**
     * Should be run upon loading the panel
     * Adds all components to the panel
     * Loads all buttons and sets actionlisteners
     */
    private void onLoad(){



        setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 3));

        buttons.add(new MenuButton("Statistics"));
        buttons.add(new MenuButton("Map"));
        buttons.add(new MenuButton("Administrators"));
        buttons.add(new MenuButton("Docking Stations"));
        buttons.add(new MenuButton("Bikes"));
        buttons.add(new MenuButton("Users"));



        BufferedImage pic = null;
        InputStream in = getClass().getResourceAsStream("/design/pictures/framelogots.png");
        try{
            //pic = ImageIO.read(new File("src/design/pictures/framelogots.png"));
            pic = ImageIO.read(in);
        }catch (Exception e){

        }
        picLabel = new PictureLabel(0,0,1,1,1,1,GridBagConstraints.CENTER,pic);
        picLabel.setOpaque(true);
        picLabel.setBackground(GuiColors.menuColor);
        picLabel.setPreferredSize(new Dimension(220,50));
        wrapper.add(picLabel,BorderLayout.LINE_START);



        int i = 0;
        for(MenuButton temp : buttons){
            /*
            temp.setBorder(BorderFactory.createEmptyBorder());
            temp.setOpaque(true);
            temp.setBackground(new GuiColors(69, 143, 209 ));
            temp.setFocusPainted(false);
            //temp.setSize(70, 90);
            temp.setPreferredSize(new Dimension(150, 100));
            */
            //temp.setForeground(new GuiColors(255, 255, 255));
            //temp.setFocusPainted(false);

            //add button to menu
            wrapper.add(temp);

            addListeners(temp);
            int finalI = i;
            temp.addActionListener(e -> parentFrame.changeContent(finalI));



            //Increment button counter
            i++;
        }

        //control buttons
        controls.setPreferredSize(new Dimension(161, 50));
        controls.setBackground(normalC);
        //ImageIcon icon = createImageIcon("/image/exit.png", "exit");

        Image image;
        ImageIcon icon;

        int labelWidth = 30;
        int labelHeight = 50;

        image = getScaledImage(getImage("/design/pictures/profileoutline.png"),40,40);
        icon = new ImageIcon(image,"profile");
        profile = new JLabel(icon);
        profile.setOpaque(true);
        profile.setBackground(normalC);
        profile.setPreferredSize(new Dimension(labelWidth,labelHeight));
        profile.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);
                editAdmin();
            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {
                super.mouseEntered(mouseEvent);
                profile.setBackground(hoverC);
            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {
                super.mouseExited(mouseEvent);
                profile.setBackground(normalC);
            }
        });


        //image = getScaledImage(getImage("src/design/pictures/tray.png"), 20, 20);
        image = getScaledImage(getImage("/design/pictures/tray.png"), 20, 20);
        icon = new ImageIcon(image, "to system tray");
        tray = new JLabel(icon);
        tray.setOpaque(true);
        tray.setBackground(normalC);
        tray.setPreferredSize(new Dimension(labelWidth, labelHeight));
        tray.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                parentFrame.setExtendedState(JFrame.ICONIFIED);
            }
            @Override
            public void mouseEntered(MouseEvent e) {
                super.mouseEntered(e);
                tray.setBackground(hoverC);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                super.mouseExited(e);
                tray.setBackground(normalC);
            }
        });

        //image = getScaledImage(getImage("src/design/pictures/max.png"), 20, 20);
        image = getScaledImage(getImage("/design/pictures/max.png"), 20, 20);
        icon = new ImageIcon(image, "max/min");
        maxToggle = new JLabel(icon);
        maxToggle.setOpaque(true);
        maxToggle.setBackground(normalC);
        maxToggle.setPreferredSize(new Dimension(labelWidth, labelHeight));
        maxToggle.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                toggleExtendedState();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                super.mouseEntered(e);
                maxToggle.setBackground(hoverC);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                super.mouseExited(e);
                maxToggle.setBackground(normalC);
            }
        });

        //image = getScaledImage(getImage("src/design/pictures/exit.png"), 20, 20);
        image = getScaledImage(getImage("/design/pictures/exit.png"), 20, 20);
        icon = new ImageIcon(image, "exit");
        exit = new JLabel(icon);
        exit.setOpaque(true);
        exit.setBackground(normalC);
        exit.setPreferredSize(new Dimension(labelWidth, labelHeight));
        exit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                parentFrame.dispose();
            }
            @Override
            public void mouseEntered(MouseEvent e) {
                super.mouseEntered(e);
                exit.setBackground(hoverC);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                super.mouseExited(e);
                exit.setBackground(normalC);
            }
        });

        controls.add(profile);
        controls.add(tray);
        controls.add(maxToggle);
        controls.add(exit);

        FrameDragListener frameDragListener = new FrameDragListener(parentFrame);
        this.addMouseListener(frameDragListener);
        this.addMouseMotionListener(frameDragListener);

        this.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if(e.getClickCount()==2){
                    toggleExtendedState();
                }
            }
        });


        add(wrapper, BorderLayout.LINE_START);

        //add controls
        add(controls, BorderLayout.LINE_END);
        //exit.setMargin(new Insets(0, 200, 0, 0));
        //exit.setAlignmentX(FlowLayout.RIGHT);
        //addListeners(exit);
        //exit.addActionListener(e -> parentFrame.dispose());

        //add(exit, BorderLayout.LINE_END);


    }

    /**
     * Scale the initial image to the height and width inserted into the parameters w and h
     * @param srcImg the initial image
     * @param w the intended width
     * @param h the intended height
     * @return a resized image
     */
    private Image getScaledImage(Image srcImg, int w, int h){
        BufferedImage resizedImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = resizedImg.createGraphics();

        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(srcImg, 0, 0, w, h, null);
        g2.dispose();

        return resizedImg;
    }

    /**
     * Creates a form for changing an admin password
     */
    private void editAdmin(){
        parentFrameTwo = (JFrame) SwingUtilities.getWindowAncestor(this);
        int height = 300;
        int width = 300;
        int x = (this.getRootPane().getWidth()/2) - (width/2);
        int y = (this.getRootPane().getHeight()/2) - (height/2);

        if(card == null) {
            card = new EditForm(parentFrameTwo,this, x,y,width,height, adminEmail,con);
            repaint();
        }else{
            card.toggle();
            card.setBounds(x, y, width, height);
        }
    }
    /**
     * Get an image
     * @param path the path to the initial image
     * @return the image
     */
    private Image getImage(String path){
        InputStream in = getClass().getResourceAsStream(path);
        try {
            System.out.println((new File(path).getCanonicalPath()));
            //BufferedImage img = ImageIO.read(new File(path));
            BufferedImage img = ImageIO.read(in);
            return img;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Create an image icon
     * @param path path to the image icon
     * @param description description of the icon if the picture does not load
     * @return Returns an ImageIcon, or null if the path was invalid.
     */
    protected ImageIcon createImageIcon(String path, String description) {
        java.net.URL imgURL = getClass().getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL, description);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }

    /**
     * minimize or maximize the frame
     */
    public void toggleExtendedState(){
        if(parentFrame.getExtendedState() == JFrame.MAXIMIZED_BOTH){
            parentFrame.setExtendedState(JFrame.NORMAL);
        }else {
            parentFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        }
    }

    /**
     * A help method to add listeners to different buttons
     * @param temp
     */
    private void addListeners(MenuButton temp){
        //set hover colors
        temp.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                    /*if(temp.getModel().isPressed()){
                        temp.setBackground(new GuiColors(82, 177, 249));
                    } */
                if(temp.getModel().isArmed()){
                    //Shoot back
                    temp.setBackground(hoverC);
                }
                if(temp.getModel().isRollover()){
                    temp.setBackground(hoverC);
                    temp.setForeground(GuiColors.focusTextColor);
                }else{
                    if(!temp.equals(parentFrame.getMostRecentFocusOwner())) {
                        temp.setBackground(normalC);
                    }
                }
            }
        });

        //add focus listener to set color of selected panel's button
        temp.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                temp.setBackground(hoverC);
            }

            @Override
            public void focusLost(FocusEvent e) {
                temp.setBackground(normalC);
            }
        });
    }

    /**
     * empty overwritten method
     */
    @Override
    public void closeCon() {

    }

    /**
     * empty overwritten method
     */
    @Override
    public void swapOut() {

    }

    /**
     * empty overwritten method
     */
    @Override
    public boolean save(ArrayList<String> input) {
        return false;
    }

    /**
     * empty overwritten method
     */
    @Override
    public Object[][] tableData() {
        return new Object[0][];
    }

    /**
     * empty overwritten method
     */
    @Override
    public boolean clickCellEdit() {
        return false;
    }
}
