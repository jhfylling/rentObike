package panels;

import design.GuiColors;
import dto.StatsContainer;
import gui.TablePanel;
import statistics.*;
import dao.StatisticsDAO;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.sql.Connection;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The StatisticsPanel class is used to create a panel to view statistics.
 */

public class StatisticsPanel extends TablePanel {
    private StatisticsDAO stats;
    private StatsContainer statsContainer;

    private JPanel monthlyRepairWrapper, dailyTripsWrapper, comparisonChartWrapper, avarageTripsWrapper, monthlyTripWrapper, finalWrapper;
    private JPanel chartContainer, chartContainerTwo, statsWrapper, chartWrapper;


    /**
     * A constructor to initialize a statistics panel and to load initial data into the panel
     */
    public StatisticsPanel(Connection con) {
        super(new BorderLayout());
        System.out.println("Created staticsticsPanel");
        stats = new StatisticsDAO(con);
        onLoad();
        setBackground(GuiColors.DARK);
        StatisticUpdater sup = new StatisticUpdater(this);
        sup.start();
    }

    /**
     * Should be run upon loading the panel
     * Adds all components to the panel
     */
    private void onLoad() {
        //get stats
        statsContainer = stats.getStatscontainer();
        //removeall
        removeAll();
        //create graphs
        createChartContainers(statsContainer);
        setSelectedStats(statsContainer);

        chartWrapper = new JPanel(new GridBagLayout());
        chartWrapper.add(chartContainer, getGbc(0,0,1,1,1,1,GridBagConstraints.CENTER));
        chartWrapper.add(chartContainerTwo,getGbc(0,1,1,1,1,1,GridBagConstraints.CENTER));
        add(statsWrapper, BorderLayout.WEST);
        add(chartWrapper, BorderLayout.CENTER);

        //repaint
        repaint();
    }

    /**
     * Used by thread to update statistics
     */
    public void updateStats(){
        onLoad();
    }
    /**
     * Set stats for different statistical information
     */
    private void setSelectedStats(StatsContainer statsContainer){
        double earnings = statsContainer.getEarnings();
        int numberOfUsers = statsContainer.getNumberOfUsers();
        int numberOfTrips = statsContainer.getNumberOfTrips();
        int onlyBeenOnOneTrip = statsContainer.getOnlyBeenOnOneTrip();
        double totalRepairCost = statsContainer.getTotalRepairCost();
        double powerUsage = statsContainer.getPowerUsage();

        String text = "<html><h1>Statistics</h1><h3>Total Earnings: " + earnings + ",-kr <br><br>Unique Users:  " + numberOfUsers +
                " <br><br> One Time Users: "+ onlyBeenOnOneTrip +"<br><br>Totalt Trips Taken: "+ numberOfTrips +
                "<br><br>Avarage Trips Per User: "+ numberOfTrips/numberOfUsers + "<br><br>Repair Costs: " + totalRepairCost + ",-kr<br><br>" +
                "Power Usage: " + powerUsage + " kWH</h3></html>";

        statsWrapper = new JPanel(new BorderLayout());
        statsWrapper.setBackground(GuiColors.DARK);
        statsWrapper.setPreferredSize(new Dimension(223,140));
        JLabel statsText = new JLabel(text);
        statsText.setBorder(new EmptyBorder(0,10,0,0));
        statsWrapper.add(statsText,BorderLayout.PAGE_START);
        statsText.setForeground(GuiColors.GRAY_TEXT);
    }

    /**
     * Create a chart container to use in the panel
     */
    private void createChartContainers(StatsContainer statsContainer){
        chartContainer = new JPanel(new GridLayout(0,3));
        chartContainer.setPreferredSize(new Dimension(1077,400));

        chartContainerTwo = new JPanel(new GridLayout(0,3));
        chartContainerTwo.setPreferredSize(new Dimension(1077,400));

        createChartWrappers();
        createCharts(statsContainer);
        addWrappersToChartContainer();

    }

    /**
     * Create charts to use in the panel
     */
    private void createCharts(StatsContainer statsContainer){
        JPanel monthlyRepairChart = new MonthlyRepairsBarChart().createPanel(statsContainer);
        monthlyRepairWrapper.add(monthlyRepairChart);


        JPanel dailyTripsChart = new MonthlyTripsLineChart().createPanel(statsContainer);
        dailyTripsWrapper.add(dailyTripsChart);

        JPanel comparisonChart = new EconomyBarChart().createPanel(statsContainer);
        comparisonChartWrapper.add(comparisonChart);

        JPanel avarageBikeChart = new TripsBarChart().createPanel(statsContainer);
        avarageTripsWrapper.add(avarageBikeChart);

        JPanel monthlyUsers = new MonthlyUsersLineChart().createPanel(statsContainer);
        monthlyTripWrapper.add(monthlyUsers);

        JPanel hourlyTrips = new HourlyTripsLineChart().createPanel(statsContainer);
        finalWrapper.add(hourlyTrips);
    }

    /**
     * Adds wrappers to the chart containers
     */
    private void addWrappersToChartContainer(){
        chartContainer.add(monthlyRepairWrapper);
        chartContainer.add(dailyTripsWrapper);
        chartContainer.add(comparisonChartWrapper);


        chartContainerTwo.add(avarageTripsWrapper);
        chartContainerTwo.add(monthlyTripWrapper);
        chartContainerTwo.add(finalWrapper);

    }

    /**
     * A method to create chart wrappers
     */
    private void createChartWrappers(){
        monthlyRepairWrapper = new JPanel();
        monthlyRepairWrapper.setLayout(new BoxLayout(monthlyRepairWrapper,BoxLayout.Y_AXIS));
        monthlyRepairWrapper.setBackground(GuiColors.WHITE);

        dailyTripsWrapper = new JPanel();
        dailyTripsWrapper.setLayout(new BoxLayout(dailyTripsWrapper,BoxLayout.Y_AXIS));
        dailyTripsWrapper.setBackground(GuiColors.WHITE);

        comparisonChartWrapper = new JPanel();
        comparisonChartWrapper.setLayout(new BoxLayout(comparisonChartWrapper,BoxLayout.Y_AXIS));
        comparisonChartWrapper.setBackground(GuiColors.WHITE);

        avarageTripsWrapper = new JPanel();
        avarageTripsWrapper.setLayout(new BoxLayout(avarageTripsWrapper,BoxLayout.Y_AXIS));
        avarageTripsWrapper.setBackground(GuiColors.WHITE);

        monthlyTripWrapper = new JPanel();
        monthlyTripWrapper.setLayout(new BoxLayout(monthlyTripWrapper,BoxLayout.Y_AXIS));
        monthlyTripWrapper.setBackground(GuiColors.WHITE);

        finalWrapper = new JPanel();
        finalWrapper.setLayout(new BoxLayout(finalWrapper,BoxLayout.Y_AXIS));
        finalWrapper.setBackground(GuiColors.WHITE);

    }


    /**
     * close connection to the database
     */
    public void closeCon(){
        stats.close();
    }

    /**
     * empty method for heritage
     */
    @Override
    public void swapOut() {

    }

    /**
     * empty method for heritage
     * @param input an string array with input
     * @return false (empty method)
     */
    @Override
    public boolean save(ArrayList<String> input) {
        return false;
    }

    /**
     * An empty overwritten method
     * @return an empty object table.
     */
    @Override
    public Object[][] tableData() {
        return new Object[0][];
    }

    /**
     * an empty overwritten method that returns false
     * @return false
     */
    @Override
    public boolean clickCellEdit() {
        return false;
    }

    /**
     * A grid bad constraint method to use when you place the charts in the panel
     * @param x the x coordinate
     * @param y the y coordinate
     * @param weightX space to use in x direction
     * @param weightY space to use in y direction
     * @param spanX number of columns to span
     * @param spanY number of rows to span
     * @param anchor gridbag to place object in cell
     * @return the initial gbc object
     */
    public GridBagConstraints getGbc(int x, int y, int weightX, int weightY, int spanX, int spanY, int anchor){
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.weightx = weightX;
        gbc.weighty = weightY;
        gbc.gridwidth = spanX;
        gbc.gridheight = spanY;
        gbc.anchor = anchor;
        gbc.fill = GridBagConstraints.BOTH;
        return gbc;
    }
}
