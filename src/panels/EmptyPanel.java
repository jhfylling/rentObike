package panels;

import javax.swing.*;
import java.awt.*;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The EmptyPanel class is used to create a standard buffered JPanel with the specified layout manager.
 */

public class EmptyPanel extends JPanel{
    /**
     * Create a new buffered JPanel with the specified layout manager
     */
    public EmptyPanel(LayoutManager layout) {
        super(layout);
    }

}
