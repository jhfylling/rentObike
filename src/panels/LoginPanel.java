package panels;

import design.GuiColors;
import forms.AdminForm;
import forms.ForgottenPasswordForm;
import gui.*;
import gui.Button;
import gui.Frame;
import gui.Label;
import gui.TextField;
import registration.PasswordValidator;
import dao.AdminDAO;

import java.awt.*;
import java.awt.event.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The LoginPanel class is used to create a panel to login to the system.
 */

public class LoginPanel extends TablePanel {

    private final Frame parentFrame;
    private JPanel loginPanel;

    private final PasswordValidator passwordValidator = new PasswordValidator();
    private final Connection con;
    private final AdminDAO adminDAO;

    private ActionListener action;

    private Label emailLabel;
    private Label passwordLabel;
    private Label loginFailedLabel;
    private TextField emailTextField;
    private PasswordField passwordTextField;
    private Button loginButton;
    private Button forgottenButton;
    private Button exitButton;
    private boolean loginValid;

    private PictureLabel picLabel;
    private ForgottenPasswordForm card;

    /**
     * A constructor to create a panel to login
     * @param frame a frame object for the panel
     */
    public LoginPanel(Frame frame, Connection con) {
        super(new GridBagLayout());
        this.con = con;
        adminDAO = new AdminDAO(con);
        loginPanel = this;
        parentFrame = frame;
        onload();
        parentFrame.getRootPane().setDefaultButton(loginButton);
    }

    /**
     * Should be run upon loading the panel
     * Adds all components to the panel
     */
    private void onload(){
        BufferedImage pic = null;
        InputStream in = getClass().getResourceAsStream("/design/pictures/logo6.png");
        try{
            pic = ImageIO.read(in);
        }catch (Exception e){

        }

        add(picLabel = new PictureLabel(0,1,1,1,2,1,GridBagConstraints.CENTER,pic),picLabel.getGbc());

        emailLabel = new Label(0,2,1,1,1,1,GridBagConstraints.CENTER,"Email ");
        emailLabel.setFont(new Font("Arial Black", Font.BOLD, 20));
        emailLabel.setForeground(Color.white);
        add(emailLabel,emailLabel.getGbc());

        emailTextField = new TextField(1,2,1,1,1,1, GridBagConstraints.WEST, 15);
        emailTextField.setFont(new Font("Arial Black", Font.BOLD, 15));
        add(emailTextField,emailTextField.getGbc());

        passwordLabel = new Label(0,3,1,1,1,1,GridBagConstraints.CENTER,"Password ");
        passwordLabel.setFont(new Font("Arial Black", Font.BOLD, 20));
        passwordLabel.setForeground(Color.WHITE);
        add(passwordLabel,passwordLabel.getGbc());

        passwordTextField = new PasswordField(1,3,1,1,1,1,GridBagConstraints.WEST,15);
        passwordTextField.setFont(new Font("Arial Black", Font.BOLD, 15));
        add(passwordTextField,passwordTextField.getGbc());
        Label fill;
        add(fill = new Label(0,5,1,1,2,1,GridBagConstraints.CENTER,"  "),fill.getGbc());

        loginButton = new Button(1,6,1,1,2,1,GridBagConstraints.WEST,"Enter");
        loginButton.setFont(new Font("Arial Black", Font.BOLD, 15));
        loginButton.setFocusPainted(false);
        loginButton.setBorderPainted(false);
        loginButton.setBackground(Color.BLACK);
        loginButton.setForeground(Color.WHITE);
        add(loginButton, loginButton.getGbc());
        loginButton.addActionListener(action = actionEvent -> {

            loginValid =  passwordValidator.validatePassword(String.valueOf(passwordTextField.getPassword()),adminDAO.getPasswordFromDB(emailTextField.getText()));
            if(loginValid){
                add(loginFailedLabel = new Label(0,0,1,1,2,1, GridBagConstraints.CENTER, "Login Successful"),loginFailedLabel.getGbc());

                parentFrame.start(emailTextField.getText());

            }else{
                add(loginFailedLabel = new Label(0,5,1,1,2,1, GridBagConstraints.CENTER, "Wrong email or password"),loginFailedLabel.getGbc());
                loginFailedLabel.setForeground(Color.red);
                updateUI();
            }
        });

        forgottenButton = new Button(1,4,1,1,2,1,GridBagConstraints.WEST,"Forgot password");
        forgottenButton.setForeground(GuiColors.WHITE);
        forgottenButton.setBorderPainted(false);
        forgottenButton.setContentAreaFilled(false);
        forgottenButton.setOpaque(false);
        forgottenButton.addActionListener(actionEvent -> openCard());
        add(forgottenButton, forgottenButton.getGbc());


        exitButton = new Button(1,0,1,1,2,1,GridBagConstraints.EAST,"X");
        exitButton.setFont(new Font("Arial Black", Font.BOLD, 10));
        exitButton.setFocusPainted(false);
        exitButton.setBorderPainted(false);
        exitButton.setBackground(Color.BLACK);
        exitButton.setForeground(Color.WHITE);
        exitButton.addActionListener(actionEvent -> {
            parentFrame.setVisible(false);
            parentFrame.dispose();
        });

        add(exitButton,exitButton.getGbc());



    }

    /**
     * Creates forgotten password form
     */
    private void openCard(){
        JFrame parentFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
        int height = 130;
        int width = 300;
        int x = (this.getRootPane().getWidth()/2) - (width/2);
        int y = (this.getRootPane().getHeight()/2) - (height/2);

        if (card == null) {
            card = new ForgottenPasswordForm(parentFrame,this,x,y,width,height);
            repaint();
        } else {
            card.toggle();
            card.setBounds(x, y, width, height);
        }
    }

    /**
     * insert a image into the panel
     * @param g a graphics object
     */
    @Override
    protected void paintComponent(Graphics g) {
        InputStream in = getClass().getResourceAsStream("/design/pictures/color.jpg");
        BufferedImage pic = null;
        try{
            pic = ImageIO.read(in);
        }catch (Exception e){

        }


        super.paintComponent(g);
        g.drawImage(pic, 0, 0, null);
    }

    /**
     * Executes sendNewPassword function from adminDao
     * @param input an string array with input
     * @return true or false
     */
    @Override
    public boolean save(ArrayList<String> input) {
        if(input.size() != 0){
            if(adminDAO.sendNewPassword(input.get(0))){
                return true;
            }
        }
        return false;
    }

    @Override
    public void closeCon() {

    }

    @Override
    public void swapOut() {

    }

    @Override
    public Object[][] tableData() {
        return new Object[0][];
    }

    @Override
    public boolean clickCellEdit() {
        return false;
    }
}