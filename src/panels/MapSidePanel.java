package panels;

import design.GuiColors;
import dto.Bike;
import dao.BikeDAO;
import dto.DockingStation;
import dao.DockingStationDAO;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The MapSidePanel class is used to create a side panel to show information about bikes and docking stations on the map.
 */

public class MapSidePanel extends JPanel {

    private int width;
    private ArrayList<JLabel> labels;
    private MapsPanel parentPanel;
    private JPanel info;


    private Bike bike;
    private BikeDAO bikeDAO;

    private DockingStation dock;
    private DockingStationDAO dockDAO;


    /**
     * A constructor to initialize the side panel inside the map panel
     * @param width width of the panel
     * @param con connection to the database
     * @param parentPanel a parent panel to use as a reference when the child panel (MapSidePanel) is going to execute a method
     */
    public MapSidePanel(int width, Connection con, MapsPanel parentPanel){
        setLayout(new BorderLayout());
        //setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.width = width;
        this.parentPanel = parentPanel;
        //info = new JPanel(new BoxLayout(info, BoxLayout.Y_AXIS));
        info = new JPanel();
        info.setLayout(new BoxLayout(info, BoxLayout.Y_AXIS));
        add(info, BorderLayout.CENTER);
        labels = new ArrayList<>();
        labels.add(new JLabel("Selected bike: none"));
        labels.add(new JLabel("Bike Type: N/A"));
        labels.add(new JLabel("Make: N/A"));
        labels.add(new JLabel("Charge: N/A"));
        labels.add(new JLabel(" "));

        boolean alternate = false;
        for(JLabel label : labels){
            label.setFont(new Font("Arial", Font.PLAIN, 16));
            label.setForeground(GuiColors.GRAY_TEXT);
            info.add(label);

            label.setMinimumSize(new Dimension(width, 30));
            label.setMaximumSize(new Dimension(width, 30));

            label.setBorder(new EmptyBorder(0,5,0,0));

            label.setOpaque(true);
            if(alternate) {
                label.setBackground(GuiColors.ODD_DARK);
                alternate = false;
            }
            else{
                label.setBackground(GuiColors.DARK);
                alternate = true;
            }
        }

        setStyle();

        bikeDAO = new BikeDAO(con);
        dockDAO = new DockingStationDAO(con);

        addCloseButton();

    }

    /**
     * Called by constructor to set panel's style such as width and color
     */
    private void setStyle(){
        setPreferredSize(new Dimension(width, 500));    //sidepanel is placed in MapsPanel by BorderLayout.EAST/WEST and height is ignored.
        info.setBackground(GuiColors.DARK);                  //set background color
        info.setForeground(Color.WHITE);
    }

    /**
     * Selects bike and presents info on sidePanel
     * @param id of selected bike on map
     */
    public void setBike(int id){
        bike = bikeDAO.getBike(id);
        labels.get(0).setText("Bike ID: " + String.valueOf(bike.getBike_id()));
        labels.get(1).setText("Type: " + bike.getType().toString());
        labels.get(2).setText("Make: " + bike.getMake());
        labels.get(3).setText("Charge: " + String.valueOf(bike.getCharge_level()));
        labels.get(4).setText(" ");
    }

    /**
     * Selects dock and presents info on sidePanel
     * @param id of selected docking station on map
     */
    public void setDock(int id){
        dock = dockDAO.getDock(id);
        labels.get(0).setText("Dock ID: "+String.valueOf(dock.getId()));
        labels.get(1).setText("Name: "+String.valueOf(dock.getName()));
        labels.get(2).setText("Consumption: "+String.valueOf(dock.getPower_usage()));
        labels.get(3).setText("Maximum bikes: "+String.valueOf(dock.getMax_bikes()));
        labels.get(4).setText("Number of bikes: "+String.valueOf(dock.getNumberOfBikesAtStation()));
    }

    /**
     * Creates a button to close the side panel
     */
    private void addCloseButton(){
        MapSidePanel sidePanel = this;
        JPanel wrappingPanel = new JPanel(new BorderLayout());
        wrappingPanel.setBackground(GuiColors.DARK);

        JButton button = new JButton("Close");
        button.setForeground(GuiColors.GRAY_TEXT);
        button.setFont(new Font("Arial", Font.BOLD, 15));
        button.setMaximumSize(new Dimension(30,30));
        button.setFocusPainted(false);
        button.setBorderPainted(false);
        button.setBackground(Color.BLACK);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                parentPanel.remove(sidePanel);
                parentPanel.validate();
            }
        });
        wrappingPanel.add(button,BorderLayout.EAST);
        add(wrappingPanel,BorderLayout.AFTER_LAST_LINE);
    }

}
