package simulation;

import sql.Cleaner;
import sql.DBconnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The SimTrip class is used as a trip DAO connected to a specific trip
 */
public class SimTrip {

    private int tripID;

    private final DBconnection con = new DBconnection();

    /**
     * Creates a trip in DB
     * @param simUser
     * @param simBike
     */
    public SimTrip(SimUser simUser, SimBike simBike ) {
        PreparedStatement statement = null;
        ResultSet res = null;
        try{
            con.connect();

            String sentence = "INSERT INTO trip(trip_id,start_time, from_dock, user_id, bike_id) VALUES(DEFAULT,CURRENT_TIMESTAMP ,?,?,?)";
            statement = con.getCon().prepareStatement(sentence);
            statement.setInt(1,simBike.getDockID());
            statement.setInt(2,simUser.getUserID());
            statement.setInt(3,simBike.getBikeID());
            statement.executeUpdate();

            sentence = "SELECT trip_id FROM trip WHERE bike_id = ? and end_time IS NULL";
            statement = con.getCon().prepareStatement(sentence);
            statement.setInt(1,simBike.getBikeID());
            res = statement.executeQuery();
            if(res.next()) tripID = res.getInt("trip_id");

        }catch (Exception e ){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
            con.disconnect();
        }
    }

    public int getTripID() {
        return tripID;
    }

    /**
     * Registeres finished trip in DB
     * @param distance
     * @param dockID
     */
    public void endTrip(int distance, int dockID){
        PreparedStatement statement = null;
        ResultSet res = null;
        try{
            con.connect();
            String sentence = "UPDATE trip SET distance = ? , to_dock = ?, end_time = CURRENT_TIME where trip_id = ?";
            statement = con.getCon().prepareStatement(sentence);
            statement.setInt(1,distance);
            statement.setInt(2,dockID);
            statement.setInt(3,tripID);
            statement.executeUpdate();
        }catch (Exception e ){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
            con.disconnect();
        }
    }
}
