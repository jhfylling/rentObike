package simulation;

import sql.Cleaner;
import sql.DBconnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The SimUser class is used as a user DAO
 */
public class SimUser {

    private final int userID;

    private final DBconnection con = new DBconnection();


    public SimUser(int userID) {
        this.userID = userID;
    }
    public int getUserID(){
        return userID;
    }

    /**
     * Registeres a payment in the DB
     * @param amount
     * @param dockID
     * @param tripID
     */
    public void placePayment(double amount, int dockID, int tripID){
        PreparedStatement statement = null;
        ResultSet res = null;
        try{
            con.connect();
            String sentence = "INSERT INTO transaction VALUES(DEFAULT,?,DEFAULT ,'CARD',?,?)";
            statement = con.getCon().prepareStatement(sentence);
            statement.setDouble(1,amount);
            statement.setInt(2,dockID);
            statement.setInt(3,tripID);
            statement.execute();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
            con.disconnect();
        }
    }
}
