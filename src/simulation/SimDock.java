package simulation;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The SimDock class is used as docking station DTO for the simulation
 */
public class SimDock {

    private final int dockID;

    public SimDock(int dockID){
        this.dockID = dockID;
    }


    public int getDockID(){
        return dockID;
    }

}
