package simulation;

import sql.Cleaner;
import sql.DBconnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The SimBike class is used as a bike DAO connected to a specific bike.
 */

public class SimBike {

    public final static int DECREASECHARGE = 1;
    public final static int INCREASECHARGE = 2;

    private final DBconnection con = new DBconnection();
    private final int bikeID;

    /**
     *
     * @param bikeID
     */
    public SimBike(int bikeID){
        this.bikeID = bikeID;
    }
    public int getBikeID(){
        return bikeID;
    }

    /**
     * Set docking_id to null in DB
     */
    public void checkoutBike(){
        PreparedStatement statement = null;
        ResultSet res = null;
        try{
            con.connect();
            String sentence = "UPDATE bike SET dock_id = null WHERE bike_id = ?";
            statement = con.getCon().prepareStatement(sentence);
            statement.setInt(1,bikeID);
            statement.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
            con.disconnect();
        }
    }

    /**
     * Set docking_id to given dockID
     * @param dockID
     */
    public void checkinBike(int dockID){
        PreparedStatement statement = null;
        ResultSet res = null;
        double docklat = 0;
        double docklng = 0;
        try{
            con.connect();
            String sentence = "SELECT latitude,longitude FROM dock WHERE dock_id = ?";
            statement = con.getCon().prepareStatement(sentence);
            statement.setInt(1,dockID);
            res = statement.executeQuery();
            if(res.next()){
                docklat = res.getDouble("latitude");
                docklng = res.getDouble("longitude");
            }
            sentence = "UPDATE bike SET dock_id = ? WHERE bike_id = ?";
            statement = con.getCon().prepareStatement(sentence);
            statement.setInt(1,dockID);
            statement.setInt(2,bikeID);
            statement.executeUpdate();



            sentence = "UPDATE position SET latitude = ?, longitude = ? WHERE bike_id = ? AND time_position =" +
                    " (SELECT * FROM (Select max(time_position)FROM rentobike.position WHERE bike_id = ?) as maxtime);";
            statement = con.getCon().prepareStatement(sentence);
            statement.setDouble(1,docklat);
            statement.setDouble(2,docklng);
            statement.setInt(3,bikeID);
            statement.setInt(4,bikeID);
            statement.executeUpdate();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
            con.disconnect();
        }
    }

    /**
     * Updates bikes position based on previous position
     * @param tripID
     */

    public void updateBikePosition(int tripID){
        ArrayList<Double> latAndLong = getChangedCoordinates();
        PreparedStatement statement = null;
        ResultSet res = null;
        con.connect();
        try{
            String sentence = "INSERT INTO position (position_id, latitude, longitude, time_position ,bike_id,trip_id) VALUES (DEFAULT,?,?,CURRENT_TIMESTAMP,?,?)";
            statement = con.getCon().prepareStatement(sentence);
            statement.setDouble(1,latAndLong.get(0));
            statement.setDouble(2,latAndLong.get(1));
            statement.setInt(3,bikeID);
            statement.setInt(4,tripID);
            statement.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
        }finally {

            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
            con.disconnect();
        }
    }

    /**
     * Creates Arraylist containing new coordinates based on bikes previous coordinates
     * @return
     */
    private ArrayList<Double> getChangedCoordinates(){
        ArrayList<Double> cordinates = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet res = null;
        try{
            con.connect();
            String sentence = "SELECT bike_id, longitude, latitude FROM position WHERE time_position IN" +
                    "(SELECT MAX(time_position) FROM position GROUP BY bike_id) and bike_id = ?";
            statement = con.getCon().prepareStatement(sentence);
            statement.setInt(1,bikeID);
            res = statement.executeQuery();
            if(res.next()){
                double lat = res.getDouble("latitude");
                double lng = res.getDouble("longitude");
                cordinates.add((lat - randomPosChange()));
                cordinates.add((lng - randomPosChange()));
            }
            return cordinates;

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
            con.disconnect();
        }
        return cordinates;
    }

    /**
     *
     * @return 0.001 or 0.000
     */
    private double randomPosChange(){
        Random randomgen = new Random();
        if(randomgen.nextInt(2) == 1){
            return 0.001;
        }else {
            return 0.000;
        }
    }

    /**
     * Used to change charge level of a bike
     * @param INCREASEorDECREASE decides whether charge level should increase or decrease
     */
    public void changeChargeLevel(int INCREASEorDECREASE){
        PreparedStatement statement = null;
        ResultSet res = null;
        con.connect();
        int currentChargeLevel;
        try{
            String sentence = "SELECT * FROM bike where bike_id = ?";
            statement = con.getCon().prepareStatement(sentence);
            statement.setInt(1,bikeID);
            res = statement.executeQuery();

            if(res.next()){
                currentChargeLevel = res.getInt("charge_level");

                sentence = "UPDATE bike SET charge_level = ? WHERE bike_id = ?";
                statement = con.getCon().prepareStatement(sentence);
                int randomChange = randomChangeInChargeLevel();
                if(INCREASEorDECREASE == DECREASECHARGE && (currentChargeLevel - randomChange) >= 0){
                    statement.setInt(1,currentChargeLevel - randomChange);
                    statement.setInt(2,bikeID);
                    statement.executeUpdate();
                }else if(INCREASEorDECREASE == INCREASECHARGE){
                    statement.setInt(1,currentChargeLevel + randomChange);
                    statement.setInt(2,bikeID);
                    statement.executeUpdate();
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
            con.disconnect();
        }
    }

    /**
     *
     * @return returns a random number between 0 and 3
     */
    private int randomChangeInChargeLevel(){
        Random randomGen = new Random();
        return randomGen.nextInt(3);
    }

    /**
     *
     * @return current dock_id from DB
     */
    public int getDockID(){
        PreparedStatement statement = null;
        ResultSet res = null;
        try{
            con.connect();
            String sentence = "SELECT dock_id FROM bike WHERE bike_id = ?";
            statement = con.getCon().prepareStatement(sentence);
            statement.setInt(1,bikeID);
            res = statement.executeQuery();
            if(res.next()) return res.getInt("dock_id");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
            con.disconnect();
        }
        return -1;
    }


}
