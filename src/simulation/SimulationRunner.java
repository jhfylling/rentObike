package simulation;


import sql.Cleaner;
import sql.DBconnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The SimulationRunner class is used to start and stop simulations.
 */

public class SimulationRunner {
    private static final DBconnection con = new DBconnection();
    private static int numBikeSimulations;
    private static int numDockSimulations;

    public static boolean continueSimulation = true;

    /**
     * Loads bikes and docking station
     * Starts a thread for each bike and docking station loaded
     * First args element defines number of simulations that will start.
     * @param args
     */
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        ArrayList<Simulation> simulations = new ArrayList<>();
        ArrayList<DockThread> dockThreads = new ArrayList<>();
        ArrayList<SimDock> simDocks = getDocks();
        ArrayList<SimBike> simBikes = getBikes();

        int bikeCount = 10;
        if(args.length > 0){
            try {
                bikeCount = Integer.parseInt(args[0]);
            }catch(NumberFormatException nfe) {
                System.out.println("Invalid argument. Use integer for bikecount");
                System.out.println("Starting simulation with "+bikeCount+" bikes");
            }
        }else{
            //bikeCount = simBikes.size();
            bikeCount = 10;
        }

        System.out.println("Starting simulation with "+bikeCount+" bikes");
        numBikeSimulations = bikeCount;
        //numBikeSimulations = 30;

        numDockSimulations = simDocks.size();

        for(int i = 0; i < numBikeSimulations; i ++){
            simulations.add(new Simulation(simBikes.get(i),simDocks));
        }
        for(int i = 0; i < numDockSimulations; i ++){
            dockThreads.add(new DockThread(simDocks.get(i).getDockID()));
        }

        for(DockThread aDock : dockThreads){
            aDock.start();
        }

        for(Simulation sim : simulations){
            sim.start();
            try{
                TimeUnit.MILLISECONDS.sleep(500);
            }catch (Exception e){

            }
        }
        try{
            TimeUnit.SECONDS.sleep(12);
        }catch (Exception e){

        }

        System.out.println("All threads started.");
        System.out.println("Type stop to stop simulation.");
        boolean run = true;
        while(run) {
            if (in.hasNext()) {
                if (in.next().equals("stop")) {
                    System.out.println("Stopping simulation. Docking all bikes can take up to one minute");
                    continueSimulation = false;
                    run = false;
                }
                else{
                    System.out.println("Type stop to stop simulation");
                }
            }
        }
    }

    /**
     *
     * @return list of all bikes that are available for simulation
     */
    public static ArrayList<SimBike> getBikes (){
        ArrayList<SimBike> bikes = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet res = null;
        try{
            con.connect();
            String sentence =   "SELECT  position.bike_id, max(time_position) FROM position\n" +
                                "WHERE position.bike_id\n" +
                                "NOT IN (SELECT bike.bike_id FROM  bike JOIN repairs r ON bike.bike_id = r.bike_id WHERE repair_description IS NULL AND request_description IS NOT NULL)\n" +
                                "AND position.bike_id NOT IN (SELECT bike.bike_id FROM bike WHERE isEnabled IS FALSE)\n"+
                                "GROUP BY position.bike_id;";
            statement = con.getCon().prepareStatement(sentence);
            res = statement.executeQuery();
            while(res.next()){
                bikes.add(new SimBike(res.getInt("bike_id")));
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
            con.disconnect();
        }
        return bikes;
    }
    /**
     *
     * @return a list of all docking stations
     */
    public static ArrayList<SimDock> getDocks (){
        ArrayList<SimDock> docks = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet res = null;
        try{
            con.connect();
            String sentence = "SELECT * FROM dock";
            statement = con.getCon().prepareStatement(sentence);
            res = statement.executeQuery();
            while(res.next()){
                docks.add(new SimDock(res.getInt("dock_id")));
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
            con.disconnect();
        }
        return docks;
    }
}

