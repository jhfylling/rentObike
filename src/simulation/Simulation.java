package simulation;

import registration.RandomString;
import sql.Cleaner;
import sql.DBconnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Random;
/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The Simulation class is used to test and simulate system usage.
 */

public class Simulation implements Runnable {

    private Thread thread;

    private final DBconnection con = new DBconnection();
    private final SimBike simBike;
    private  SimUser simUser;
    private  SimTrip simTrip;
    private final ArrayList<SimDock> stations;

    /**
     *
     * @param simBike
     * @param stations all registered docking station
     */
    public Simulation(SimBike simBike, ArrayList<SimDock> stations) {
        this.simBike = simBike;
        this.stations = stations;
    }

    /**
     * Creates the thread and starts the simulation
     */
    public void start(){
        if(thread == null){
            thread = new Thread(this,"SimThread");
            thread.start();
        }
    }

    /**
     * Contains the simulation loop
     */
    public void run(){
        int distance;
        while(SimulationRunner.continueSimulation){
            distance = 0;
            setUser();
            simTrip = new SimTrip(simUser, simBike);
            simUser.placePayment(50,simBike.getDockID(),simTrip.getTripID());
            simBike.checkoutBike();
            for(int i = 0; i < 4; i ++){
                normalMovementLoop();
                distance ++;
            }
            int checkIndock = randomDock();
            simBike.checkinBike(checkIndock);
            simTrip.endTrip(distance,checkIndock);
            chargeLoop(3);
        }
        System.out.println("Stopping thread");
    }

    /**
     *
     * @return a random docking stations ID
     */
    private int randomDock(){
        Random randomGen = new Random();
        return stations.get(randomGen.nextInt(stations.size())).getDockID();
    }
    private void chargeLoop(int numberofSeconds){
        try{
            for (int i = 0; i < numberofSeconds; i++) {
                if(!SimulationRunner.continueSimulation) break;
                simBike.changeChargeLevel(simBike.INCREASECHARGE);
                thread.sleep(60000);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Updates bike position and charge level
     */
    private void normalMovementLoop(){
        try{
            for(int i = 0; i < 2; i ++){
                if(!SimulationRunner.continueSimulation) break;
                simBike.updateBikePosition(simTrip.getTripID());
                thread.sleep(60000);
                if(i%5 == 0){
                    simBike.changeChargeLevel(SimBike.DECREASECHARGE);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * First tries to set user to an already registered user from DB, otherwise it creates a new simulated user.
     */
    private void setUser(){
        if(findResgisterAndAvailableUser()){
        }else{
            addNewUser();
        }
    }

    /**
     * Sets simUser to first available registered user
     * @return true or false
     */
    private boolean findResgisterAndAvailableUser(){
        PreparedStatement statement = null;
        ResultSet res = null;
        try{
            con.connect();
            String sentence = "SELECT users.user_id FROM users WHERE users.user_id NOT IN (SELECT trip.user_id FROM trip WHERE trip.end_time IS NULL)"+
                    "and users.user_id IN (SELECT user_id FROM trip WHERE CURRENT_DATE > ban_until OR ban_until IS NULL);";
            statement = con.getCon().prepareStatement(sentence);
            res = statement.executeQuery();
            if(res.next()){
                simUser = new SimUser(res.getInt("user_id"));
                return true;
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
            con.disconnect();
        }
        return false;
    }

    /**
     * Creates a new User in DB
     */
    private void addNewUser() {
        RandomString randomString = new RandomString(10);
        String randomGenCardInfo = randomString.nextString();

        PreparedStatement statement = null;
        ResultSet res = null;
        try {
            con.connect();
            String sentence = "INSERT INTO users (card_info) VALUES (?)";
            statement = con.getCon().prepareStatement(sentence);
            statement.setString(1, randomGenCardInfo);
            statement.executeUpdate();

            sentence = "SELECT user_id FROM users WHERE card_info = ?";
            statement = con.getCon().prepareStatement(sentence);
            statement.setString(1, randomGenCardInfo);
            res = statement.executeQuery();
            if (res.next()) {
                simUser = new SimUser(res.getInt("user_id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeRes(res);
            con.disconnect();
        }
    }
    
}
