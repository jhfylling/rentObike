package simulation;

import sql.Cleaner;
import sql.DBconnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The DockThread class creates a thread that simulates docking station power usage updates.
 */

public class DockThread implements Runnable {

    private Thread thread;

    private final DBconnection con = new DBconnection();
    private final Cleaner cleaner = new Cleaner();

    private final int dockID;
    private int powerUsage = 0;

    /**
     *
     * @param dockID
     */
    public DockThread(int dockID) {
        this.dockID = dockID;
        setPowerUsage();
    }

    /**
     * Sets a local power usage variable to reduce db queries.
     */
    private void setPowerUsage(){
        PreparedStatement statement = null;
        ResultSet res = null;
        try{
            con.connect();
            String sentence = "SELECT power_usage FROM dock WHERE dock_id = ?";
            statement = con.getCon().prepareStatement(sentence);
            statement.setInt(1,dockID);
            res = statement.executeQuery();
            if(res.next()) powerUsage = res.getInt("power_usage");

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            cleaner.closeStatement(statement);
            cleaner.closeRes(res);
            con.disconnect();
        }
    }

    /**
     * Creates thread and starts simulation
     */
    public void start(){
        if(thread == null){
            thread = new Thread(this,"dockThread");
            thread.start();
        }
    }

    /**
     *Contains the simulation loop
     * Sends docking station power usage updates to db
     */
    public void run(){
        while(SimulationRunner.continueSimulation){
            try{
                updatePowerUsage();
                thread.sleep(60000);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Updates local power usage variable and DB
     */
    private void updatePowerUsage(){
        int bikesAtStation = 10;
        powerUsage += bikesAtStation;
        PreparedStatement statement = null;
        try{
            con.connect();
            String sentence = "UPDATE dock SET power_usage = ?";
            statement = con.getCon().prepareStatement(sentence);
            statement.setInt(1, powerUsage);
            statement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        finally {
            Cleaner.closeStatement(statement);
            con.disconnect();
        }
    }
}
