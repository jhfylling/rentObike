package sql;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The DBconnection class is used to get a connection to the database.
 */

public class DBconnection {
    Connection con;

    /**
     * Connect to the database
     * @return true if the connection was a success and false if something went wrong
     */
    public boolean connect(){

        String filename = "/config/db.properties";
        try (
                InputStream inputStream = getClass().getResourceAsStream(filename);
                InputStreamReader inputReader = new InputStreamReader(inputStream);
                BufferedReader buffRead =  new BufferedReader(inputReader);
                ){
            String url = buffRead.readLine();
            String account = buffRead.readLine();
            String pw = buffRead.readLine();

            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, account, pw);

            return true;
        } catch (SQLException se) {
            System.out.println("Could not connect to database: "+se);
            return false;
        }catch(ClassNotFoundException cnfe){
            System.out.println("Could not load driver: "+cnfe);
            return false;
        } catch (FileNotFoundException fnfe) {
            System.out.println("Could not find existing file" + fnfe);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        return false;
    }

    /**
     * @return a connection to the database
     */
    public  Connection getCon() {
        return con;
    }

    /**
     * @return true if the disconnection was a success and false if something went wrong
     */
    public boolean disconnect(){
        try{
            con.close();
            return true;
        }catch(SQLException e){
            System.out.println("Could not disconnect: "+e);
            return false;
        }
    }
}
