package sql;

import java.sql.*;

/**
 * Cleaner class *** mostly taken from else lervik "MittBiblotek".<br>
 * Used by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * Course: Computer engineering.<br>
 * Class: System development.<br>
 *<br>
 * The Cleaner class is used as a helping class for communication with the database.
 */

    public class Cleaner {
        /**
         * Close the result set
         * @param res the result set
         */
        public static void closeRes(ResultSet res) {
            try {
                if (res != null) {
                    res.close();
                }
            }
            catch (SQLException e) {
                errorMessage(e, "closeResSet()");
            }
        }

        /**
         * Close the SQL statement
         * @param stm SQL statement
         */
        public static void closeStatement(Statement stm) {
                try {
                    if (stm != null) {
                        stm.close();
                    }
                }
                catch (SQLException e) {
                    errorMessage(e, "closeStatement()");
                }
            }

        /**
         * A method to set the autocommit on and off
         * @param con Connection to the database
         */
        public static void settAutoCommit(Connection con) {
                try {
                    if (con != null && !con.getAutoCommit()) {
                        con.setAutoCommit(true);
                    }
                } catch (SQLException e) {
                    errorMessage(e, "setAutoCommit()");
                }
            }

        /**
         * Close the connection to the database
         * @param forbindelse Connection to the database
         */
        public static void closeCon(Connection forbindelse) {
                try {
                    if (forbindelse != null) {
                        forbindelse.close();
                    }
                } catch (SQLException e) {
                    errorMessage(e, "lukkForbindelse()");
                }
            }

        /**
         * A error message method you can use where something can be wrong, mostly used in a catch statement
         * @param e the exception e you want to catch
         * @param melding the error message you want to display
         */
        public static void errorMessage(Exception e, String melding) {
                System.err.println("errorMessage: " + melding + ". ***");
                e.printStackTrace(System.err);
            }
        }

