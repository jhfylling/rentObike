package JUnit;

import dto.Bike;
import org.junit.*;
import org.junit.runners.MethodSorters;
import dto.DockingStation;
import dao.DockingStationDAO;
import sql.Cleaner;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DockingStationDAOTest extends TestRules {
    private DockingStationDAO instance;
    private Statement stmt;

    @Before
    public void setUp() {
        instance = new DockingStationDAO(getCon());
        String sentence1 = "DELETE FROM dock";
        String sentence2 = "ALTER TABLE dock AUTO_INCREMENT = 1";
        String sentence3 = "DELETE FROM type";
        String sentence4 = "ALTER TABLE type AUTO_INCREMENT = 1";
        String sentence5 = "DELETE FROM bike";
        String sentence6 = "ALTER TABLE bike AUTO_INCREMENT = 1";
        try{
            stmt = getCon().createStatement();
            stmt.executeUpdate(sentence1);
            stmt.executeUpdate(sentence2);
            stmt.executeUpdate(sentence3);
            stmt.executeUpdate(sentence4);
            stmt.executeUpdate(sentence5);
            stmt.executeUpdate(sentence6);
        }catch(SQLException e){
            e.printStackTrace();
        }
        finally {
            Cleaner.closeStatement(stmt);
        }

        String sentence7 = "INSERT INTO type(type_id, description) VALUES\n" +
                "  (DEFAULT ,\"Gentlemen\"),\n" +
                "  (DEFAULT ,\"Ladies\"),\n" +
                "  (DEFAULT ,\"Unisex\"),\n" +
                "  (DEFAULT ,\"Small\"),\n" +
                "  (DEFAULT ,\"Large\");";


        String sentence8 = "INSERT INTO dock(dock_id, dock_name, power_usage, max_bikes, latitude, longitude) VALUES\n" +

                "  (DEFAULT, 'Kalvskinnet', 100,20, 63.429254, 10.387912),\n" +
                "  (DEFAULT, 'Leuthenhaven', 100,20, 63.430054, 10.391358),\n" +
                "  (DEFAULT, 'Gloshaugen', 100,20, 63.420796, 10.404359),\n" +
                "  (DEFAULT, 'St Olav', 100,20, 63.422130, 10.393562),\n" +
                "  (DEFAULT, 'Solsiden', 100,20, 63.433931, 10.412754)";

        String sentence9 = "INSERT INTO bike (bike_id, make, price, mileage, charge_level, date_bought, type_id, dock_id) VALUES\n" +
                "  (DEFAULT,\"DBS\",500.0,110,85,('2018-03-21'),1,1),\n" +
                "  (DEFAULT,\"DBS\",500.0,140,95,('2018-03-21'),2,2),\n" +
                "  (DEFAULT,\"DBS\",750.40,70,80,('2018-03-21'),3,3),\n" +
                "  (DEFAULT,\"DBS\",800.50,50,100,('2018-03-21'),4,4),\n" +
                "  (DEFAULT,\"DBS\",999.99,150,70,('2018-03-21'),5,5);";
        try{
            stmt = getCon().createStatement();
            stmt.executeUpdate(sentence7);
            stmt.executeUpdate(sentence8);
            stmt.executeUpdate(sentence9);
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() {
        instance = null;

    }


    @Test
    public void testGetDockingStation() {
        System.out.println("DockingStationDAO: getDockingStation");
        int expResult = 1;
        instance.update(); // updates the dockingstationlist
        int result = instance.getDockingStation(0).getId();
        assertEquals(expResult,result);
    }


    @Test
    public void getDockingStationList() {
        System.out.println("DockingStationDAO: getDockingStationList");
        String expResult = "KalvskinnetLeuthenhavenGloshaugenSt OlavSolsiden";
        String result = "";
        ArrayList<DockingStation> dockingStationlist = instance.getDockingStationList();
        for (DockingStation d: dockingStationlist) {
            result += d.toString();
        }
        assertEquals(expResult,result);
    }

    @Test
    public void testGetDock() {
        System.out.println("DockingStationDAO: getDocK");
        DockingStationDAO dDAO = new DockingStationDAO(getCon());
        Object result = dDAO.getDock(2);
        Object expResult = instance.getDock(2);
        assertEquals(expResult,result);
    }

    @Test
    public void testGetDock2() {
        System.out.println("DockingStationDAO: getDocK ");
        Object result = instance.getDock(-1);
        Object expResult = null;
        assertEquals(expResult,result);
    }

    @Test
    public void testXAddDockingStation() {
        System.out.println("DockingStationDAO: addDockingStation");
        assertTrue(instance.addDockingStation("test", 1, 1,1));

    }

    // testing method that update the list of docking station with information from the database
    @Test
    public void testUpdate() {
        System.out.println("DockingStationDAO: update");
        assertTrue(instance.update());
    }

    @Test
    public void testZDisableDockingStation() {
        System.out.println("DockingStationDAO: disableDockingStation");
        assertTrue(instance.disableDockingStation(5));
    }

    @Test
    public void testEditDockingStation() {
        System.out.println("DockingStationDAO: editDockingStation");
        String dockname = "test";
        double powerU = 100;
        int maxB = 100;
        double lat = 63.02342;
        double longi = 10.123123;
        int id = 4;
        assertTrue(instance.editDckingStation(dockname,powerU,maxB,lat,longi,id));
    }

    @Test
    public void testGetBiksAtStaion() {
        System.out.println("DockingStationDAO: getBikesAtStation");
        String result = "";
        ArrayList<Bike> bikes = instance.getBikesAtStation(1);
        String expResult= "1 DBS 500 110 85 2018-03-21";
        for(Bike b: bikes){
            result += b.toString();
        }

        assertEquals(expResult,result);
    }

    public static void main(String args[]) {
        org.junit.runner.JUnitCore.main(DockingStationDAOTest.class.getName());
    }

}