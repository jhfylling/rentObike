package JUnit;

import dao.AdminDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import dto.User;
import dao.UserDAO;
import sql.Cleaner;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class UserDAOTest extends TestRules {
    private Connection con;
    private UserDAO instance;
    private Statement statement;


    @Before
    public void setUp() {
        instance = new UserDAO(getCon());

        String sentence1 = "DELETE FROM users";
        String sentence2 = "ALTER TABLE users AUTO_INCREMENT = 1";

        String sentence3 = "INSERT INTO users(user_id, card_info, ban_until, comments) VALUES\n" +
                "  (DEFAULT ,'4512 7896 7896 1478','2018-01-01',''),\n" +
                "  (DEFAULT ,'4287 8265 9878 1320','2018-01-01',''),\n" +
                "  (DEFAULT ,'4578 6598 3265 1245',('2018-05-21'),\"vandalism on docking station\"),\n" +
                "  (DEFAULT ,'9685 6352 8574 5241','2018-01-01',''),\n" +
                "  (DEFAULT ,'1425 3696 8525 5847',('2018-03-05'),\"tried to steal a bicylce\");";

        try {

        statement = getCon().createStatement();
        statement.executeUpdate(sentence1);
        statement.executeUpdate(sentence2);
        statement.executeUpdate(sentence3);

        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            Cleaner.closeStatement(statement);
        }
    }

    @After
    public void tearDown() {
        instance = null;
    }

    @Test
    public void testGetAllUsers() throws Exception {
        System.out.println("UserDAOTest: getAllUsers");
        String expResult =
                        "\nid: 1 card info: 4512 7896 7896 1478 banned until: 2018 jan 01 reason: " +
                        "\nid: 2 card info: 4287 8265 9878 1320 banned until: 2018 jan 01 reason: " +
                        "\nid: 3 card info: 4578 6598 3265 1245 banned until: 2018 mai 21 reason: vandalism on docking station" +
                        "\nid: 4 card info: 9685 6352 8574 5241 banned until: 2018 jan 01 reason: " +
                        "\nid: 5 card info: 1425 3696 8525 5847 banned until: 2018 mar 05 reason: tried to steal a bicylce";
        String result = "";

        ArrayList<User> users = instance.getAllUsers();
        for (User u: users) {
            result += u.toString();
        }

        assertEquals(expResult, result);
    }

    @Test
    public void testBan() {
        System.out.println("Bike: ban");
        java.sql.Date banDate = Date.valueOf("2029-01-01");
        boolean expResult = true;
        boolean result = instance.ban(banDate, "testBan", 1);
        assertEquals(expResult, result);
    }

    @Test
    public void testUnBan() {
        System.out.println("Bike: ban");

        boolean expResult = true;
        boolean result = instance.unban(1);
        assertEquals(expResult, result);
    }



    public static void main(String args[]) {
        org.junit.runner.JUnitCore.main(UserDAOTest.class.getName());
    }


}