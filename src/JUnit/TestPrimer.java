package JUnit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 *  A primer used by JunitMain, makes it possible to test all classes at once.
 */


@RunWith(Suite.class)
@Suite.SuiteClasses({

        StatisticsDAOTest.class,
        DockingStationDAOTest.class,
        UserDAOTest.class,
        BikeDAOTest.class,
        AdminDAOTest.class

})

public class TestPrimer {
}
