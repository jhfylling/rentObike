package JUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;


import java.sql.Connection;

/**
 * A ruleset that is equal for every JUnit test, every JUnit test class should Extend TestRules.
 *
 */

public class TestRules {

    private static DBconnectionJUnit connector = new DBconnectionJUnit();
    private static Connection con;

    public TestRules() {
    }

    public Connection getCon() {
        return con;
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        if(connector.connect()){
           con = connector.getCon();
            System.out.println("Connected to DB");
        }
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
            connector.disconnect();
            System.out.println("Disconnected from DB");
    }

    @Rule
    public TestRule listen = new TestWatcher() {
        @Override
        public void failed(Throwable t, Description description) {

            System.out.println(description.getMethodName() + " Failed!");

        }

        @Override
        public void succeeded(Description description) {

            System.out.println(description.getMethodName() + " Passed!");

        }
    };

}
