package JUnit;

import dao.BikeDAO;
import org.junit.*;
import org.junit.runners.MethodSorters;
import dto.Admin;
import dao.AdminDAO;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import org.junit.FixMethodOrder;
import sql.Cleaner;

import static org.junit.Assert.*;

/**
 * Fixed method order for testing getAll function without the delete and add funkctions interfering with it
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING) // Makes the tests in order of ascending lexicographic order
public class AdminDAOTest extends TestRules {
    private AdminDAO instance;
    private Statement statement;



    @Before
    public void setUp() {
        instance = new AdminDAO(getCon());

        String sentence1 = "INSERT INTO admin (admin_id, email, salt, hashed_pw, admin_name) VALUES\n" +
                "  (DEFAULT,\"kristian_kampenhoy@mail.com\",\"123456\",\"1q2w3e4r5t6y\",\"kristian\"),\n" +
                "  (DEFAULT,\"johan_fylling@mail.com\",\"101010\",\"10a10e10g\",\"johan\"),\n" +
                "  (DEFAULT,\"ben_oscar@mail.com\",\"asddfg\",\"Ben\",\"asd1dfg2g5h\"),\n" +
                "  (DEFAULT,\"test\",\"b6b25597c8b2fa330eb6d67fbda5e221\"," +
                "\"c68ee95de77ac658f0226feb264b62af80708cbdf1822c2e022493b43b48c2c9535afdcc085c63ba029a17e6a809334fca39c2369ec4a1c25d5d72aed00f316\",\"test\");\n";
        try {
            statement = getCon().createStatement();
            statement.executeUpdate(sentence1);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            Cleaner.closeStatement(statement);
        }
    }

    @After
    public void tearDown() {
        instance = null;
        String sentence = "DELETE FROM admin";
        String sentence2 = "ALTER TABLE admin AUTO_INCREMENT = 1";
        try{
            statement = getCon().createStatement();
            statement.executeUpdate(sentence);
            statement.executeUpdate(sentence2);
        }catch(SQLException e){
            e.printStackTrace();
        }
        finally {
            Cleaner.closeStatement(statement);
        }
    }


    @Test
    public void testGetAllAdmin() throws Exception {
        System.out.println("AdminDAO: getAllAdmin");
        Admin a1 = new Admin(1,"kristian_kampenhoy@mail.com","kristian",getCon());
        Admin a2 = new Admin(2, "johan_fylling@mail.com","johan", getCon());
        Admin a3 = new Admin(3, "ben_oscar@mail.com","Ben", getCon());
        Admin a4 = new Admin(4,"test","test",getCon());
        ArrayList<Admin> testAdmins = new ArrayList();
        testAdmins.add(a1);
        testAdmins.add(a2);
        testAdmins.add(a3);
        testAdmins.add(a4);
        String expResult = "";
        for (Admin a: testAdmins) { expResult += a.toString(); }
        ArrayList<Admin> resultList = instance.getAllAdmin();
        String result = "";
        for (Admin a: resultList) { result += a.toString(); }
        assertEquals(expResult,result);
    }



    @Test
    public void testRegisterNewAdmin() {
        System.out.println("AdminDAO: registerNewAdmin");
        ArrayList<String> regAdm = new ArrayList();
        regAdm.add("test@test.no");
        regAdm.add("test");
        assertTrue(instance.registerNewAdmin(regAdm));
    }

    @Test
    public void testzeditadmin() throws Exception {
        System.out.println("AdminDAO: editadmin");
        assertTrue(instance.editadmin("test@edit.no", "edit",4));
    }


    @Test
    public void testzdisableAdmin(){
        System.out.println("AdminDAO: disable admin");
        assertTrue(instance.disableAdmin(3));
    }

    @Test
    public void testzdisableAdmin2() {
        System.out.println("AdminDAO: disable admin");
        int index = 0;
        assertTrue(instance.disableAdmin(index));
    }

    @Test
    public void testGetpasswordFromDB() {
        System.out.println("AdminDAO: getPasswordFromDB");
        String email = "test";
        String result = "1000:b6b25597c8b2fa330eb6d67fbda5e221:c68ee95de77ac658f0226feb264b62af80708cbdf1822c2e022493b43b48c2c9535afdcc085c63ba029a17e6a809334fca39c2369ec4a1c25d5d72aed00f316";
        String expResult = instance.getPasswordFromDB(email);
        assertEquals(result,expResult);

    }

    @Test
    public void testChangePassword(){
        System.out.println("AdminDAO: changePassword");
        String email = "kristian_kampenhoy@mail.com";
        String salt = "123456";
        String hash = "1q2w3e4r5t6y";
        assertTrue(instance.changePassword(email,salt,hash));
    }

    public static void main(String args[]) {
            org.junit.runner.JUnitCore.main(AdminDAOTest.class.getName());


    }
}

