package JUnit;

import org.junit.*;
import dao.StatisticsDAO;
import sql.Cleaner;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.*;


public class StatisticsDAOTest extends TestRules {
    private StatisticsDAO instance;
    private Statement stmt;

    @Before
    public void setUp() {
        instance = new StatisticsDAO(getCon());

        String sentence = "DELETE FROM transaction";
        String sentence2 = "ALTER TABLE transaction AUTO_INCREMENT = 1";

        String sentence3 = "DELETE FROM repairs";
        String sentence4 = "ALTER TABLE repairs AUTO_INCREMENT = 1";

        String sentence5 = "DELETE FROM dock";
        String sentence6 = "ALTER TABLE dock AUTO_INCREMENT = 1";

        String sentence7 = "DELETE FROM users";
        String sentence8 = "ALTER TABLE users AUTO_INCREMENT = 1";

        String sentence9 = "DELETE FROM bike";
        String sentence10 = "ALTER TABLE bike AUTO_INCREMENT = 1";

        String sentence11 = "DELETE FROM trip";
        String sentence12 = "ALTER TABLE trip AUTO_INCREMENT = 1";

        String sentence13 = "DELETE FROM type";
        String sentence14 = "ALTER TABLE type AUTO_INCREMENT = 1";

        String sentence21 = "INSERT INTO transaction(transaction_id, amount, type, dock_id, trip_id) VALUES\n" +
                "  (DEFAULT ,100.0,'VISA',1,1),\n" +
                "  (DEFAULT ,100.0,'MASTERCARD',2,2),\n" +
                "  (DEFAULT ,100.0,'MASTERCARD',3,3),\n" +
                "  (DEFAULT ,100.0,'VISA',4,4),\n" +
                "  (DEFAULT ,100.0,'CASH',5,5);";

        String sentence20 = "INSERT INTO repairs(repair_id, date_sent, date_recieved, price, request_description, repair_description, bike_id) VALUES\n" +
                "  (DEFAULT ,('2018-03-15'),('2018-03-25'),2500,\"new battery\",\"new battery\",1),\n" +
                "  (DEFAULT ,('2018-03-15'),('2018-03-25'),2500,\"rear wheel punctured\",\"new rear wheel\",2),\n" +
                "  (DEFAULT ,('2018-03-15'),('2018-03-25'),2500,\"worn pedals\",\" new handels\",3),\n" +
                "  (DEFAULT ,('2018-03-15'),('2018-03-25'),2500,\"worn handels\",\"new pedals\",4),\n" +
                "  (DEFAULT ,('2018-02-15'),('2018-03-25'),2500,\"malfunction\",\"new bike\",5),\n" +
                "  (DEFAULT ,(\"2018-04-14\"),(\"2018-04-24\"),1500,'new wheel','new wheel inn place',1);";

        String sentence19 = "INSERT INTO trip(trip_id, distance, from_dock, to_dock, start_time, end_time, user_id, bike_id) VALUES\n" +
                "  (DEFAULT ,100,1,4,('2018-03-10 18:45:15'),('2018-03-10 19:30:00'),1,1),\n" +
                "  (DEFAULT ,100,2,1,('2018-03-30 10:00:01'),('2018-03-30 12:00:45'),2,2),\n" +
                "  (DEFAULT ,100,4,4,('2018-03-25 20:30:16'),('2018-03-25 22:45:36'),3,3),\n" +
                "  (DEFAULT ,100,5,1,('2018-03-20 14:55:00'),('2018-03-20 16:15:00'),4,4),\n" +
                "  (DEFAULT ,100,3,2,('2018-03-05 12:00:00'),('2018-03-05 15:00:00'),5,5);";

        String sentence15 = "INSERT INTO dock(dock_id, dock_name, power_usage, max_bikes, latitude, longitude) VALUES\n" +
                "  (DEFAULT, 'Kalvskinnet', 100,20, 63.429254, 10.387912),\n" +
                "  (DEFAULT, 'Leuthenhaven', 100,20, 63.430054, 10.391358),\n" +
                "  (DEFAULT, 'Gloshaugen', 100,20, 63.420796, 10.404359),\n" +
                "  (DEFAULT, 'St Olav', 100,20, 63.422130, 10.393562),\n" +
                "  (DEFAULT, 'Solsiden', 100,20, 63.433931, 10.412754);";

        String sentence18 = "INSERT INTO users(user_id, card_info, ban_until, comments) VALUES\n" +
                "  (DEFAULT ,'4512 7896 7896 1478','2018-01-01',''),\n" +
                "  (DEFAULT ,'4287 8265 9878 1320','2018-01-01',''),\n" +
                "  (DEFAULT ,'4578 6598 3265 1245',('2018-05-21'),\"vandalism on docking station\"),\n" +
                "  (DEFAULT ,'9685 6352 8574 5241','2018-01-01',''),\n" +
                "  (DEFAULT ,'1425 3696 8525 5847',('2018-03-05'),\"tried to steal a bicylce\");";

        String sentence17 = "INSERT INTO bike (bike_id, make, price, mileage, charge_level, date_bought, type_id, dock_id) VALUES\n" +
                "  (DEFAULT,\"DBS\",500.0,110,85,('2018-03-21'),1,1),\n" +
                "  (DEFAULT,\"DBS\",500.0,140,95,('2018-03-21'),2,2),\n" +
                "  (DEFAULT,\"DBS\",750.40,70,80,('2018-03-21'),3,3),\n" +
                "  (DEFAULT,\"DBS\",800.50,50,100,('2018-03-21'),4,4),\n" +
                "  (DEFAULT,\"DBS\",999.99,150,70,('2018-03-21'),5,5);";

        String sentence16 = "INSERT INTO type(type_id, description) VALUES\n" +
                "  (DEFAULT ,\"Gentlemen\"),\n" +
                "  (DEFAULT ,\"Ladies\"),\n" +
                "  (DEFAULT ,\"Unisex\"),\n" +
                "  (DEFAULT ,\"Small\"),\n" +
                "  (DEFAULT ,\"Large\");";

        try{
            stmt = getCon().createStatement();
            stmt.executeUpdate(sentence);
            stmt.executeUpdate(sentence2);
            stmt.executeUpdate(sentence3);
            stmt.executeUpdate(sentence4);
            stmt.executeUpdate(sentence5);
            stmt.executeUpdate(sentence6);
            stmt.executeUpdate(sentence7);
            stmt.executeUpdate(sentence8);
            stmt.executeUpdate(sentence9);
            stmt.executeUpdate(sentence10);
            stmt.executeUpdate(sentence11);
            stmt.executeUpdate(sentence12);
            stmt.executeUpdate(sentence13);
            stmt.executeUpdate(sentence14);
            stmt.executeUpdate(sentence15);
            stmt.executeUpdate(sentence16);
            stmt.executeUpdate(sentence17);
            stmt.executeUpdate(sentence18);
            stmt.executeUpdate(sentence19);
            stmt.executeUpdate(sentence20);
            stmt.executeUpdate(sentence21);

        }catch(SQLException e){
            e.printStackTrace();
        }
        finally {
            Cleaner.closeStatement(stmt);
        }
    }

    @After
    public void tearDown() {
        instance = null;

    }

    @Test
    public void testGetRepairedBikes () {

        Date start = Date.valueOf("2018-01-01");
        Date end = Date.valueOf("2018-12-31");

        int expResult = 6;
        int result = instance.getRepairedBikes(start, end);
        assertEquals(expResult, result);
    }

    @Test
    public void testTripsInPeriode () {

        Date start = Date.valueOf("2018-01-01");
        Date end = Date.valueOf("2018-12-31");

        int expResult = 5;
        int result = instance.getTripsInPeriode(start, end);
        assertEquals(expResult, result);
    }

    @Test
    public void testTripsInPerHour () {

        int expResult = 0;
        int result = instance.getTripsPerHour("2018-01-01", "2018-12-31");
        assertEquals(expResult, result);
    }

    @Test
    public void testGetUserForMonth () {

        Date start = Date.valueOf("2018-01-01");
        Date end = Date.valueOf("2018-12-31");

        int expResult = 5;
        int result = instance.getUserForMonth(start, end);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTotaltEarned() {

        double expResult = 500.0;
        double result = instance.getTotaltEarned();
        assertEquals(expResult, result, 0.1);
    }

    @Test
    public void testGetNumberOfUsers() {

        int expResult = 5;
        int result = instance.getNumberOfUsers();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetNumberOfTrips() {

        int expResult = 5;
        int result = instance.getNumberOfTrips();
        assertEquals(expResult, result);
    }

    @Test
    public void testbetweebSixAndTenTrups() {

        int expResult = 0;
        int result = instance.betweenSixAndTenTrips();
        assertEquals(expResult, result);
    }

    @Test
    public void testbetweenTwoAndFiveTrips() {

        int expResult = 0;
        int result = instance.betweenTwoAndFiveTrips();
        assertEquals(expResult, result);
    }

    @Test
    public void testonlyBeenOnOneTrip() {

        int expResult = 5;
        int result = instance.onlyBeenOnOneTrip();
        assertEquals(expResult, result);
    }

    @Test
    public void testgetTotalRepairCosts() {

        double expResult = 14000.0;
        double result = instance.getTotalRepairCosts();
        assertEquals(expResult, result,0.1);
    }

    @Test
    public void testgetPowerUsage() {
        double expResult = 500;
        double result = instance.getPowerUsage();
        assertEquals(expResult, result,0.1);
    }

    public static void main(String args[]) {
        org.junit.runner.JUnitCore.main(StatisticsDAOTest.class.getName());
    }
}