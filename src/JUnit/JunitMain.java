package JUnit;

import org.junit.runner.JUnitCore;
import sql.DBconnection;

import java.sql.Connection;

/**
 * Runs the TestPrimer Class, such that all JUnit tests are run at once
 * When running, DBconnections filename must be changed to /config/dbtest.properties for it to run against the test database.
 *
 */

public class JunitMain {

    public static void main(String[] args) {

        // Runs all tests at once

        JUnitCore.runClasses(TestPrimer.class);
    }

}