package JUnit;

import org.junit.*;
import org.junit.runners.MethodSorters;
import dto.Bike;
import dao.BikeDAO;
import dto.BikeType;
import sql.Cleaner;

import java.sql.*;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.Assert.*;

/**
 * Fixed method order for running the delete method in the end
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BikeDAOTest extends TestRules {
    private BikeDAO instance;
    private Statement stmt;
    


    @Before
    public void setUp() {
        instance = new BikeDAO(getCon());

        String sentence1 = "DELETE FROM type";
        String sentence2 = "ALTER TABLE type AUTO_INCREMENT = 1";

        String sentence3 = "DELETE FROM repairs";
        String sentence4 = "ALTER TABLE repairs AUTO_INCREMENT = 1";

        String sentence5 = "DELETE FROM trip";
        String sentence6 = "ALTER TABLE trip AUTO_INCREMENT = 1";

        String sentence7 = "DELETE FROM bike";
        String sentence8 = "ALTER TABLE bike AUTO_INCREMENT = 1";

        String sentence9 = "DELETE FROM users";
        String sentence10 = "ALTER TABLE users AUTO_INCREMENT = 1";

        try{
            stmt = getCon().createStatement();
            stmt.executeUpdate(sentence1);
            stmt.executeUpdate(sentence2);
            stmt.executeUpdate(sentence3);
            stmt.executeUpdate(sentence4);
            stmt.executeUpdate(sentence5);
            stmt.executeUpdate(sentence6);
            stmt.executeUpdate(sentence7);
            stmt.executeUpdate(sentence8);
            stmt.executeUpdate(sentence9);
            stmt.executeUpdate(sentence10);
        }catch(SQLException e){
            e.printStackTrace();
        }
        finally {
            Cleaner.closeStatement(stmt);
        }

         sentence1 = "INSERT INTO dock(dock_id, dock_name, power_usage, max_bikes, latitude, longitude) VALUES\n" +
                "  (DEFAULT, 'Kalvskinnet', 100,20, 63.429254, 10.387912),\n" +
                "  (DEFAULT, 'Leuthenhaven', 100,20, 63.430054, 10.391358),\n" +
                "  (DEFAULT, 'Gloshaugen', 100,20, 63.420796, 10.404359),\n" +
                "  (DEFAULT, 'St Olav', 100,20, 63.422130, 10.393562),\n" +
                "  (DEFAULT, 'Solsiden', 100,20, 63.433931, 10.412754)";

        sentence2 = "INSERT INTO type(type_id, description) VALUES\n" +
                "  (DEFAULT ,\"Gentlemen\"),\n" +
                "  (DEFAULT ,\"Ladies\"),\n" +
                "  (DEFAULT ,\"Unisex\"),\n" +
                "  (DEFAULT ,\"Small\"),\n" +
                "  (DEFAULT ,\"Large\");";

         sentence3 = "INSERT INTO bike (bike_id, make, price, mileage, charge_level, date_bought, type_id, dock_id) VALUES\n" +
                "  (DEFAULT,\"DBS\",500.0,110,85,('2018-03-21'),1,1),\n" +
                "  (DEFAULT,\"DBS\",500.0,140,95,('2018-03-21'),2,2),\n" +
                "  (DEFAULT,\"DBS\",750.40,70,80,('2018-03-21'),3,3),\n" +
                "  (DEFAULT,\"DBS\",800.50,50,100,('2018-03-21'),4,4),\n" +
                "  (DEFAULT,\"DBS\",999.99,150,70,('2018-03-21'),5,5);";

         sentence4 = "INSERT INTO repairs(repair_id, date_sent, date_recieved, price, request_description, repair_description, bike_id) VALUES\n" +
                "  (DEFAULT ,('2018-03-15'),('2018-03-25'),2500,\"new battery\",\"new battery\",1),\n" +
                "  (DEFAULT ,('2018-03-15'),('2018-03-25'),2500,\"rear wheel punctured\",\"new rear wheel\",2),\n" +
                "  (DEFAULT ,('2018-03-15'),('2018-03-25'),2500,\"worn pedals\",\" new handels\",3),\n" +
                "  (DEFAULT ,('2018-03-15'),('2018-03-25'),2500,\"worn handels\",\"new pedals\",4),\n" +
                "  (DEFAULT ,('2018-02-15'),('2018-03-25'),2500,\"malfunction\",\"new bike\",5),\n" +
                "  (DEFAULT ,(\"2018-04-14\"),(\"2018-04-24\"),1500,'new wheel','new wheel inn place',1);";

         sentence5 = "INSERT INTO users(user_id, card_info, ban_until, comments) VALUES\n" +
                "  (DEFAULT ,'4512 7896 7896 1478','2018-01-01',''),\n" +
                "  (DEFAULT ,'4287 8265 9878 1320','2018-01-01',''),\n" +
                "  (DEFAULT ,'4578 6598 3265 1245',('2018-05-21'),\"vandalism on docking station\"),\n" +
                "  (DEFAULT ,'9685 6352 8574 5241','2018-01-01',''),\n" +
                "  (DEFAULT ,'1425 3696 8525 5847',('2018-03-05'),\"tried to steal a bicylce\");";

         sentence6 = "INSERT INTO trip(trip_id, distance, from_dock, to_dock, start_time, end_time, user_id, bike_id) VALUES\n" +
                "  (DEFAULT ,100,1,4,('2018-03-10 18:45:15'),('2018-03-10 19:30:00'),1,1),\n" +
                "  (DEFAULT ,100,2,1,('2018-03-30 10:00:01'),('2018-03-30 12:00:45'),2,2),\n" +
                "  (DEFAULT ,100,4,4,('2018-03-25 20:30:16'),('2018-03-25 22:45:36'),3,3),\n" +
                "  (DEFAULT ,100,5,1,('2018-03-20 14:55:00'),('2018-03-20 16:15:00'),4,4),\n" +
                "  (DEFAULT ,100,3,2,('2018-03-05 12:00:00'),('2018-03-05 15:00:00'),5,5);";



        try {
            stmt = getCon().createStatement();
            stmt.executeUpdate(sentence1);
            stmt.executeUpdate(sentence2);
            stmt.executeUpdate(sentence3);
            stmt.executeUpdate(sentence4);
            stmt.executeUpdate(sentence5);
            stmt.executeUpdate(sentence6);
            }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            Cleaner.closeStatement(stmt);
        }
    }

    @After
    public void tearDown() {
        instance = null;
    }

    @Test
    public void testGetTypes() {
        System.out.println("BikeDAO: getTypes");
        String expResult = "Gentlemen Ladies Unisex Small Large ";
        ArrayList<BikeType> types = instance.getTypes();
        String result = "";
        for (BikeType t: types) {
            result += t.toString() + " ";
        }
        assertEquals(expResult, result);
    }

    @Test
    public void testAddBike() {
        System.out.println("BikeDAO: addBike");
        assertTrue(instance.addBike("testAddBike",1,1,1,1,1 ));
    }


    @Test
    public void testDisableBike() {
        System.out.println("BikeDAO: disableBike");
        assertTrue(instance.disableBike(1));
    }

    @Test
    public void testDisableType() {
        System.out.println("BikeDAO: disableType");
        assertTrue(instance.disableType(1));
    }
    @Test
    public void testDisableType2() {
        System.out.println("BikeDAO: disableType");
        assertFalse(instance.disableType(-1));
    }

    @Test
    public void testDisableBike3() {
        System.out.println("BikeDAO: disableBike3");
        assertFalse(instance.disableBike(-1));
    }


    @Test
    public void testGetBikes() {
        System.out.println("BikeDAO: getNumberOfBikesAtStation");
        String expResult = "1 2 3 4 5 ";
        ArrayList<Bike> bikes = instance.getBikes();
        String result = "";

        for (int i = 0; i < bikes.size(); i++) {
            result += bikes.get(i).getBike_id()+ " ";
        }
        assertEquals(expResult, result);
    }


    @Test
    public void testGetBike() {
        System.out.println("BikeDAO: getBike");
        int expResult = 1;
        int result = instance.getBike(1).getBike_id();
        assertEquals(expResult, result);
    }


    @Test
    public void testGetBike2() {
        System.out.println("BikeDAO: getBikeNegative");
        instance.getBike(-1);
        Object result = instance.getBike(-1);
        Object expResult = null;
        assertEquals(result, expResult);

    }

    @Test
    public void testEditBike() {
        System.out.println("BikeDAO: editBike");

        assertTrue(instance.editBike(1, "testus",1,1.0, 1 , 1));
    }

    @Test
    public void testGetRepairHistory() {
        System.out.println("BikeDAO: getRepairHistory");
        String expResult = "Date sent: 2018-03-15" + ";Date received: 2018-03-25" + ";Request: worn pedals" + ";Work done:  new handels";
        ArrayList<String> history = instance.getRepairHistory(3);
        String result = "";
        for (int i = 0; i < history.size(); i++) {
            result += history.get(i);
        }
        assertEquals(expResult, result);
    }

    @Test
    public void testNumberOfTrips() {
        System.out.println("BikeDAO: numberOfTrips");
        int expResult = 1;
        int result = instance.numberOfTrips(1);
        assertEquals(expResult, result);
    }

    @Test
    public void testNumberOfKms() {
        System.out.println("BikeDAO: numberOfKms");
        int expResult = 100;
        int result = instance.numberOfKms(1);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetDock() {
        System.out.println("BikeDAO: getDock");
        int expResult = 1;
        int result = instance.getDock(1);
        assertEquals(expResult,result);

    }

    @Test
    public void testRegisterNewRepair() {
        System.out.println("BikeDAO: registerNewRepair");
        Calendar date = new GregorianCalendar();
        Date date2 = new Date(date.getTimeInMillis());
        String description = "test";
        assertTrue(instance.registerNewRepair(date2,description,1));

    }

    @Test
    public void testRegisterFinishedRepair() {
        System.out.println("bikeDAO: registerFinishedRepair");
        Calendar date = new GregorianCalendar();
        Date date2 = new Date(date.getTimeInMillis());
        String description = "test finished";
        assertTrue(instance.registerFinishedRepair(date2,description,1,100));
    }

    @Test
    public void testSaveNewType() {
        System.out.println("bikeDAO: saveNewType");
        int expResult = 6;
        int result = instance.saveNewType("test");
        assertEquals(expResult,result);
    }

    @Test
    public void testGetUserHistory() {
        System.out.println("bikeDAO: getUserHistory");
        String expResult = "User ID 1<br> Trip started: 2018-03-10 18:45:15.0<br>Trips Ended: 2018-03-10 19:30:00.0";
        String result = "";
        ArrayList<String> userH = instance.getUserHistory(1);
        for(int i = 0; i < userH.size(); i++){
            result += userH.get(i);
        }
        assertEquals(expResult,result);
    }



    public static void main(String args[]) {
        org.junit.runner.JUnitCore.main(BikeDAOTest.class.getName());
    }

}