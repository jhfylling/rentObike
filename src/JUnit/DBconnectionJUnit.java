package JUnit;



import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Same as DBConnection, but made to seperate junit from main database without having to change file.
 */

@SuppressWarnings("ALL")
public class DBconnectionJUnit {
    Connection con;

    public boolean connect(){

        String filename = "/config/dbtest.properties";
        try (
                //FileReader read = new FileReader(filename);
                //BufferedReader buffRead =  new BufferedReader(read);
                InputStream inputStream = getClass().getResourceAsStream(filename);
                InputStreamReader inputReader = new InputStreamReader(inputStream);
                BufferedReader buffRead =  new BufferedReader(inputReader);
        ){
            String url = buffRead.readLine();
            String account = buffRead.readLine();
            String pw = buffRead.readLine();

            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, account, pw);

            return true;
        } catch (SQLException se) {
            System.out.println("Could not connect to test database: "+se);
            return false;
        }catch(ClassNotFoundException cnfe){
            System.out.println("Could not load driver for test database: "+cnfe);
            return false;
        } catch (FileNotFoundException fnfe) {
            System.out.println("Could not find existing file for test database" + fnfe);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        return false;
    }

    public  Connection getCon() {
        return con;
    }

    public  boolean disconnect(){
        try{
            con.close();
            return true;
        }catch(SQLException e){
            System.out.println("Could not disconnect test database: "+e);
            return false;
        }
    }

   /* public static void main(String[] args) {
        DBconnection db = new DBconnection();
        if(db.connect()){
            System.out.println("Ok");
        }else{
            System.out.println("error");
        }
        System.out.println(new File(".").getAbsolutePath());
    } */
}
