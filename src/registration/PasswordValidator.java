package registration;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The PasswordValidator class is used to validate passwords used to access the system.
 */

public class PasswordValidator {

    /**
     * Validates the used password
     * @param unhashedPassword the unhashed password of the user
     * @param storedPassword the password stored in the system
     * @return returnes true if the password match and false if something went wrong or the password did not match
     */
    public boolean validatePassword(String unhashedPassword, String storedPassword){
        String [] parts = storedPassword.split(":");
        if(parts.length != 3) return false;
        int iterations = Integer.parseInt(parts[0]);
        byte [] salt = fromHex(parts[1]);
        byte [] hash = fromHex(parts[2]);

        PBEKeySpec spec = new PBEKeySpec(unhashedPassword.toCharArray(),salt,iterations,hash.length*8);
        try {

            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte [] testHash = skf.generateSecret(spec).getEncoded();

            int diff = hash.length ^ testHash.length;
            for (int i = 0; i < hash.length && i < testHash.length; i++) {
                diff |= hash[i] ^ testHash[i];
            }
            return diff == 0;
        }catch (NoSuchAlgorithmException nsae){
            System.out.println("System needs to download the PBKDF2WithHmacSHA1, maybe update java API?");
            nsae.printStackTrace();
        }catch (InvalidKeySpecException ikse){
            System.out.println("Invalid key");
            ikse.printStackTrace();
        }
        return false;
    }

    /**
     * converts from hex to a table with bytes
     * @param hex the initial hex value
     * @return a table with bytes
     */
    private byte[] fromHex (String hex){
        byte [] bytes = new byte[hex.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte)Integer.parseInt(hex.substring(2*i,2*i+2),16);
        }
        return bytes;
    }
}

