package registration;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The MailSender class is used to send out registration mails to new users of the system.
 */

public class MailSender {

    /**
     * Sends out a registration mail to new users
     * @param email the users e-mail
     * @param randomGenPassword the random generated password
     * @return returns true if the mail is sent and false if something went wrong
     */
    public boolean sendRegistrationEmail(String email, String randomGenPassword, String name){
        final String username = "rentobike.trondheim@gmail.com";
        final String password = "rentobike123";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("rentobike.trondheim@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(email));
            message.setSubject("Registration complete");


            /*
            *We need to add a method to send out autogen password and replace INSERT PASSWORD.
             */
            message.setText(
                    "Hello "+name+"\n\n"+
                    "An administrator account has been registered in your name with this E-mail address\n" +
                    "If you are not associated with Trondheim City Council you can ignore this E-mail.\n\n" +
                    "Your temporary password is: "+randomGenPassword+"\n" +
                    "We recommend you to login and change your password.\n\n" +
                    "Please do not reply to this E-mail.\n\n" +
                    "Regards,\n\n" +
                    "Trondheim City Council\n" +
                    "RentObike\n" +
                    "E-mail: rentobike@gmail.com\n" +
                    "Phone: +47 47 888 514");
            Transport.send(message);

            System.out.println("Done");
            return true;

        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean sendFogottenPasswordMail(String name, String email, String randomGenPassword){
        final String username = "rentobike.trondheim@gmail.com";
        final String password = "rentobike123";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("rentobike.trondheim@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(email));
            message.setSubject("Registration complete");


            /*
             *We need to add a method to send out autogen password and replace INSERT PASSWORD.
             */
            message.setText(
                    "Hello " + name +"\n\n"+
                            "If you are not associated with Trondheim City Council you can ignore this E-mail.\n\n" +
                            "Your temporary password is: "+randomGenPassword+"\n" +
                            "We recommend you to login and change your password.\n\n" +
                            "Please do not reply to this E-mail.\n\n" +
                            "Regards,\n\n" +
                            "Trondheim City Council\n" +
                            "RentObike\n" +
                            "E-mail: rentobike@gmail.com\n" +
                            "Phone: +47 47 888 514");
            Transport.send(message);

            System.out.println("Done");
            return true;

        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return false;
    }

}
