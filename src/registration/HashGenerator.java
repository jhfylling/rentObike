package registration;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The HashGenerator class is used to hash passwords used to access the system.
 */

public class HashGenerator {

    public static final int iterations = 1000;
    private final int keyLength = 64 * 8;

    /**
     * Generate a hashed version of the password
     * @param unhashedPassword the unhashed password of the user
     * @return a string with the hashed version of the password
     */
    private String generateHashedPassword(String unhashedPassword){

        char [] chars = unhashedPassword.toCharArray();
        byte [] salt = getSalt();

        PBEKeySpec spec = new PBEKeySpec(chars,salt,iterations,keyLength);
        try{
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte [] hash = skf.generateSecret(spec).getEncoded();
            return iterations + ":" + toHex(salt) + ":" + toHex(hash);
        }catch (NoSuchAlgorithmException nsae){
            System.out.println("System needs to download the PBKDF2WithHmacSHA1, maybe update java API?");
            nsae.printStackTrace();
        }catch (InvalidKeySpecException ikse){
            System.out.println("Invalid key");
            ikse.printStackTrace();
        }
        return null;
    }

    /**
     * Get the salt used to hash the password
     * @return salt
     */
    private byte [] getSalt() {
        try{
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            byte [] salt = new byte [16];
            sr.nextBytes(salt);
            return salt;
        }catch (NoSuchAlgorithmException nsae){
            System.out.println("System needs to download the SHA1PRNG, maybe update java API?");
            nsae.printStackTrace();
        }
        return null;
    }

    /**
     * converts an array of bytes to string
     * @param array array of bytes
     * @return a string of hexabytes
     */
    private String toHex(byte[] array){
        BigInteger bi = new BigInteger(1,array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if(paddingLength > 0){
            return String.format("%0" + paddingLength +"d",0) + hex;
        }else {
            return hex;
        }
    }

    /**
     * Get the salted and hashed password
     * @param unhashedPassword the unhased password of the user
     * @return salted and hashed password
     */
    public String [] getSaltAndHash(String unhashedPassword){
        String resultFromAlgorithm = generateHashedPassword(unhashedPassword);
        String iterationsLength = String.valueOf(iterations);
        resultFromAlgorithm = resultFromAlgorithm.substring(iterationsLength.length() + 1,resultFromAlgorithm.length()-1);
        return resultFromAlgorithm.split(":");
    }
}