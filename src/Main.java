
import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import javax.swing.*;
import gui.*;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The Main class is used to run the main program.
 */

public class Main {
    public static void main(String[] args){
        NativeInterface.open();
        SwingUtilities.invokeLater(() -> {
            Frame mainFrame = new Frame("RentObike", 1300, 900);
        });
        NativeInterface.runEventPump();
    }
}