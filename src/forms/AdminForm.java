package forms;

import gui.CardForm;
import gui.TablePanel;

import javax.swing.*;

public class AdminForm extends CardForm {

    /**
     * Creates a CardForm on top of other content in order to collect data from user
     * @param parentFrame the JFrame container the card should be added to
     * @param parentPanel the panel that is retrieving data from user
     * @param x cards horizontal position anchored left side
     * @param y cards vertical position anchored in bottom
     * @param width card width
     * @param height card height
     */
    public AdminForm(JFrame parentFrame, TablePanel parentPanel, int x, int y, int width, int height) {
        super(parentFrame, parentPanel, x, y, width, height);

        headLine.setText("Register Administrator");

        addInput("Email");
        addInput("Name");
        createForm();
    }

    @Override
    protected boolean validateInput() {
        if(!InputValidator.validEmail(textFields.get(0).getText())){
            errorLabel.setText("Incorrect Email");
            validate();
            textFields.get(0).requestFocus();
            return false;
        }
        if(!InputValidator.validName(textFields.get(1).getText())){
            errorLabel.setText("Name can not be empty");
            validate();
            textFields.get(1).requestFocus();
            return false;
        }
        return true;
    }
}
