package forms;


import dao.BikeDAO;
import dao.DockingStationDAO;
import design.GuiColors;
import dto.BikeType;
import dto.DockingStation;
import gui.CardForm;
import gui.TablePanel;
import panels.BikePanel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The CardForm class is inherited by several form classes
 * The CardForms are displayed on top of other content like a floating card
 * Depends on a JFrame with a JLayeredPane
 *
 * BikeForm used to add new bikes
 */
public class BikeForm extends CardForm{

    private ArrayList<JComboBox<Object>> comboBoxes;
    private ArrayList<JLabel> boxLabels;
    private BikeDAO bikeDAO;
    private DockingStationDAO dockDAO;
    JComboBox<Object> typeBox;
    JComboBox<Object> dockBox;
    ArrayList<BikeType> types;

    /**
     * Creates a CardForm on top of other content in order to collect data from user
     * @param parentFrame the JFrame container the card should be added to
     * @param parentPanel the panel that is retrieving data from user
     * @param x cards horizontal position anchored left side
     * @param y cards vertical position anchored in bottom
     * @param width card width
     * @param height card height
     * @param con database connection object
     */
    public BikeForm(JFrame parentFrame, TablePanel parentPanel, int x, int y, int width, int height, Connection con) {
        super(parentFrame, parentPanel, x, y, width, height);

        comboBoxes = new ArrayList<>();
        boxLabels = new ArrayList<>();

        headLine.setText("Register new Bike");
        headLine.setBorder(new EmptyBorder(0,0,0,20));

        bikeDAO = new BikeDAO(con);
        dockDAO = new DockingStationDAO(con);


        typeBox = new JComboBox<>();
        //add content to biketype combobox
        types = bikeDAO.getTypes();
        for(BikeType temp : types){
            typeBox.addItem(temp);
        }
        typeBox.addItem("New type");

        typeBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED){
                    Object selected = e.getItem();
                    if(selected.toString().equals("New type")){
                        System.out.println("New type selected");
                        newType();
                    }
                }
            }
        });

        dockBox = new JComboBox<>();
        ArrayList<DockingStation> docks = dockDAO.getDockingStationList();
        for(DockingStation temp : docks){
            dockBox.addItem(temp);
        }

        addInputs();

        createForm();
    }
    @Override
    public void toggle(){

        typeBox.removeAllItems();
        types = bikeDAO.getTypes();
        for(BikeType temp : types){
            typeBox.addItem(temp);
        }
        typeBox.addItem("New type");

        super.toggle();
    }

    private void newType(){
        boxLabels.remove(0);
        comboBoxes.remove(0);
        addInput("New type");
        createForm();
        validate();
    }

    protected void addInput(String label, JComboBox<Object> comboBox) {
        boxLabels.add(new JLabel(label));
        comboBoxes.add(comboBox);
    }

    @Override
    protected void createForm() {
        removeAll();
        add(headLine);
        for(int i=0;i<boxLabels.size();i++){
            //add label first with style
            JLabel tempLabel = boxLabels.get(i);
            tempLabel.setForeground(GuiColors.GRAY_TEXT);
            add(tempLabel);

            JComboBox tempBox = comboBoxes.get(i);
            tempBox.setPreferredSize(new Dimension(width, 25));
            tempBox.setBackground(GuiColors.ODD_DARK);
            tempBox.setForeground(GuiColors.WHITE);
            tempBox.setBorder(BorderFactory.createLineBorder(GuiColors.GRAY_TEXT, 1));
            add(tempBox);
        }
        super.createForm();
        validate();
    }

    private void removeInputs(){
        labels.clear();
        textFields.clear();
        boxLabels.clear();
        comboBoxes.clear();
    }

    private void addInputs(){
        addInput("Make");
        addInput("Purchase Price");
        addInput("Type", typeBox);
        addInput("Docking Station", dockBox);
    }

    @Override
    protected void save() {
        if(validateInput()) {
            System.out.println("Valid input");
            ArrayList<String> input = new ArrayList<>();
            BikeType type;
            DockingStation dock;
            int type_id = 0;
            if(textFields.size() > 2 && textFields.get(2) != null){
                type = new BikeType(textFields.get(2).getText(), bikeDAO);
                type_id = type.saveNewType();
                dock = (DockingStation) comboBoxes.get(0).getSelectedItem();
            }
            else{
                type = (BikeType) comboBoxes.get(0).getSelectedItem();
                type_id = type.getId();
                dock = (DockingStation) comboBoxes.get(1).getSelectedItem();
            }

            if(type_id == -1){
                errorLabel.setText("Could add new type");
                return;
            }

            int dock_id = dock.getId();

            String make = textFields.get(0).getText();
            String price = textFields.get(1).getText();

            input.add(String.valueOf(type_id));
            input.add(make);
            input.add(String.valueOf(dock_id));
            input.add(price);
            ((BikePanel) parentPanel).updateTypeBox();
            parentPanel.save(input);
            cancelAndClear();
        }
    }

    @Override
    protected void cancelAndClear() {
        removeInputs();
        addInputs();
        createForm();
        for(JComboBox<Object> temp : comboBoxes){
            temp.setSelectedIndex(0);
        }
        super.cancelAndClear();
    }

    @Override
    protected boolean validateInput() {
        if(!InputValidator.validMake(textFields.get(0).getText())){
            errorLabel.setText("Invalid make");
            validate();
            textFields.get(0).requestFocus();
            return false;
        }
        if(!InputValidator.validInteger(textFields.get(1).getText())){
            errorLabel.setText("Price must be valid number");
            validate();
            textFields.get(1).requestFocus();
            return false;
        }
        if(textFields.size() > 2 && textFields.get(2) != null){
            if(!InputValidator.validText(textFields.get(2).getText())){
                errorLabel.setText("Invalid new type");
                validate();
                textFields.get(2).requestFocus();
                return false;
            }
        }
        return true;
    }

}
