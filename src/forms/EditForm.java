package forms;

import design.GuiColors;
import gui.CardForm;
import gui.TablePanel;
import registration.HashGenerator;
import registration.PasswordValidator;
import dao.AdminDAO;

import javax.swing.*;
import java.awt.*;
import java.sql.Connection;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The CardForm class is inherited by several form classes
 * The CardForms are displayed on top of other content like a floating card
 * Depends on a JFrame with a JLayeredPane
 *
 * EditForm is used to edit password
 */
public class EditForm extends CardForm {

    private final String adminEmail;
    private final Connection con;

    private ArrayList<JPasswordField> passwordFields = new ArrayList<>();
    private ArrayList<JLabel> passLabels = new ArrayList<>();

    /**
    * Creates a CardForm on top of other content in order to collect data from user
    * @param parentFrame the JFrame container the card should be added to
    * @param parentPanel the panel that is retrieving data from user
    * @param x cards horizontal position anchored left side
    * @param y cards vertical position anchored in bottom
    * @param width card width
    * @param height card height
    * @param adminEmail currently logged in email
    * @param con database connection object
    */
    public EditForm(JFrame parentFrame, TablePanel parentPanel, int x, int y, int width, int height, String adminEmail, Connection con) {
        super(parentFrame, parentPanel, x, y, width, height);
        this.con = con;
        this.adminEmail = adminEmail;
        headLine.setText("Edit password");
        addInput("Current Password");
        addInput("New password");
        addInput("Repeat new password");
        createForm();
    }
    protected void addInput(String label){
        passLabels.add(new JLabel(label));
        passwordFields.add(new JPasswordField());
    }
    @Override
    protected void createForm(){

        for(int i=0;i<passLabels.size();i++){
            //add label first with style
            JLabel tempLabel = passLabels.get(i);
            tempLabel.setForeground(GuiColors.GRAY_TEXT);
            add(tempLabel);
            JPasswordField tempField = passwordFields.get(i);
            tempField.setPreferredSize(new Dimension(width, 25));
            tempField.setBackground(GuiColors.ODD_DARK);
            tempField.setForeground(GuiColors.WHITE);
            tempField.setCaretColor(GuiColors.WHITE);
            tempField.setBorder(BorderFactory.createLineBorder(GuiColors.GRAY_TEXT, 1));
            add(tempField);
        }

        super.createForm();


        passwordFields.get(0).requestFocus();
    }
    @Override
    protected void save(){
        if(validateInput()) {
            ArrayList<String> input = new ArrayList<>();
            for (JTextField temp : textFields) {
                input.add(temp.getText());
            }
            PasswordValidator passwordValidator = new PasswordValidator();
            HashGenerator hashGenerator = new HashGenerator();
            AdminDAO adminDAO = new AdminDAO(con);
            String [] saltAndHash = hashGenerator.getSaltAndHash(passwordFields.get(1).getText());
            if (!passwordValidator.validatePassword(passwordFields.get(0).getText(),adminDAO.getPasswordFromDB(adminEmail))) {
                errorLabel.setText("Wrong Password");
                validate();
                passwordFields.get(0).requestFocus();
            }else if(!adminDAO.changePassword(adminEmail,saltAndHash[0],saltAndHash[1])){
                errorLabel.setText("Error: Please try again");
                validate();
                passwordFields.get(0).requestFocus();
            }else{

                for (JPasswordField temp : passwordFields) {
                    temp.setText(null);
                }
                this.hideCard();
                errorLabel.setText("");
            }
        }
    }
    @Override
    protected boolean validateInput() {
        if(!InputValidator.validPasswordLength(passwordFields.get(1).getText(), passwordFields.get(2).getText())){
            errorLabel.setText("8 characters +");
            validate();
            passwordFields.get(1).requestFocus();
            return false;
        }
        if(!InputValidator.validPasswordSame(passwordFields.get(1).getText(), passwordFields.get(2).getText())){
            errorLabel.setText("Not the same");
            validate();
            passwordFields.get(1).requestFocus();
            return false;
        }
        return true;
    }
    @Override
    protected void showCard(){
        super.showCard();
        passwordFields.get(0).requestFocus();
    }
    @Override
    public void newBounds() {
        if(isVisible()) {
            int height = this.height;
            int width = this.width;
            int x = (this.getRootPane().getWidth()/2) - (width/2);
            int y = (this.getRootPane().getHeight()/2) - (height/2);
            this.setBounds(x, y, width, height);
        }
    }
}
