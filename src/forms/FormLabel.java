package forms;

import javax.swing.*;
import java.awt.*;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The FormLabel class is used to create a label in CardForms with set font
 */

public class FormLabel extends JLabel{
    /**
     * Create a new label
     * @param text the text you want in the label
     */
    public FormLabel(String text){
        setFont(new Font("Arial", Font.PLAIN, 17));
        setText(text);
    }
}
