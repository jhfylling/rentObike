package forms;


import gui.CardForm;
import gui.TablePanel;

import javax.swing.*;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The CardForm class is inherited by several form classes
 * The CardForms are displayed on top of other content like a floating card
 * Depends on a JFrame with a JLayeredPane
 *
 * DockingForm is used to add new Docking Stations
 */
public class DockingForm extends CardForm{

    /**
     * Creates a CardForm on top of other content in order to collect data from user
     * @param parentFrame the JFrame container the card should be added to
     * @param parentPanel the panel that is retrieving data from user
     * @param x cards horizontal position anchored left side
     * @param y cards vertical position anchored in bottom
     * @param width card width
     * @param height card height
     */
    public DockingForm(JFrame parentFrame, TablePanel parentPanel, int x, int y, int width, int height){
        super(parentFrame, parentPanel, x, y, width, height);


        headLine.setText("Add new Docking Station");

        addInput("Name");
        addInput("Max bikes");
        addInput("Latitude");
        addInput("Longitude");
        createForm();
    }

    @Override
    protected boolean validateInput(){
        if(!InputValidator.validName(textFields.get(0).getText())){
            errorLabel.setText("Invalid name");
            validate();
            textFields.get(0).requestFocus();
            return false;
        }

        if(!InputValidator.validAmountOfBikes(textFields.get(1).getText())){
            errorLabel.setText("Max bikes can not be negative or empty");
            validate();
            textFields.get(1).requestFocus();
            return false;
        }
        if(!InputValidator.validCordinate(textFields.get(2).getText())){
            errorLabel.setText("Latitude can not be negative or empty");
            validate();
            textFields.get(2).requestFocus();
            return false;
        }
        if(!InputValidator.validCordinate(textFields.get(3).getText())){
            errorLabel.setText("Longitude can not be negative or empty");
            validate();
            textFields.get(3).requestFocus();
            return false;
        }
        return true;
    }
}
