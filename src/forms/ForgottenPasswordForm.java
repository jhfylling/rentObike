package forms;

import gui.CardForm;
import gui.TablePanel;

import javax.swing.*;
/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The CardForm class is inherited by several form classes
 * The CardForms are displayed on top of other content like a floating card
 * Depends on a JFrame with a JLayeredPane
 *
 * ForgottenPasswordForm is used to send new password to exiting admin
 */
public class ForgottenPasswordForm extends CardForm {

    /**
     * Creates a CardForm on top of other content in order to collect data from user
     *
     * @param parentFrame the JFrame container the card should be added to
     * @param parentPanel the panel that is retrieving data from user
     * @param x           cards horizontal position anchored left side
     * @param y           cards vertical position anchored in bottom
     * @param width       card width
     * @param height      card height
     */
    public ForgottenPasswordForm(JFrame parentFrame, TablePanel parentPanel, int x, int y, int width, int height) {
        super(parentFrame, parentPanel, x, y, width, height);
        headLine.setText("Send Password");
        addInput("Email: ");
        createForm();
    }

    /**
     * For security reasons we don't want any validation here.
     * @return true
     */
    @Override
    protected boolean validateInput() {
        return true;
    }

    /**
     * Overrides mother classes bounds to place the card in a natural position
     */
    @Override
    public void newBounds() {
        if(isVisible()) {
            int height = this.height;
            int width = this.width;
            int x = (this.getRootPane().getWidth()/2) - (width/2);
            int y = (this.getRootPane().getHeight()/2) - (height/2);
            this.setBounds(x, y, width, height);
        }
    }
}
