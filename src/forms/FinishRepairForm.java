package forms;

import gui.CardForm;
import gui.TablePanel;
import panels.BikePanel;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The CardForm class is inherited by several form classes
 * The CardForms are displayed on top of other content like a floating card
 * Depends on a JFrame with a JLayeredPane
 *
 * FinishRepairForm is used to register finished bike repairs
 */
public class FinishRepairForm extends CardForm {

    /**
     * Creates a CardForm on top of other content in order to collect data from user
     * @param parentFrame the JFrame container the card should be added to
     * @param parentPanel the panel that is retrieving data from user
     * @param x cards horizontal position anchored left side
     * @param y cards vertical position anchored in bottom
     * @param width card width
     * @param height card height
     */
    public FinishRepairForm(JFrame parentFrame, TablePanel parentPanel, int x, int y, int width, int height) {
        super(parentFrame, parentPanel, x, y, width, height);
        headLine.setText("Finish Repair");
        addInput("Bike Id:");
        addInput("Date (dd-mm-yyyy):");
        addInput("Description:");
        addInput("Price of Repair");
        createForm();
    }
    @Override
    protected void save(){
        if(validateInput()) {
            ArrayList<String> input = new ArrayList<>();
            for (JTextField temp : textFields) {
                input.add(temp.getText());
            }
            BikePanel bikePanel = (BikePanel) parentPanel;
            if (!bikePanel.registerFinishRepair(input)) {
                errorLabel.setText("Could not save");
                validate();
            }else{
                for (JTextField temp : textFields) {
                    temp.setText(null);
                }
                this.hideCard();
                errorLabel.setText("");
            }
        }
    }

    @Override
    protected boolean validateInput() {
        if(!InputValidator.validId(textFields.get(0).getText())){
            errorLabel.setText("Invalid ID");
            validate();
            textFields.get(0).requestFocus();
            return false;
        }
        if(!InputValidator.validDate(textFields.get(1).getText())){
            errorLabel.setText("Invalid date");
            validate();
            textFields.get(1).requestFocus();
            return false;
        }
        if(!InputValidator.validDescription(textFields.get(2).getText())){
            errorLabel.setText("Please write full description");
            validate();
            textFields.get(2).requestFocus();
            return false;
        }
        if(!InputValidator.validPrice(textFields.get(3).getText())){
            errorLabel.setText("Price can not be empty or negative");
            validate();
            textFields.get(3).requestFocus();
            return false;
        }
        return true;
    }
}
