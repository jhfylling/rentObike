package forms;

import dao.BikeDAO;
import design.GuiColors;
import dto.BikeType;
import gui.CardForm;
import gui.TablePanel;
import panels.BikePanel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.sql.Connection;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The CardForm class is inherited by several form classes
 * The CardForms are displayed on top of other content like a floating card
 * Depends on a JFrame with a JLayeredPane
 *
 * TypeForm is used to disable BikeTypes
 */

public class TypeForm extends CardForm {
    private ArrayList<JComboBox<Object>> comboBoxes;
    private ArrayList<JLabel> boxLabels;
    private BikeDAO bikeDAO;
    private BikeType bikeType;
    private BikePanel parentPanel;

    JComboBox<Object> typeBox;
    ArrayList<BikeType> types;

    /**
     * Creates a CardForm on top of other content in order to collect data from user
     * @param parentFrame A frame to use as a reference for a child frame(TypeForm)
     * @param parentPanel a panel to use as a reference when you are creating the form
     * @param x x coordinates
     * @param y y coordinates
     * @param width width of the form
     * @param height height of the form
     * @param con connection to the database
     */
    public TypeForm(JFrame parentFrame, TablePanel parentPanel, int x, int y, int width, int height, Connection con) {
        super(parentFrame, parentPanel, x, y, width, height);
        bikeDAO = new BikeDAO(con);
        this.parentPanel = (BikePanel) parentPanel;

        comboBoxes = new ArrayList<>();
        boxLabels = new ArrayList<>();

        headLine.setText("Disable type");
        headLine.setBorder(new EmptyBorder(0,0,0,20));

        typeBox = new JComboBox<>();

        types = bikeDAO.getTypes();
        for(BikeType temp : types){
            temp.setDAO(bikeDAO);
            typeBox.addItem(temp);
        }

        addInputs();
        createForm();
    }

    /**
     * An empty method for heritage
     * @return false
     */
    @Override
    protected boolean validateInput() {
        return false;
    }

    /**
     * A method to add elements to the type form
     * @param label a lable to the form
     * @param comboBox the combobox for the form
     */
    protected void addInput(String label, JComboBox<Object> comboBox) {
        boxLabels.add(new JLabel(label));
        comboBoxes.add(comboBox);
    }

    /**
     * adds the delete and cancle button to the form
     */
    @Override
    protected void addSubmitButton() {
        //submit button
        super.addSubmitButton();
        submit.setText("Disable");
    }

    /**
     * Creates the form.
     */
    @Override
    protected void createForm() {
        removeAll();
        add(headLine);
        for(int i=0;i<boxLabels.size();i++){
            //add label first with style
            JLabel tempLabel = boxLabels.get(i);
            tempLabel.setForeground(GuiColors.GRAY_TEXT);
            add(tempLabel);

            JComboBox tempBox = comboBoxes.get(i);
            tempBox.setPreferredSize(new Dimension(width, 25));
            tempBox.setBackground(GuiColors.ODD_DARK);
            tempBox.setForeground(GuiColors.WHITE);
            tempBox.setBorder(BorderFactory.createLineBorder(GuiColors.GRAY_TEXT, 1));
            add(tempBox);
        }
        super.createForm();
        validate();
    }

    /**
     * adds the input to the combobox
     */
    private void addInputs(){
        addInput("Type", typeBox);
    }

    /**
     * disables a type and updates the combobox
     */
    @Override
    protected void save() {
        bikeType = (BikeType) typeBox.getSelectedItem();
        if(bikeType != null && bikeType.delete()){
            parentPanel.updateTypeBox();
            errorLabel.setText(" ");
            //typeBox = new JComboBox<>();
            typeBox.removeAllItems();
            types = bikeDAO.getTypes();
            for(BikeType temp : types){
            temp.setDAO(bikeDAO);
            typeBox.addItem(temp);
            }
            this.hideCard();
        }else{
            errorLabel.setText("Could not disable type");
        }
    }
}


