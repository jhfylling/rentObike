package forms;


import gui.CardForm;
import gui.TablePanel;

import javax.swing.*;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The CardForm class is inherited by several form classes
 * The CardForms are displayed on top of other content like a floating card
 * Depends on a JFrame with a JLayeredPane
 *
 * UserForm is used to ban users
 */
public class UserForm extends CardForm{
    public UserForm(JFrame parentFrame, TablePanel parentPanel, int x, int y, int width, int height){
        super(parentFrame, parentPanel, x, y, width, height);


        headLine.setText("Ban selected user");

        addInput("Number of days");
        addInput("Comment");
        createForm();
    }

    @Override
    protected boolean validateInput() {
        if(!InputValidator.validInteger(textFields.get(0).getText())){
            errorLabel.setText("Ban days can not be negative or empty");
            validate();
            textFields.get(0).requestFocus();
            return false;
        }
        if(!InputValidator.validComment(textFields.get(1).getText())){
            errorLabel.setText("Comment can not be empty");
            validate();
            textFields.get(1).requestFocus();
            return false;
        }
        return true;
    }
}
