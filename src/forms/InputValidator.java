package forms;

import java.text.SimpleDateFormat;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 *
 * InputValidator is used to validate all input coming from forms
 */
public class InputValidator {

    /**
     * @param dateInput date to validate
     * @return true if valid date
     */
    public static boolean validDate(String dateInput){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date date = null;
        try{
            date = sdf.parse(dateInput);
        }catch (Exception e){

        }
        return date != null;
    }

    /**
     * @param textInput to validate
     * @return true if text is not empty
     */
    public static boolean validText(String textInput){return !textInput.isEmpty();}

    /**
     * @param input number to validate
     * @return true if input is not empty and not negative integer
     */
    public static boolean validInteger(String input){
        if (input.isEmpty()) return false;
        if(!isInteger(input)) return false;
        int num = Integer.parseInt(input);
        return num >= 0;
    }

    /**
     * @param input number to validate
     * @return true if input is not empty and not negative double
     */
    public static boolean validDouble(String input){
        if (input.isEmpty()) return false;
        if(!isDouble(input)) return false;
        int num = Integer.parseInt(input);
        return num >= 0;
    }

    /**
     * @param descriptionInput
     * @return true if valid input
     */
    public static boolean validDescription(String descriptionInput){
        return descriptionInput.length() > 5;
    }

    /**
     * @param id
     * @return true if valid input
     */
    public static boolean validId(String id){
        return id.length() != 0;
    }

    /**
     * @param priceInput
     * @return true if valid input
     */
    public static boolean validPrice(String priceInput){
        if(priceInput.isEmpty()) return false;
        Double price = Double.parseDouble(priceInput);
        return price >= 0;
    }

    /**
     * @param makeInput
     * @return true if valid input
     */
    public static boolean validMake(String makeInput){
        return !makeInput.isEmpty();
    }

    /**
     * @param emailInput
     * @return true if valid input
     */
    public static boolean validEmail(String emailInput){
        return !emailInput.isEmpty();
    }

    /**
     * @param nameInput
     * @return true if valid input
     */
    public static boolean validName(String nameInput){
        return !nameInput.isEmpty();
    }

    /**
     * @param nameInput
     * @return true if valid input
     */
    public static boolean validComment(String nameInput){return !nameInput.isEmpty();}

    /**
     * @param secLvlInput
     * @return true if valid input
     */
    public static boolean validSecLvl(String secLvlInput){
        if (secLvlInput.isEmpty()) return false;
        if(!isInteger(secLvlInput)) return false;
        int secLvl = Integer.parseInt(secLvlInput);
        return secLvl >= 1 && secLvl < 20;
    }

    /**
     * @param lngOrLat
     * @return true if valid input
     */
    public static boolean validCordinate(String lngOrLat){
        if(lngOrLat.isEmpty()) return false;
        if(!isDouble(lngOrLat)) return false;
        Double cordinate = Double.parseDouble(lngOrLat);
        return cordinate > 0;
    }

    /**
     * @param amount
     * @return true if valid input
     */
    public static boolean validAmountOfBikes(String amount){
        if(amount.isEmpty()) return false;
        if(!isDouble(amount)) return false;
        int numberOfBikes = Integer.parseInt(amount);
        return numberOfBikes > 0;
    }

    /**
     * @param in
     * @return true if valid input
     */
    private static boolean isInteger(String in){
        try{
            Integer.parseInt(in);
            return true;
        }catch (NumberFormatException nfe){
            return false;
        }
    }

    /**
     * @param in
     * @return true if valid input
     */
    private static boolean isDouble(String in){
        try{
            Double.parseDouble(in);
            return true;
        }catch (NumberFormatException nfe){
            return false;
        }
    }

    /**
     * @param newPassword
     * @param repeatedPassword
     * @return true if valid input
     */
    public static boolean validPasswordLength(String newPassword, String repeatedPassword){
        if(newPassword == null || repeatedPassword == null) return false;
        if(newPassword.length() < 4) return false;
        return true;
    }

    /**
     * @param newPassword
     * @param repeatedPassword
     * @return true if valid input
     */
    public static boolean validPasswordSame(String newPassword, String repeatedPassword){
        if(!newPassword.equals(repeatedPassword)) return false;
        return true;
    }

}
