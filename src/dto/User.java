package dto;
import dao.UserDAO;
import sql.Cleaner;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The User class is used to store information about users.
 */

public class User {
    private int user_id;
    private String card_info;
    private Date ban_until;
    private String comments;
    private Connection con;
    private UserDAO userDAO;

    /**
     * A constructor to initialize a user object
     * @param user_id the initial user ID
     * @param card_info the initial card information for payment
     * @param ban_until how long a user is banned if it should be the case
     * @param comments a comment for the initial user
     * @param con a connection to the database
     */
    public User(int user_id,String card_info, Date ban_until, String comments, Connection con) {
        this.user_id = user_id;
        this.card_info = card_info;
        this.ban_until = ban_until;
        this.comments = comments;
        this.con = con;
        this.userDAO = new UserDAO(con);
    }

    /**
     * @return user ID
     */
    public int getUserId() {return user_id;}

    /**
     * @param newId new user iD
     */
    public void setUserId(int newId){ user_id = newId;}

    /**
     * @return card information
     */
    public String getCard_info() {
        return card_info;
    }

    /**
     * @return ban date
     */
    public Date getBan_until() {
        return ban_until;
    }

    /**
     * @return comments for the user
     */
    public String getComments() {
        return comments;
    }

    /**
     * Ban a user
     * @param banDate banned until date of the user
     * @param banComment a comment for the ban
     * @return true if the person is banned and false if something went wrong
     */
    public boolean ban(Date banDate,String banComment) {

        return userDAO.ban(banDate, banComment, this.user_id);
    }

    /**
     * unban a user
     * @return return true if the person is unbanned and false if something went wrong
     */
    public boolean unban(){

        return userDAO.unban(this.user_id);
    }

    /**
     * A helping method to get days from the calender to use in other methods
     * @param days amount of days you want
     * @return a timeline of days between current date and the amount of days you wanted
     */
    private static Calendar addDays2(int days){
        Calendar date = new GregorianCalendar();
        date.getInstance();
        date.add(Calendar.DATE, days);
        return date;
    }

    /**
     *
     * @return a string of information about the initial user
     */
    public String toString() {
        System.out.println(ban_until);
        String bannedUntil = "null";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd");
        if(ban_until != null) {
            bannedUntil = sdf.format(ban_until.getTime());
        }
        return "\nid: " + user_id + " card info: " + card_info + " banned until: " + bannedUntil + " reason: " + comments;
    }


}