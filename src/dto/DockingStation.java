package dto;

import dao.DockingStationDAO;
import sql.Cleaner;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The DockingStation class is used to store information about docking stations.
 */

public class DockingStation {

    private int id;
    private PreparedStatement statement;
    private Connection con;
    private String name;
    private double power_usage;
    private int max_bikes;
    private double lat;
    private double lon;
    private DockingStationDAO dockDAO;

    /**
     * This is a constructor to initialize an docking station object.
     * @param id an initial dockin station id
     * @param name an initial docking station name
     * @param power_usage power usage of the initial docking station
     * @param max_bikes max capacity of bike in the initial docking station
     * @param lat latitude coordinates of the initial docking station
     * @param lon longitude coordinates of the initial docking station
     * @param con a connection to the database
     */
    public DockingStation(int id, String name, double power_usage, int max_bikes, double lat, double lon, Connection con) {
        this.con = con;
        this.id = id;
        this.name = name;
        this.power_usage = power_usage;
        this.max_bikes = max_bikes;
        this.lat = lat;
        this.lon = lon;

        this.dockDAO = new DockingStationDAO(con);

    }

    /**
     * @return docking station ID
     */
    public int getId () {
        return id;
    }

    /**
     * @return docking station name
     */
    public String getName() { return name; }

    /**
     * @return power usage
     */
    public double getPower_usage() { return power_usage; }

    /**
     * @return max capacity
     */
    public int getMax_bikes() { return max_bikes; }

    /**
     * @return latitude coordinates
     */
    public double getLat() {
        return lat;
    }

    /**
     * @return longitude coordinates
     */
    public double getLon() {
        return lon;
    }

    /**
     * @param id a new docking station ID
     */
    public void setId (int id) {
        this.id = id;
    }

    /**
     * Get an array list of bike IDs in the current docking station
     * @return number of bikes at station
     */
    public int getNumberOfBikesAtStation() { //returns list of bike_IDs at dockingStation
        return dockDAO.getNumberOfBikesAtStation(this.id);
    }

    /**
     * @param obj object to compare
     * @return true if object is of type DockingStation and has same dock_id
     */
    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof DockingStation)) return false;
        DockingStation temp = (DockingStation) obj;
        if(temp.getId() != this.id) return false;

        return true;
    }

    /**
     * Wrights a string with docking station information
     * @return a string with docking station ID, name, power usage, capacity, latitude and longitude coordinates
     */
    public String toString() {
        return name;
    }

    /**
     * @return Connection to the database
     */
    public Connection getCon() {
        return con;
    }

    /**
     * @param con set a new connection
     */
    public void setCon(Connection con) {
        this.con = con;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @param power_usage
     */
    public void setPower_usage(double power_usage) {
        this.power_usage = power_usage;
    }

    /**
     *
     * @param max_bikes
     */
    public void setMax_bikes(int max_bikes) {
        this.max_bikes = max_bikes;
    }

    /**
     *
     * @param lat
     */
    public void setLat(double lat) {
        this.lat = lat;
    }

    /**
     *
     * @param lon
     */
    public void setLon(double lon) {
        this.lon = lon;
    }

    /**
     * save a edited docking station
     * @return true if it was saved and false if something went wrong
     */
    public boolean save(){
        return dockDAO.editDckingStation(this.name, this.power_usage, this.max_bikes, this.lat, this.lon, this.id);
    }
}
