package dto;

import dao.BikeDAO;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The BikeType class is used to store information about bike types.
 */

public class BikeType {
    private int id;
    private String description;
    private BikeDAO bikeDAO;

    /**
     * This is a constructor to initialize a bike type.
     * @param id type ID for the initial type
     * @param description type description of the initial type
     */
    public BikeType(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public void setDAO(BikeDAO bikeDAO) {
        this.bikeDAO = bikeDAO;
    }

    /**
     * This is a constructor to initialize a bike type.
     * @param description type description of the initial type
     */
    public BikeType(String description, BikeDAO bikeDAO) {
        this.bikeDAO = bikeDAO;
        this.description = description;
    }

    public boolean delete(){
        return bikeDAO.disableType(id);
    }

    /**
     * Get the type ID
     * @return type ID
     */
    public int getId() {
        return id;
    }

    /**
     * Set a new type ID
     * @param id new type ID
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Get the type description
     * @return type description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set a new type description
     * @param description new type description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @param obj Object to compare with
     * @return true if object is of type BikeType and has same type_id
     */
    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof BikeType)) return false;
        BikeType temp = (BikeType) obj;
        if(temp.getId() != this.id) return false;

        return true;
    }

    /**
     * Saves this type object to database and returns generated BikeType ID
     * @return id of new BikeType from autoincrement in DB
     */
    public int saveNewType(){
        this.id = bikeDAO.saveNewType(this.description);
        return id;
    }

    /**
     * a toString method to return information about the type
     * @return type description
     */
    public String toString(){
        return description;
    }
}