package dto;

import java.util.ArrayList;
/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * StatsContaienr class is a container for stats used to create the statistic panel.
 */
public class StatsContainer {

    private ArrayList<Integer> repairedBikesPerMonth = new ArrayList<>();
    private ArrayList<Integer> amountOfTripsPerUser = new ArrayList<>();
    private ArrayList<Double> compareSet = new ArrayList<>();
    private ArrayList<Integer> monthlyBikeTrips = new ArrayList<>();
    private ArrayList<Integer> monthlyNewUser = new ArrayList<>();
    private ArrayList<Integer> hourlyTrips = new ArrayList<>();


    private double earnings = 0;
    private int numberOfUsers = 0;
    private int numberOfTrips = 0;
    private int onlyBeenOnOneTrip = 0;
    private double totalRepairCost = 0;
    private double powerUsage = 0;

    /**
     *
     * @return ArrayList containing hourly trips
     */
    public ArrayList<Integer> getHourlyTrips() {
        return hourlyTrips;
    }

    /**
     *
     * @param hourlyUsers ArrayList containing hourly users
     */
    public void setHourlyTrips(ArrayList<Integer> hourlyUsers) {
        this.hourlyTrips = hourlyUsers;
    }

    /**
     *
     * @return ArrayList containing monthlyNewUsers
     */
    public ArrayList<Integer> getMonthlyNewUser() {
        return monthlyNewUser;
    }

    /**
     *
     * @param monthlyNewUser ArrayList containing monthly new users
     */
    public void setMonthlyNewUser(ArrayList<Integer> monthlyNewUser) {
        this.monthlyNewUser = monthlyNewUser;
    }

    /**
     *
     * @return ArrrayList containing monthly bike trips.
     */
    public ArrayList<Integer> getMonthlyBikeTrips() {
        return monthlyBikeTrips;
    }

    /**
     *
     * @param monthlyBikeTrips ArrayList containing monthly bike trips
     */
    public void setMonthlyBikeTrips(ArrayList<Integer> monthlyBikeTrips) {
        this.monthlyBikeTrips = monthlyBikeTrips;
    }

    /**
     *
     * @return ArrayList containing amount of repaired bikes per month
     */
    public ArrayList<Integer> getRepairedBikesPerMonth() {
        return repairedBikesPerMonth;
    }

    /**
     *
     * @param repairedBikesPerMonth ArrayList containing repairedBikesPerMonth
     */
    public void setRepairedBikesPerMonth(ArrayList<Integer> repairedBikesPerMonth) {
        this.repairedBikesPerMonth = repairedBikesPerMonth;
    }

    /**
     *
     * @return ArrayList with amount of trips per user
     */
    public ArrayList<Integer> getAmountOfTripsPerUser() {
        return amountOfTripsPerUser;
    }

    /**
     *
     * @param amountOfTripsPerUser ArrayList containing amount of trips per user for spesific periodes
     */
    public void setAmountOfTripsPerUser(ArrayList<Integer> amountOfTripsPerUser) {
        this.amountOfTripsPerUser = amountOfTripsPerUser;
    }

    /**
     *
     * @return set of selected stats for comparing
     */
    public ArrayList<Double> getCompareSet() {
        return compareSet;
    }

    /**
     *
     * @param compareSet set of selected stats
     */
    public void setCompareSet(ArrayList<Double> compareSet) {
        this.compareSet = compareSet;
    }

    /**
     *
     * @return totat earnings
     */
    public double getEarnings() {
        return earnings;
    }

    /**
     *
     * @param earnings total
     */
    public void setEarnings(double earnings) {
        this.earnings = earnings;
    }

    /**
     *
     * @return number of users
     */
    public int getNumberOfUsers() {
        return numberOfUsers;
    }

    /**
     *
     * @param numberOfUsers
     */
    public void setNumberOfUsers(int numberOfUsers) {
        this.numberOfUsers = numberOfUsers;
    }

    /**
     *
     * @return number of trips
     */
    public int getNumberOfTrips() {
        return numberOfTrips;
    }

    /**
     *
     * @param numberOfTrips
     */
    public void setNumberOfTrips(int numberOfTrips) {
        this.numberOfTrips = numberOfTrips;
    }

    /**
     *
     * @return amount of users only been on one trip
     */
    public int getOnlyBeenOnOneTrip() {
        return onlyBeenOnOneTrip;
    }

    /**
     *
     * @param onlyBeenOnOneTrip amount of users
     */
    public void setOnlyBeenOnOneTrip(int onlyBeenOnOneTrip) {
        this.onlyBeenOnOneTrip = onlyBeenOnOneTrip;
    }

    /**
     *
     * @return total repair costs
     */
    public double getTotalRepairCost() {
        return totalRepairCost;
    }

    /**
     *
     * @param totalRepairCost
     */
    public void setTotalRepairCost(double totalRepairCost) {
        this.totalRepairCost = totalRepairCost;
    }

    /**
     *
     * @return total power usages for all docking stations
     */
    public double getPowerUsage() {
        return powerUsage;
    }

    /**
     *
     * @param powerUsage for all docking stations
     */
    public void setPowerUsage(double powerUsage) {
        this.powerUsage = powerUsage;
    }

}
