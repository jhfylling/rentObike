package dto;

import dao.BikeDAO;
import sql.Cleaner;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The Bike class is used to store information about bikes.
 */

public class Bike {

    private Connection con;
    private BikeDAO bikeDAO;
    private int bike_id;
    private String make;
    private int price;
    private int mileage;
    private int charge_level;
    private Date date_bought;
    private BikeType type;
    private DockingStation doc;
    private double lat;
    private double lon;

    /**
     * This is a constructor to initialize an Bike object.
     * @param bike_id an initial bike ID
     * @param type an initial bike type
     * @param make an initial bike make
     * @param price an initial bike price
     * @param mileage mileage of the bike
     * @param charge_level charge level of the bike
     * @param date_bought the date the bike was bought
     * @param doc a docking station object
     * @param con connection to the database
     * @param lat latitude coordinates for the initial bike
     * @param lon longitude coordinates for the initial bike
     */
    public Bike(int bike_id, BikeType type, String make, int price, int mileage, int charge_level, Date date_bought, DockingStation doc, Connection con, double lat, double lon){
        this.con = con;
        this.bike_id = bike_id;
        this.make = make;
        this.price = price;
        this.mileage = mileage;
        this.charge_level = charge_level;
        this.date_bought = date_bought;
        this.type = type;
        if(doc == null){
            //Special fake docking station to indicate that bike is not docked
            this.doc = new DockingStation(0, "Not in dock", 0, 5000,5,5, con);
        }else {
            this.doc = doc;
        }
        this.lat = lat;
        this.lon = lon;
        bikeDAO = new BikeDAO(con);
    }

    public Bike(Bike x){
        this.con = x.getCon();
        this.bike_id = x.getBike_id();
        this.make = x.getMake();
        this.price = x.getPrice();
        this.mileage = x.getMileage();
        this.charge_level = x.getCharge_level();
        this.date_bought = x.getDate_bought();
        this.type = x.getType();
        this.doc = x.getDoc();
        this.lat = x.getLat();
        this.lon = x.getLon();
        bikeDAO = new BikeDAO(con);
    }

    /**
     * @return latitude coordinates
     */
    public double getLat() {
        return lat;
    }

    /**
     * @return longitude coordinates
     */
    public double getLon() {
        return lon;
    }

    /**
     * @return bike type
     */
    public BikeType getType(){
        return type;
    }

    /**
     * @return bike ID
     */
    public int getBike_id() {
        return bike_id;
    }

    /**
     * @return bike make
     */
    public String getMake() {
        return make;
    }

    /**
     * @return charge level
     */
    public int getCharge_level() {
        return charge_level;
    }

    /**
     * @return bike mileage
     */
    public int getMileage() {
        return mileage;
    }

    /**
     * @return bike price
     */
    public int getPrice(){
        return price;
    }

    /**
     * @return date bought
     */
    public Date getDate_bought(){
        return date_bought;
    }

    /**
     * @return connection
     */
    public Connection getCon() {
        return con;
    }

    /**
     * @return a string with bike ID, bike make, bike price, bike mileage, bike charge level and date bought
     */
    public String toString(){
        return  String.valueOf(bike_id)+ " " + make + " " + price + " " + mileage + " " + charge_level + " " + date_bought;
    }


    /**
     * @return a docking station object
     */
    public DockingStation getDock(){
        return doc;
    }

    /**
     *
     * @param con
     */
    public void setCon(Connection con) {
        this.con = con;
    }

    /**
     *
     * @param bike_id
     */
    public void setBike_id(int bike_id) {
        this.bike_id = bike_id;
    }

    /**
     *
     * @param make
     */
    public void setMake(String make) {
        this.make = make;
    }

    /**
     *
     * @param price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     *
     * @param mileage
     */
    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    /**
     *
     * @param charge_level
     */
    public void setCharge_level(int charge_level) {
        this.charge_level = charge_level;
    }

    /**
     *
     * @param date_bought
     */
    public void setDate_bought(Date date_bought) {
        this.date_bought = date_bought;
    }

    /**
     *
     * @param type
     */
    public void setType(BikeType type) {
        this.type = type;
    }

    /**
     *
     * @return
     */
    public DockingStation getDoc() {
        return doc;
    }

    /**
     *
     * @param doc
     */
    public void setDoc(DockingStation doc) {
        this.doc = doc;
    }

    /**
     *
     * @param lat
     */
    public void setLat(double lat) {
        this.lat = lat;
    }

    /**
     *
     * @param lon
     */
    public void setLon(double lon) {
        this.lon = lon;
    }

    /**
     * Calls upon bikeDAO's edit bike function to edit this bike and store current information in database.
     * @return true if the edit was complete and saved in the database and false if something went wrong
     */
    public boolean save(){
        int type_id = type.getId();
        int dock_id;
        if(doc == null || doc.getId() == 0){
            return false;
        }
        dock_id = doc.getId();
        return bikeDAO.editBike(this.bike_id, this.make, this.charge_level, this.price, type_id, dock_id);
    }
}
