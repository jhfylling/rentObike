package dto;

import dao.AdminDAO;

import java.sql.*;

/**
 * Created by team 14 during school project on NTNU second semester.<br>
 * Duration: february 2018 - april 2018.<br>
 * course: Computer engineering.<br>
 * class: System development.<br>
 *<br>
 * The Admin class is used to store information about administrators of the system.
 */

public class Admin {

    private int admin_id;
    private String email;
    private String admin_name;
    private boolean isEnabled;
    private AdminDAO adminDAO;

    /**
     * This is a constructor to initialize an administrator object.
     * @param admin_id an initial admin ID
     * @param email an initial admin e-mail
     * @param admin_name an initial admin name
     * @param con the connection to the database
     */
    public Admin(int admin_id, String email, String admin_name,Connection con){
        this.admin_id = admin_id;
        this.email = email;
        this.admin_name = admin_name;
        this.isEnabled = isEnabled;

        adminDAO = new AdminDAO(con);
    }

    /**
     * @return administrator ID
     */
    public int getAdmin_id() {
        return admin_id;
    }

    /**
     * @param newAdmin_id a new administrator ID
     */
    public void setAdmin_id(int newAdmin_id){
        admin_id = newAdmin_id;
    }

    /**
     * @return administrator e-mail
     */
    public String getAdmin_email() {
        return email;
    }

    /**
     * @return administrator name
     */
    public String getAdmin_name() {
        return admin_name;
    }

    /**
     * Get the boolean value where administrator is enabled or not
     * @return boolean value true/false
     */
    public boolean isEnabled(){
        return isEnabled;
    }

    /**
     * Calls upon adminDAO's edit admin function to edit this bike and store current information in database.
     * @return true of the edit was complete and saved in the database and false if something went wrong
     */
    public boolean save(){
        return adminDAO.editadmin(this.email, this.admin_name, this.admin_id);
    }

    /**
     *
     * @param pw
     * @param email
     * @return
     */
    // hent denne metoden fra en annen klasse
    public  boolean isValid(String pw, String email){
        if(isValid(pw,email)){
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        String res = getAdmin_id() + getAdmin_email() + getAdmin_email();
        return res;
    }

    /**
     * @return admin email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @param admin_name
     */
    public void setAdmin_name(String admin_name) {
        this.admin_name = admin_name;
    }
}
