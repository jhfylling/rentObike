RentObike

Javadoc link:

http://team14ntnu18.pages.stud.iie.ntnu.no/rentObike

When creating a project with intelliJ:
-Select rentobike with .git folder as project root folder
-Make sure you use files in src/ and put all new sourcefiles in that folder

When committing:
-Use "git add ."
	This will add all files in the folder, ignoring everyhing in .gitignore file
-If you want to know which files are staged for commit, run this command:
	"git diff --cached --name-only"
-git commit
-git push