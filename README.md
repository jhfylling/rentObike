#RentObike

Dette er en applikasjon skrevet med JAVA og Swing, 1.året dataingeniør.

Applikasjonen vi har laget brukes for å administrere et el-sykkelutleie system som minner om bysyklene i f.eks. Trondheim.
Syklene spores med GPS og kan sees på kartet vi har integrert i applikasjonen. (Google Maps)