/**
* Contains functions to retrieve bike information and placer markers
* Version 1.0 14.April 2018
* Dev: Johan-Henrik Fylling
*/


/**
* Loads XML file from server and runs createBikeMarkers()
*/
function loadXMLDoc() {
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
	if (this.readyState == 4 && this.status == 200) {
		createBikeMarkers(this);
	}
	};
	xmlhttp.open("GET", "https://johandev.no/bikeXML.php", true);
	xmlhttp.send();
	console.log("loading bikes");
}
/**
* @PARAM xml document from server
* Started by loadXMLDoc()
* Creates bike markers and places them on the map
* Reference to markers stored in marker[]
*/
function createBikeMarkers(xml) {
	var i;
	var xmlDoc = xml.responseXML;
	var table="Positions<br />";
	var x = xmlDoc.getElementsByTagName("bike");
	for (i = 0; i <x.length; i++) {

		//console.log("i: "+i);
		//x[i].childNodes[0].nodeValue + "<br />";
		var bike_id = x[i].getElementsByTagName("bike_id")[0].childNodes[0].nodeValue;
		var longitude = x[i].getElementsByTagName("longitude")[0].childNodes[0].nodeValue;
		var latitude = x[i].getElementsByTagName("latitude")[0].childNodes[0].nodeValue;
		var pos = {lat: Number(latitude), lng: Number(longitude)};

		markers[i] = new google.maps.Marker({
			position: pos,
			title: "bike",
			icon: 'bikeMarkerSmall.png',
			map: map,
			id: bike_id,
			address: "unknown"
		});

		markers[i].addListener('click', function () {
			console.log("Clicked on bike: "+this.id);
			console.log("Address: " + this.address);
			if(map.getZoom() < 15){
				map.setZoom(15);
				map.setCenter(this.getPosition());
			}

			if(typeof setBike === "function"){
				console.log("setBike defined");
				setBike(this.id);
			}
			geo(geocoder, map, this, "BIKE");
			infoWindow.open(map, this);
		});
	}
}