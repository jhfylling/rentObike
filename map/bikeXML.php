<?php
	/**
	* File returns XML structure with bike information from database
	* Version 1.0 14.April 2018
	* Dev: Johan-Henrik Fylling
	*/
	
	/**
	* Creates XML structure of bikes to be parsed by JS
	*/
    $file = fopen("/home/web/db.properties", "r") or die("Could not open properties file");
    $properties = array();
    $i = 0;
    while(!feof($file)) {
        $properties[$i] = rtrim(fgets($file));
        $i++;
    }

	$host = $properties[0];
	$user = $properties[1];
	$pw = $properties[2];
	$db = $properties[3];

	$tilkobling = mysqli_connect($host,$user,$pw);
	mysqli_select_db($tilkobling, $db);

	$sql = "SELECT p.bike_id, p.longitude, p.latitude, p.time_position, b.dock_id FROM position p
			  JOIN bike b ON p.bike_id = b.bike_id
			WHERE CONCAT(p.bike_id, p.time_position) IN (SELECT CONCAT(bike_id, MAX(time_position)) FROM position GROUP BY bike_id)
			AND dock_id IS NULL
			ORDER BY bike_id ASC";
	$resultat = mysqli_query($tilkobling, $sql);


	$xml = new SimpleXMLElement('<xml/>');

	if($resultat){
		while($row = $resultat->fetch_assoc()){
            $bike = $xml-> addChild('bike', '');
			$bike -> addChild('bike_id', $row['bike_id']);
			$bike -> addChild('longitude', $row['longitude']);
			$bike -> addChild('latitude', $row['latitude']);
		}
	}

	Header('Content-type: text/xml');
	Header('Cache-Control: no-cache, must-revalidate');
	print($xml->asXML());


?>

