<?php
	/**
	* File returns XML structure with Dock information from database
	* Version 1.0 14.April 2018
	* Dev: Johan-Henrik Fylling
	*/

	/**
	* Creates XML structure of docking stations to be parsed by JS
	*/
	header('Content-type: text/plain; charset=utf-8');
    $file = fopen("/home/web/db.properties", "r") or die("Could not open properties file");
    $properties = array();
    $i = 0;
    while(!feof($file)) {
        $properties[$i] = rtrim(fgets($file));
        $i++;
    }

	$host = $properties[0];
	$user = $properties[1];
	$pw = $properties[2];
	$db = $properties[3];

	$tilkobling = mysqli_connect($host,$user,$pw);
	mysqli_select_db($tilkobling, $db);

	$sql = "SELECT * FROM dock";
	$resultat = mysqli_query($tilkobling, $sql);


	$xml = new SimpleXMLElement('<xml/>');

	if($resultat){
		while($row = $resultat->fetch_assoc()){
            $bike = $xml-> addChild('dock', '');
			$bike -> addChild('dock_id', $row['dock_id']);
			$bike -> addChild('dock_name', $row['dock_name']);
			$bike -> addChild('longitude', $row['longitude']);
			$bike -> addChild('latitude', $row['latitude']);
		}
	}

	Header('Content-type: text/xml');
	print($xml->asXML());


?>

