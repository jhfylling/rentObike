/**
* Contains functions to retrieve Docking Station information and placer markers
* Version 1.0 14.April 2018
* Dev: Johan-Henrik Fylling
*/

/**
* Loads XML file from server and runs createDockMarkers()
*/
function loadDocks() {
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			createDockMarkers(this);
		}
	};
	xmlhttp.open("GET", "https://johandev.no/dockXML.php", true);
	xmlhttp.send();
	//console.log("loading docks");
}
/**
* @PARAM xml document from server
* Started by loadXMLDoc()
* Creates Docking station markers and places them on the map
* Reference to markers stored in dockMarkers[]
*/
function createDockMarkers(xml) {
	var i;
	var xmlDoc = xml.responseXML;
	var table="Positions<br />";
	var x = xmlDoc.getElementsByTagName("dock");
	for (i = 0; i <x.length; i++) {

		//console.log("i: "+i);
		//x[i].childNodes[0].nodeValue + "<br />";
		var dock_id = x[i].getElementsByTagName("dock_id")[0].childNodes[0].nodeValue;
		var dock_name = x[i].getElementsByTagName("dock_name")[0].childNodes[0].nodeValue;
		var longitude = x[i].getElementsByTagName("longitude")[0].childNodes[0].nodeValue;
		var latitude = x[i].getElementsByTagName("latitude")[0].childNodes[0].nodeValue;
		var pos = {lat: Number(latitude), lng: Number(longitude)};

		dockMarkers[i] = new google.maps.Marker({
			position: pos,
			title: dock_name,
			map: map,
			id: dock_id,
			address: "unknown"
		});
		
		//label: dock_name,
		
		//geo(geocoder, map, dockMarkers[i].getPosition(), dockMarkers[i]);
		
		//set address to this bikes address
		//geocodeLatLng(geocoder, map, pos);
		//set title to address

		dockMarkers[i].addListener('click', function() {
			console.log("Clicked on docking station: " + this.id);
			console.log("Address: " + this.address);
			//set zoom and position to clicked marker
			if(map.getZoom() < 15){
				map.setZoom(15);
				map.setCenter(this.getPosition());
			}
			//check if setDock is defined and run setDock
			if(typeof setDock === "function"){
				console.log("setDock defined");
				setDock(this.id);
			}
			geo(geocoder, map, this, "DOCK");
			infoWindow.open(map, this);
		});
	}
}